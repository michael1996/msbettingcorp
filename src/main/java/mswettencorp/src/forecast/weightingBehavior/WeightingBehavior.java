package mswettencorp.src.forecast.weightingBehavior;

import java.util.List;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.SdTeams;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> WeightingBehavior <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 11.02.2018
 */
public interface WeightingBehavior {
        
    public List<Double> weighting(List<Matches> match, SdTeams team);
}
