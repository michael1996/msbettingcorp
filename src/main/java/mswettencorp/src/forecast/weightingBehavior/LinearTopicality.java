package mswettencorp.src.forecast.weightingBehavior;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.SdTeams;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> LinearTopicality <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 11.02.2018
 */
public class LinearTopicality implements WeightingBehavior {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    private List<Double> listWeighting;
    private double increment;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public LinearTopicality() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public List<Double> weighting(List<Matches> listMatches, SdTeams team) {
        listWeighting = new ArrayList<>(listMatches.size());
        increment = listMatches.size();
        for (Matches match : listMatches) {
            listWeighting.add(increment);
            increment--;
        }
        if (listWeighting.isEmpty()) {
            return new LinkedList<>();
        } else {
            return listWeighting;
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
