package mswettencorp.src.processor;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.Predictions;
import mswettencorp.src.mapping.SdModels;
import mswettencorp.src.util.HibernateUtil;
import mswettencorp.src.util.LoggerUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> MPCubeProcessor <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 09.02.2018
 */
public class MPCubeProcessor {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final Logger LOGGER = LoggerUtil.getLogger();
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    private Session session;    
    private BigDecimal factorization;
    private BigDecimal winHomeOdd;
    private BigDecimal drawOdd;
    private BigDecimal winAwayOdd;
    private BigDecimal under15Odd;
    private BigDecimal over15Odd;
    private BigDecimal under25Odd;
    private BigDecimal over25Odd;
    private BigDecimal under35Odd;
    private BigDecimal over35Odd;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    /**
     * 
     * @return 
     */
    public boolean initialize() {
        try {
            session = HibernateUtil.startConfiguredSession(); 
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        } catch (HibernateException ex) {
            LOGGER.log(Level.SEVERE, null, ex.getMessage());
            return false;
        }
    }
    
    /**
     * 
     */
    public void deInitialize() {
        try {
            session = HibernateUtil.closeSession(); 
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": deinitialized succesfully."));
        } catch (HibernateException ex) {
            LOGGER.log(Level.SEVERE, null, ex.getMessage());
        } 
    }
    
    /**
     * 
     * @param ctxType
     * @param ctxValue 
     */
    public void process(String ctxType, String ctxValue) {
        List<Matches> listMatches = session.createQuery("SELECT DISTINCT mf.matchForecast "
            .concat(" FROM MatchForecasts mf")
            .concat(" WHERE mf.isHistoric = false"))
            .getResultList();
        for (Matches match : listMatches) {
            List<Predictions> listForecasts = match.getListMatchForecasts();
            factorization = BigDecimal.valueOf((double)1 / (double)listForecasts.size());
            winHomeOdd = BigDecimal.valueOf(0);
            drawOdd = BigDecimal.valueOf(0);
            winAwayOdd =  BigDecimal.valueOf(0);
            under15Odd = BigDecimal.valueOf(0);
            over15Odd = BigDecimal.valueOf(0);
            under25Odd = BigDecimal.valueOf(0);
            over25Odd = BigDecimal.valueOf(0);
            under35Odd = BigDecimal.valueOf(0);
            over35Odd = BigDecimal.valueOf(0);
            for (Predictions forecast : listForecasts) {
                winHomeOdd = winHomeOdd.add(forecast.getWinHomeOdd().multiply(factorization));
                drawOdd = drawOdd.add(forecast.getDrawOdd().multiply(factorization));
                winAwayOdd = winAwayOdd.add(forecast.getWinAwayOdd().multiply(factorization));
                under15Odd = under15Odd.add(forecast.getUnder15Odd().multiply(factorization));
                over15Odd = over15Odd.add(forecast.getOver15Odd().multiply(factorization));
                under25Odd = under25Odd.add(forecast.getUnder25Odd().multiply(factorization));
                over25Odd = over25Odd.add(forecast.getOver25Odd().multiply(factorization));
                under35Odd = under35Odd.add(forecast.getUnder35Odd().multiply(factorization));
                over35Odd = over35Odd.add(forecast.getOver35Odd().multiply(factorization));
            }
            
            //Part 2
            List<SdModels> findSdModels = findModel(getClass().getSimpleName());
            List<Predictions> listMatchForecasts = match.getListMatchForecasts();
            for (Predictions cubeForecast : match.getListMatchForecasts()) {
                if (cubeForecast.getModel().equals(findSdModels.get(0))) listMatchForecasts.add(cubeForecast);
            }
            Predictions createMatchForecast;
            if (!listMatchForecasts.isEmpty()) {
                createMatchForecast = listMatchForecasts.get(0);
                LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                    .concat(createMatchForecast.toString())
                    .concat(" was updated."));
            } else {
                createMatchForecast = new Predictions();
                LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                    .concat(createMatchForecast.toString())
                    .concat(" was created."));
            }
            createMatchForecast.setMatchForecast(match);
            createMatchForecast.setModel(findSdModels.get(0));
            createMatchForecast.setAnalyzedAwayMatches(listForecasts.size());
            createMatchForecast.setAnalyzedHomeMatches(listForecasts.size());
//            if (match.isIsHistoric()) createMatchForecast.setIsHistoric(true);
//            if (!match.isIsHistoric()) createMatchForecast.setIsHistoric(false);
            createMatchForecast.setWinHomeOdd(winHomeOdd);
            createMatchForecast.setDrawOdd(drawOdd);
            createMatchForecast.setWinAwayOdd(winAwayOdd);
            createMatchForecast.setUnder15Odd(under15Odd);
            createMatchForecast.setOver15Odd(over15Odd);
            createMatchForecast.setUnder25Odd(under25Odd);
            createMatchForecast.setOver25Odd(over25Odd);
            createMatchForecast.setUnder35Odd(under35Odd);
            createMatchForecast.setOver35Odd(over35Odd);
            //session.persist(createMatchForecast);
        }
        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": Processing data was succesfully."));    
    }
          
    /**
     * 
     * @param name
     * @return 
     */
    public List<SdModels> findModel(String name) {
        List<SdModels> returnModels = session.createQuery(" FROM SdModels m"
                .concat(" WHERE m.calculator = :name"))
                .setParameter("name", name)
                .setMaxResults(1)
                .setFetchSize(1)
                .getResultList();
        if (returnModels != null) {
            if (returnModels.isEmpty()) {
                LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Cannot find a model."));  
                return new LinkedList<>();
            }
            return returnModels;
        } else {
            return new LinkedList<>();
        }   
    }
}
