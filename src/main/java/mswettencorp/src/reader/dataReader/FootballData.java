package mswettencorp.src.reader.dataReader;

import com.mysql.jdbc.StringUtils;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.Odds;
import mswettencorp.src.mapping.SdBookmakers;
import mswettencorp.src.mapping.SdTeams;
import mswettencorp.src.mapping.SystemMappingTeams;
import mswettencorp.src.reader.common.FilesReader;
import org.hibernate.Session;
/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> FootballData <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 26.12.2017
 */
public class FootballData extends FilesReader {

    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final String DELIMITER = ",";
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////          
    
    ////////////////////////////////////////////////////////////////////////////
    // Data Fields
    //////////////////////////////////////////////////////////////////////////// 
    private String div;         //
    private String date;        //
    private String homeTeam;    //
    private String awayTeam;    //
    private int FTHG;           //
    private int FTAG;           //
    private String FTR;         //
    private int HTHG;           //
    private int HTAG;           //
    private String HTR;         //
    //private String referee;   //
    private int HS;             //
    private int AS;             //
    private int HST;            //
    private int AST;            //
    private int HF;             //
    private int AF;             //
    private int HC;             //
    private int AC;             //
    private int HY;             //
    private int AY;             //
    private int HR;             //
    private int AR;             //
    private double B365H;       // Bet365 Home
    private double B365D;       // Bet365 Draw
    private double B365A;       // Bet365 Away
//    private double BSH;         // Blue Square Home
//    private double BSD;         // Blue Square Draw
//    private double BSA;         // Blue Square Away
    private double BWH;         // Bwin Home
    private double BWD;         // Bwin Draw
    private double BWA;         // Bwin Away
//    private double GBH;         // Gamebookers Home
//    private double GBD;         // Gamebookers Draw
//    private double GBA;         // Gamebookers Away
//    private double IWH;         // Interwetten Home
//    private double IWD;         // Interwetten Draw
//    private double IWA;         // Interwetten Away
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////     
    public FootballData(String input) {
        super(input);
    }
     
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean initialize(Session session) {
        if (super.initialize(session)) {
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        }
        return false;
    }
    
    @Override
    public void deInitialize() {
        super.deInitialize();
    }

    @Override
    public void process() {
        try {
            int plus = 0;
            String line;
            String[] split;
            while ((line = bufferedReader.readLine()) != null) {
                split = line.split(DELIMITER);

                if (line.contains("Referee")) {
                    plus = 1;
                }
                if (!line.contains("Div")){
                    if (split.length == 0) {
                        LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Skipped empty row. Check file."));
                        continue;
                    }
                    div = split[0];
                    date = split[1];
                    homeTeam = split[2].replaceAll("'", "");
                    awayTeam = split[3].replaceAll("'", "");
                    if (split[4].isEmpty()) {
                        System.out.println(date + ": " + homeTeam + " : " + awayTeam);
                    }
                    FTHG = Integer.parseInt(split[4]);
                    FTAG = Integer.parseInt(split[5]);
                    FTR = split[6].replaceAll("'", "");
                    HTHG = Integer.parseInt(split[7]);
                    HTAG = Integer.parseInt(split[8]);
                    HTR = split[9].replaceAll("'", "");
                    //referee = split[10].replaceAll("'", "");
                    HS = ((split[10 + plus].isEmpty()) ? 0 : Integer.parseInt(split[10 + plus]));
                    AS = ((split[11 + plus].isEmpty()) ? 0 : Integer.parseInt(split[11 + plus]));
                    HST = ((split[12 + plus].isEmpty()) ? 0 : Integer.parseInt(split[12 + plus]));
                    AST = ((split[13 + plus].isEmpty()) ? 0 : Integer.parseInt(split[13 + plus]));
                    HF = ((split[14 + plus].isEmpty()) ? 0 : Integer.parseInt(split[14 + plus]));
                    AF = ((split[15 + plus].isEmpty()) ? 0 : Integer.parseInt(split[15 + plus]));
                    HC = ((split[16 + plus].isEmpty()) ? 0 : Integer.parseInt(split[16 + plus]));
                    AC = ((split[17 + plus].isEmpty()) ? 0 : Integer.parseInt(split[17 + plus]));
                    HY = ((split[18 + plus].isEmpty()) ? 0 : Integer.parseInt(split[18 + plus]));
                    AY = ((split[19 + plus].isEmpty()) ? 0 : Integer.parseInt(split[19 + plus]));
                    HR = ((split[20 + plus].isEmpty()) ? 0 : Integer.parseInt(split[20 + plus]));
                    AR = ((split[21 + plus].isEmpty()) ? 0 : Integer.parseInt(split[21 + plus]));
                    B365H = ((split[22 + plus].isEmpty()) ? 2.0 : Double.parseDouble(split[22 + plus]));
                    B365D = ((split[23 + plus].isEmpty()) ? 2.0 : Double.parseDouble(split[23 + plus]));
                    B365A = ((split[24 + plus].isEmpty()) ? 2.0 : Double.parseDouble(split[24 + plus]));
                    BWH = ((split[25 + plus].isEmpty()) ? 2.0 : Double.parseDouble(split[25 + plus]));
                    BWD = ((split[26 + plus].isEmpty()) ? 2.0 : Double.parseDouble(split[26 + plus]));
                    BWA = ((split[27 + plus].isEmpty()) ? 2.0 : Double.parseDouble(split[27 + plus]));
                    
                    SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy");
                    Date parsedDate = format.parse(date);
                    java.sql.Date sqlDate = new java.sql.Date(parsedDate.getTime());
                    List<SystemMappingTeams> listMappedHomeTeam = mappingTeamsTeam(homeTeam);
                    List<SystemMappingTeams> listMappedAwayTeam = mappingTeamsTeam(awayTeam);
                    if (listMappedHomeTeam.isEmpty() || listMappedAwayTeam.isEmpty()) {
                        continue;
                    }
                    SystemMappingTeams mappedHomeTeam = listMappedHomeTeam.get(0);
                    SystemMappingTeams mappedAwayTeam = listMappedAwayTeam.get(0);
                    if (StringUtils.isNullOrEmpty(mappedHomeTeam.getOriginalName())) {
                        LOGGER.log(Level.WARNING, getClass().getSimpleName()
                            .concat(": Skipped row. Cannot identify original team name: ")
                            .concat(mappedHomeTeam.getMappingName()));
                        continue;
                    }
                    if (StringUtils.isNullOrEmpty(mappedAwayTeam.getOriginalName())) {
                        LOGGER.log(Level.WARNING, getClass().getSimpleName()
                            .concat(": Skipped row. Cannot identify original team name: ")
                            .concat(mappedAwayTeam.getMappingName()));
                        continue;
                    }
                    List<SdTeams> listHomeTeam = findTeam(mappedHomeTeam.getOriginalName());
                    List<SdTeams> listAwayTeam = findTeam(mappedAwayTeam.getOriginalName());
                    if (listHomeTeam.isEmpty() || listAwayTeam.isEmpty()) {
                        continue;
                    }
                    SdTeams findHomeTeam = listHomeTeam.get(0);
                    SdTeams findAwayTeam = listAwayTeam.get(0);
                    List<Matches> listMatches = findMatch(sqlDate, findHomeTeam, findAwayTeam);
                    if (!listMatches.isEmpty()) {
                        Matches updateMatch = listMatches.get(0);
                        updateMatch.setFullTimeAwayGoals(FTAG);
                        updateMatch.setFullTimeHomeGoals(FTHG);
                        updateMatch.setFullTimeResult(FTR);
                        updateMatch.setHalfTimeAwayGoals(HTAG);
                        updateMatch.setHalfTimeHomeGoals(HTHG);
                        updateMatch.setHalfTimeResult(HTR);
                        updateMatch.setAwayTeamCorners(AC);
                        updateMatch.setAwayTeamFouls(AF);
                        //updateMatch.setAwayTeamOffsides(AO);
                        updateMatch.setAwayTeamRedCards(AR);
                        updateMatch.setAwayTeamShots(AS);
                        updateMatch.setAwayTeamShotsOt(AST);
                        updateMatch.setAwayTeamYellowCards(AY);
                        updateMatch.setHomeTeamCorners(HC);
                        updateMatch.setHomeTeamFouls(HF);
                        //updateMatch.setHomeTeamOffsides(HO);
                        updateMatch.setHomeTeamRedCards(HR);
                        updateMatch.setHomeTeamShots(HS);
                        updateMatch.setHomeTeamShotsOt(HST);
                        updateMatch.setHomeTeamYellowCards(HY);
                        updateMatch.setUpdatedBy(getClass().getSimpleName());
                        session.persist(updateMatch);
                        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                            .concat(updateMatch.toString())
                            .concat(" was updated."));
                        
                        List<Odds> listOdds = updateMatch.getListOdds();
                        if (listOdds.isEmpty()) {
                            List<SdBookmakers> listBookmakers = session.createQuery(" FROM SdBookmakers bm"
                                + " WHERE bm.name = :firstName"
                                + " OR bm.name = :secondName")
                                .setParameter("firstName", "Bet365")
                                .setParameter("secondName", "Bwin")
                                .getResultList();
                            for (SdBookmakers bookmaker : listBookmakers) {
                                Odds createOdd = new Odds();
                                createOdd.setIsHistoric(true);
                                createOdd.setBookmaker(bookmaker);
                                createOdd.setMatchOdds(updateMatch);
                                switch (bookmaker.getName()) {
                                    case "Bet365":
                                        createOdd.setWinHomeOdd(BigDecimal.valueOf(B365H * (1 - bookmaker.getTaxes().doubleValue())));
                                        createOdd.setDrawOdd(BigDecimal.valueOf(B365D * (1 - bookmaker.getTaxes().doubleValue())));
                                        createOdd.setWinAwayOdd(BigDecimal.valueOf(B365A * (1 - bookmaker.getTaxes().doubleValue())));
                                        break;
                                    case "Bwin":
                                        createOdd.setWinHomeOdd(BigDecimal.valueOf(BWH * (1 - bookmaker.getTaxes().doubleValue())));
                                        createOdd.setDrawOdd(BigDecimal.valueOf(BWD * (1 - bookmaker.getTaxes().doubleValue())));
                                        createOdd.setWinAwayOdd(BigDecimal.valueOf(BWA * (1 - bookmaker.getTaxes().doubleValue())));
                                        break;
                                    default:
                                        break;
                                }                                   
                                session.persist(createOdd);
                                LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                                    .concat(" Historic odd ")
                                    .concat(createOdd.toString())
                                    .concat(" was created."));
                            }
                        }                        
                    } else {
                        LOGGER.log(Level.SEVERE, getClass().getSimpleName().concat(": Cannot find a match."));
                    } 
                }
            }
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": Processing data was succesfully."));
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        } catch (ParseException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        }
    }
        
    /**
     * 
     * @param startDate
     * @param homeTeam
     * @param awayTeam
     * @return 
     */
    public List<Matches> findMatch(Date startDate, SdTeams homeTeam, SdTeams awayTeam) {
        Calendar calandar = Calendar.getInstance();
        calandar.setTime(startDate);
        calandar.add(Calendar.DATE, 1);
        Date endDate = calandar.getTime();
        List<Matches> returnMatches = session.createQuery(" FROM Matches m"
                + " WHERE m.startDate > :startDate"
                + " and m.endDate < :endDate"
                + " and m.homeTeam = :home"
                + " and m.awayTeam = :away")
                .setParameter("startDate", startDate)
                .setParameter("endDate", endDate)
                .setParameter("home", homeTeam)
                .setParameter("away", awayTeam)
                .setMaxResults(1)
                .setFetchSize(1)
                .getResultList();
        if (returnMatches.isEmpty()) {
            LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Cannot find a Match."));  
            return new LinkedList<>();
        }
        return returnMatches;    
    }
        
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
