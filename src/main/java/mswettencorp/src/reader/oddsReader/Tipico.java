package mswettencorp.src.reader.oddsReader;

import java.net.URISyntaxException;
import java.util.logging.Level;
import mswettencorp.src.reader.common.OddsReader;
import org.apache.http.client.utils.URIBuilder;
import org.hibernate.Session;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> Tipico <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 13.01.2017
 */
public class Tipico extends OddsReader {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final String URL = "www.tipico.de/";
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////          
    
    ////////////////////////////////////////////////////////////////////////////
    // Data Fields
    //////////////////////////////////////////////////////////////////////////// 

    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public Tipico(String input) {
        super(input);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean initialize(Session session) {
        builder = new URIBuilder();
        builder.setScheme("https").setHost(URL).setPath(input);
        try {
            uri = builder.build();
        } catch (URISyntaxException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        }
        if (super.initialize(session)) {
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        }
        return false;
    }
    
    @Override
    public void deInitialize() {
        super.deInitialize();
    }
    
    @Override
    public void process() {   
        Elements oddElements = parseDoc.getElementsByClass("multi_row");
        Elements matchElements = parseDoc.getElementsByClass("t_cell w_128 left");
        int counter = 2;
        MatchOdds createMatchOdd = new MatchOdds();
        for (Element teamElement : matchElements) {
            if (counter % 2 == 0) {
                createMatchOdd = new MatchOdds();
                createMatchOdd.homeTeam = teamElement.ownText().replace(".", "");
            } else {
                createMatchOdd.awayTeam = teamElement.ownText().replace(".", "");
                listMatchOdds.add(createMatchOdd);
            }
            counter++;
        }
        for (int i = 0; i < listMatchOdds.size(); i++) {
            MatchOdds findMatchOdd = listMatchOdds.get(i);
            findMatchOdd.winHomeOdd = Double.parseDouble(oddElements.get(i).child(0).ownText().replace(",", ".")); 
            findMatchOdd.drawOdd = Double.parseDouble(oddElements.get(i).child(1).ownText().replace(",", "."));
            findMatchOdd.winAwayOdd = Double.parseDouble(oddElements.get(i).child(2).ownText().replace(",", "."));
        }
        super.process();
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
