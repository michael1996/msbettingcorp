package mswettencorp.src.processor;

import java.util.logging.Level;
import java.util.logging.Logger;
import mswettencorp.src.util.HibernateUtil;
import mswettencorp.src.util.LoggerUtil;
import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> DataProcessor <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 08.01.2018
 */
public class DataProcessor {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final Logger LOGGER = LoggerUtil.getLogger();
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    public static void main(String[] args) {
        final long timeStart = System.currentTimeMillis();
        Options options = new Options();
        Option ctxOption = new Option("ctx", "ctx", true, "input ctx");
        ctxOption.setRequired(true);
        options.addOption(ctxOption);
        Option ctxTypeOption = new Option("ctxType", "ctxType", true, "input ctxType");
        ctxTypeOption.setRequired(false);
        options.addOption(ctxTypeOption);
        Option ctxValueOption = new Option("ctxValue", "ctxValue", true, "input ctxValue");
        ctxValueOption.setRequired(false);
        options.addOption(ctxValueOption);    
        
        CommandLineParser parser = new BasicParser();
        CommandLine commandLine;
        try {
            commandLine = parser.parse(options, args);
        } catch (ParseException ex) {
            LOGGER.log(Level.SEVERE, null, ex.getMessage());
            System.exit(1);
            return;
        }
        String ctx = ((args.length >= 1) ? commandLine.getOptionValue("ctx") : "");
        String ctxType = ((args.length >= 3) ? commandLine.getOptionValue("ctxType") : "");
        String ctxValue = ((args.length >= 5) ? commandLine.getOptionValue("ctxValue") : "");
        
        LOGGER.log(Level.INFO, ": Starting DataProcessor [".concat(ctx).concat("; ")
            .concat(ctxType).concat("; ")
            .concat(ctxValue).concat("]"));
          
        switch(ctx) {
            case "MPDownloadProcessor":
                MPDownloadProcessor mpDownloadProcessor = new MPDownloadProcessor();
                if (mpDownloadProcessor.initialize()) mpDownloadProcessor.process(ctxType, ctxValue); 
                mpDownloadProcessor.deInitialize();
                break;
            case "MPDataProcessor":
                MPDataProcessor mpDataProcessor = new MPDataProcessor();
                if (mpDataProcessor.initialize()) mpDataProcessor.process(ctxType, ctxValue); 
                mpDataProcessor.deInitialize();
                break;
            case "MPForecastProcessor":
                MPForecastProcessor mpForecastProcessor = new MPForecastProcessor();
                if (mpForecastProcessor.initialize()) mpForecastProcessor.process(ctxType, ctxValue);
                mpForecastProcessor.deInitialize();
                break;
            case "MPBettingProcessor":
                MPBettingProcessor mpBettingProcessor = new MPBettingProcessor();
                if (mpBettingProcessor.initialize()) mpBettingProcessor.process(ctxType, ctxValue);
                mpBettingProcessor.deInitialize();
                break;
            case "MPCubeProcessor":
                MPCubeProcessor mpCubeProcessor = new MPCubeProcessor();
                if (mpCubeProcessor.initialize()) mpCubeProcessor.process(ctxType, ctxValue);
                mpCubeProcessor.deInitialize();
                break;
            case "MPLearningProcessor":
                MPLearningProcessor mpLearningProcessor = new MPLearningProcessor();
                if (mpLearningProcessor.initialize()) mpLearningProcessor.process(ctxType, ctxValue);
                mpLearningProcessor.deInitialize();
                break;    
            default: 
                LOGGER.log(Level.SEVERE," Cannot find the specified processor.");
                break;
        }        
        HibernateUtil.shutdown();
        final long timeEnd = System.currentTimeMillis(); 
        LOGGER.log(Level.INFO," Total time: ".concat(Long.toString((timeEnd - timeStart) / 1000)).concat("s"));
    }
}
