package mswettencorp.src.mapping;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> SdSeasons <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 16.07.2017
 */
@Entity
@Immutable
@Table
public class SdSeasons implements java.io.Serializable {

    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final long serialVersionUID = 2393561028971838562L;

    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
    
    @Column (name = "name", nullable = false, length = 50)
    private String name;

    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public SdSeasons() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof SdSeasons)) return false;
        SdSeasons other = (SdSeasons) obj;
        if( name == null ) {
            if( other.name != null ) {
                return false;
            }
        } else if( !name.equals( other.name ) ) {
            return false;
        }
        return true;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}