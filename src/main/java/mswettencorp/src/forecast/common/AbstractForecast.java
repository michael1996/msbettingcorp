package mswettencorp.src.forecast.common;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.SdTeams;
import mswettencorp.src.util.LoggerUtil;
import org.hibernate.Session;

/**
 *
 * @author Michael Werner
 */
public abstract class AbstractForecast {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    protected static final Logger LOGGER = LoggerUtil.getLogger();
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////          
    protected Session session; 
    
    ////////////////////////////////////////////////////////////////////////////
    // Data Fields
    //////////////////////////////////////////////////////////////////////////// 
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    //////////////////////////////////////////////////////////////////////////// 
    public AbstractForecast() {

    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    //////////////////////////////////////////////////////////////////////////// 
    /**
     * 
     * @param session 
     * @return  
     */
    public boolean initialize(Session session) {
        this.session = session;
        return true;
    }

    public void deInitialize() {
        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": deinitialized succesfully."));
    }
    
    public abstract void process();
        
    /**
     * 
     * @param session
     * @param parmLoadingBehavior
     * @param isHomeTeam
     * @return 
     */
    public abstract List<Matches> loading(Session session, String parmLoadingBehavior, boolean isHomeTeam);
    
    /**
     * 
     * @param parmWeightingBehavior
     * @param listMatches
     * @return 
     */
    public abstract List<Double> weighting(String parmWeightingBehavior, List<Matches> listMatches, SdTeams team);

    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
