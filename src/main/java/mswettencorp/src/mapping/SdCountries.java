package mswettencorp.src.mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> SdCountries <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 16.07.2017
 */
@Entity
@Immutable
@Table
public class SdCountries implements java.io.Serializable {

    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final long serialVersionUID = 5670636938928103944L;
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) 
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "continent", nullable = false)
    private SdContinents continent;
    
    @Column (name = "name", nullable = false, length = 100)
    private String name;

    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public SdCountries() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof SdCountries)) return false;
        SdCountries other = (SdCountries) obj;
        if( name == null ) {
            if( other.name != null ) {
                return false;
            }
        } else if( !name.equals( other.name ) ) {
            return false;
        }
        return true;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SdContinents getContinent() {
        return continent;
    }

    public void setContinent(SdContinents continent) {
        this.continent = continent;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
