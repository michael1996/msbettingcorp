package mswettencorp.src.mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> SystemBetUsers <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 08.01.2018
 */
@Entity
@Immutable
@Table
public class SystemBetUsers implements java.io.Serializable {
   
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    /////////////////////////////////////////////////%///////////////////////////
    private static final long serialVersionUID = -5280171458285798273L;
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bookmaker", nullable = false)
    private SdBookmakers bookmaker;
    
    @Column (name = "username", nullable = false, length = 100)
    private String username;
    
    @Column (name = "password", nullable = false, length = 100)
    private String password;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public SystemBetUsers() {

    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((bookmaker == null) ? 0 : bookmaker.hashCode());
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof SystemBetUsers)) return false;
        SystemBetUsers other = (SystemBetUsers) obj;
        if( bookmaker == null ) {
            if( other.bookmaker != null ) {
                return false;
            }
        } else if( !bookmaker.equals( other.bookmaker ) ) {
            return false;
        }
        if( username == null ) {
            if( other.username != null ) {
                return false;
            }
        } else if( !username.equals( other.username ) ) {
            return false;
        }
        return true;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public SdBookmakers getBookmaker() {
        return bookmaker;
    }

    public void setBookmaker(SdBookmakers bookmaker) {
        this.bookmaker = bookmaker;
    }
}
