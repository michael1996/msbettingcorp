package mswettencorp.src.mapping;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Formula;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> Bets <br>
 *
 * @author Michael Werner
 * @version v1.1
 * @since 18.07.2017
 */
@Entity
@Table
public class Bets implements java.io.Serializable {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final long serialVersionUID = -5280171458285798279L;
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "betMatch", nullable = false)
    private Matches betMatch;
        
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "betUser", nullable = false)
    private SystemBetUsers betUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "betType", nullable = false)
    private SdBetTyps betType;
    
    @Column(name = "stake", nullable = false, precision = 12, scale = 2)
    private BigDecimal stake;
    
    @Column(name = "value", nullable = false, precision = 12, scale = 4)
    private BigDecimal value;
    
    @Column(name = "odd", nullable = false, precision = 12, scale = 4)
    private BigDecimal odd;
    
    @Formula("stake*odd-stake")
    private BigDecimal potentialProfit;
    
    @Column(name = "payout", precision = 12, scale = 2)
    private BigDecimal payout;
    
    @Column(name = "betResult", nullable = false, length = 5)
    private String betResult;
        
    @Column(name = "isCorrect")
    private boolean isCorrect;
    
    @Column(name = "isHistoric")
    private boolean isHistoric;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public Bets() {

    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((betMatch == null) ? 0 : betMatch.hashCode());
        result = prime * result + ((betType == null) ? 0 : betType.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof Bets)) return false;
        Bets other = (Bets) obj;
        if( betMatch == null ) {
            if( other.betMatch != null ) {
                return false;
            }
        } else if( !betMatch.equals( other.betMatch ) ) {
            return false;
        }
        if( betType == null ) {
            if( other.betType != null ) {
                return false;
            }
        } else if( !betType.equals( other.betType ) ) {
            return false;
        }
        return true;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Matches getBetMatch() {
        return betMatch;
    }

    public void setBetMatch(Matches betMatch) {
        this.betMatch = betMatch;
    }

    public SystemBetUsers getBetUser() {
        return betUser;
    }

    public void setBetUser(SystemBetUsers betUser) {
        this.betUser = betUser;
    }

    public SdBetTyps getBetType() {
        return betType;
    }

    public void setBetType(SdBetTyps betType) {
        this.betType = betType;
    }

    public BigDecimal getStake() {
        return stake;
    }

    public void setStake(BigDecimal stake) {
        this.stake = stake;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getOdd() {
        return odd;
    }

    public void setOdd(BigDecimal odd) {
        this.odd = odd;
    }

    public BigDecimal getPotentialProfit() {
        return potentialProfit;
    }

    public void setPotentialProfit(BigDecimal potentialProfit) {
        this.potentialProfit = potentialProfit;
    }

    public BigDecimal getPayout() {
        return payout;
    }

    public void setPayout(BigDecimal payout) {
        this.payout = payout;
    }

    public String getBetResult() {
        return betResult;
    }

    public void setBetResult(String betResult) {
        this.betResult = betResult;
    }

    public boolean isIsCorrect() {
        return isCorrect;
    }
    
    public void setIsCorrect(boolean isCorrect) {    
        this.isCorrect = isCorrect;
    }

    public boolean isIsHistoric() {
        return isHistoric;
    }

    public void setIsHistoric(boolean isHistoric) {
        this.isHistoric = isHistoric;
    }
}
