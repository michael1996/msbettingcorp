package mswettencorp.src.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> LoggerUtil <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 10.01.2018
 */
public class LoggerUtil {
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    protected static final Logger LOGGER = buildLogger();
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Data Fields
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    private static Logger buildLogger() {
        String callingClassName = Thread.currentThread().getStackTrace()[2].getClassName();
        Logger returnLogger;
        InputStream properties = LoggerUtil.class.getClassLoader().getResourceAsStream("logging.properties");
        try {
            synchronized (LoggerUtil.class){
                if (LOGGER == null) {
                    LogManager.getLogManager().readConfiguration(properties);
                    returnLogger = Logger.getLogger(callingClassName);
                    return returnLogger;
                }
            }
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        } catch (SecurityException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        }
        return null;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public static Logger getLogger() {
        return LOGGER;
    }
}