package mswettencorp.src.forecast.weightingBehavior;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.SdTeams;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> EqualTopicality <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 11.02.2018
 */
public class EqualTopicality implements WeightingBehavior {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    private List<Double> listWeighting;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public EqualTopicality() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public List<Double> weighting(List<Matches> listMatches, SdTeams team) {
        listWeighting = new ArrayList<>(listMatches.size());
        for (Matches match : listMatches) {
            listWeighting.add(1.0);
        }
        if (listWeighting.isEmpty()) {
            return new LinkedList<>();
        } else {
            return listWeighting;
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
