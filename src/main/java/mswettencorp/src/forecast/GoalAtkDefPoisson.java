package mswettencorp.src.forecast;

import java.util.logging.Level;
import mswettencorp.src.forecast.common.MatchForecast;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.SdModels;
import org.apache.commons.math3.distribution.PoissonDistribution;
import org.apache.commons.math3.stat.descriptive.summary.Sum;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> GoalAtkDefPoisson <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 15.02.2018
 */
public class GoalAtkDefPoisson extends MatchForecast {
   
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
        
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    protected Sum atkHome;
    protected Sum defHome;
    protected Sum atkAway;
    protected Sum defAway;
    protected Sum homeWeighting;
    protected Sum awayWeighting;
    protected PoissonDistribution homePoissonDistribution;
    protected PoissonDistribution awayPoissonDistribution;
    protected double[][] resultMatrix = new double[7][7];
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public GoalAtkDefPoisson(Matches match, SdModels model) {
        super(match, model);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean initialize(Session session) {
        if (super.initialize(session)) {
            atkHome = new Sum();
            defHome = new Sum();
            atkAway = new Sum();
            defAway = new Sum();
            homeWeighting = new Sum();
            awayWeighting = new Sum();
            
            int iterator = (analyzedHomeTeamMatches.size() > analyzedAwayTeamMatches.size() 
                    ? analyzedHomeTeamMatches.size() : analyzedAwayTeamMatches.size());
            for (int i = 0; i < iterator; i++) {
                if (i < analyzedHomeTeamMatches.size()) {
                    if (match.getHomeTeam().getName().equals(analyzedHomeTeamMatches.get(i).getHomeTeam().getName())) {
                        atkHome.increment((double)analyzedHomeTeamMatches.get(i).getFullTimeHomeGoals()
                            * weightingHomeTeamMatches.get(weightingCounterHome));
                        defHome.increment((double)analyzedHomeTeamMatches.get(i).getFullTimeAwayGoals()
                            * weightingHomeTeamMatches.get(weightingCounterHome));
                    } else {
                        atkHome.increment((double)analyzedHomeTeamMatches.get(i).getFullTimeAwayGoals()
                            * weightingHomeTeamMatches.get(weightingCounterHome));
                        defHome.increment((double)analyzedHomeTeamMatches.get(i).getFullTimeHomeGoals()
                            * weightingHomeTeamMatches.get(weightingCounterHome));
                    }
                    homeWeighting.increment(weightingHomeTeamMatches.get(weightingCounterHome));
                    weightingCounterHome++;
                }
                
                if (i < analyzedAwayTeamMatches.size()) {
                    if (match.getAwayTeam().getName().equals(analyzedAwayTeamMatches.get(i).getAwayTeam().getName())) {
                        atkAway.increment((double)analyzedAwayTeamMatches.get(i).getFullTimeAwayGoals()
                            * weightingAwayTeamMatches.get(weightingCounterAway));
                        defAway.increment((double)analyzedAwayTeamMatches.get(i).getFullTimeHomeGoals()
                            * weightingAwayTeamMatches.get(weightingCounterAway));
                    } else {
                        atkAway.increment((double)analyzedAwayTeamMatches.get(i).getFullTimeHomeGoals()
                            * weightingAwayTeamMatches.get(weightingCounterAway));
                        defAway.increment((double)analyzedAwayTeamMatches.get(i).getFullTimeAwayGoals()
                            * weightingAwayTeamMatches.get(weightingCounterAway));
                    }
                    awayWeighting.increment(weightingAwayTeamMatches.get(weightingCounterAway));
                    weightingCounterAway++;
                }
            }
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        }
        return false;
    }
    
    @Override
    public void deInitialize() {
        super.deInitialize();
    }
    
    @Override
    public void process() {
        homePoissonDistribution = new PoissonDistribution((atkHome.getResult() / homeWeighting.getResult() 
                + defAway.getResult() / awayWeighting.getResult()) / 2);
        awayPoissonDistribution = new PoissonDistribution((atkAway.getResult() / awayWeighting.getResult() 
                + defHome.getResult() / homeWeighting.getResult()) / 2);
        System.out.println(atkHome.getResult() / homeWeighting.getResult() + "  " + defAway.getResult() / awayWeighting.getResult());
        System.out.println(atkHome.getResult() + "  " + defAway.getResult());
        System.out.println(atkAway.getResult() / awayWeighting.getResult() + "  " + defHome.getResult() / homeWeighting.getResult());
        System.out.println(atkAway.getResult() + "  " + defHome.getResult());
        for (int i = 0; i < resultMatrix.length; i++) {
            for (int j = 0; j < resultMatrix.length; j++) {
                resultMatrix[i][j] = homePoissonDistribution.probability(j) * awayPoissonDistribution.probability(i);
//                System.out.printf("%.2f ", resultMatrix[i][j]);
                if (j > i) homeWinOdd += resultMatrix[i][j];
                if (j == i) drawOdd += resultMatrix[i][j];
                if (j < i)  awayWinOdd += resultMatrix[i][j];
                if (j + i < 1.5) under15Odd += resultMatrix[i][j];
                if (j + i > 1.5) over15Odd += resultMatrix[i][j];
                if (j + i < 2.5) under25Odd += resultMatrix[i][j];
                if (j + i > 2.5) over25Odd += resultMatrix[i][j];
                if (j + i < 3.5) under35Odd += resultMatrix[i][j];
                if (j + i > 3.5) over35Odd += resultMatrix[i][j];
            }
//            System.out.println("");
        }   
        double sumThreeWays = homeWinOdd + drawOdd + awayWinOdd;
//        System.out.printf("%.2f %.2f    %.2f    %.2f\n", sumThreeWays, homeWinOdd, drawOdd, awayWinOdd);
        homeWinOdd = ((homeWinOdd > 0) ? 1 / (homeWinOdd / sumThreeWays) : 0);
        drawOdd = ((drawOdd > 0) ? 1 / (drawOdd / sumThreeWays) : 0);
        awayWinOdd = ((awayWinOdd > 0) ? 1 / (awayWinOdd / sumThreeWays) : 0);
        double sum15 = under15Odd + over15Odd;
        under15Odd = ((under15Odd > 0) ? 1 / (under15Odd / sum15) : 0);
        over15Odd = ((over15Odd > 0) ? 1 / (over15Odd / sum15) : 0);
        double sum25 = under25Odd + over25Odd;
        under25Odd = ((under25Odd > 0) ? 1 / (under25Odd / sum25) : 0);
        over25Odd = ((over25Odd > 0) ? 1 / (over25Odd / sum25) : 0);
        double sum35 = under35Odd + over35Odd;
        under35Odd = ((under35Odd > 0) ? 1 / (under35Odd / sum35) : 0);
        over35Odd = ((over35Odd > 0) ? 1 / (over35Odd / sum35) : 0);

//        System.out.printf("%.2f %.2f    %.2f    %.2f\n", sumThreeWays, homeWinOdd, drawOdd, awayWinOdd);
//        System.out.println("------------------");
        if (homeWinOdd > 0 && drawOdd > 0 && awayWinOdd > 0
                && under15Odd > 0 && over15Odd > 0
                && under25Odd > 0 && over25Odd > 0
                && under35Odd > 0 && over35Odd > 0) {
            super.process();
        } else {
            LOGGER.log(Level.SEVERE, getClass().getSimpleName().concat(" Cannot create match forecast because one Odd is 0."));
        }
    }
   
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    //////////////////////////////////////////////////////////////////////////// 
}
