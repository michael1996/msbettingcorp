package mswettencorp.src.forecast.common;

import java.util.List;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.SdTeams;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> OddsForecast <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 13.01.2018
 */
public class OddsForecast extends AbstractForecast {
   
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
        
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public OddsForecast() {

    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean initialize(Session session) {
        return super.initialize(session);
    }
    
    @Override
    public void deInitialize() {
        super.deInitialize();
    }
    
    @Override
    public void process() {
        
    }
    
    @Override
    public List<Matches> loading(Session session, String parmLoadingBehavior, boolean isHomeTeam) {
        return null;
    }
    
    @Override
    public List<Double> weighting(String parmWeightingBehavior, List<Matches> match, SdTeams team) {
        return null;
    }
   
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    
}
