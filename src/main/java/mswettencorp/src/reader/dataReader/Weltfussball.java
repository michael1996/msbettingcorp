package mswettencorp.src.reader.dataReader;

import com.mysql.jdbc.StringUtils;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import mswettencorp.src.mapping.Matchdays;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.SdCompetitions;
import mswettencorp.src.mapping.SdTeams;
import mswettencorp.src.reader.common.HtmlReader;
import org.apache.http.client.utils.URIBuilder;
import org.hibernate.Session;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> Weltfussball <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 26.12.2017
 */
public class Weltfussball extends HtmlReader {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final String URL = "www.weltfussball.de/";
    private static final String PATH = "alle_spiele/";
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////          
    
    ////////////////////////////////////////////////////////////////////////////
    // Data Fields
    //////////////////////////////////////////////////////////////////////////// 
    private int matchday;
    private java.sql.Date sqlDate;
    private java.sql.Time sqlTime;
    private String homeTeam;
    private String awayTeam;       
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public Weltfussball(String input) {
        super(input);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean initialize(Session session) {
        builder = new URIBuilder();
        builder.setScheme("https").setHost(URL).setPath(PATH.concat(input));
        try {
            uri = builder.build();
        } catch (URISyntaxException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        }
        if (super.initialize(session)) {
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        }
        return false;
    }
    
    @Override
    public void deInitialize() {
        super.deInitialize();
    }
    
    @Override
    public void process() {
        String[] inputSplit = input.split("-");
        String league = "";
        String correctedSeason = "";
        switch(inputSplit.length) {
            case 5:
                league = inputSplit[1].concat(inputSplit[2]);
                correctedSeason = correctSeason(inputSplit[3], inputSplit[4]);
                break;
            case 4:  
                league = inputSplit[1];
                correctedSeason = correctSeason(inputSplit[2], inputSplit[3]);
                break;
            case 3:
                league = inputSplit[0];
                correctedSeason = correctSeason(inputSplit[1], inputSplit[2]);
                break;
            default:
                LOGGER.log(Level.WARNING, getClass().getSimpleName()
                    .concat(": Input cannot be processed because of wrong length."));
                break;
        }     
        if (StringUtils.isNullOrEmpty(correctedSeason)) {
            return;
        }
        Elements tbodyElement = parseDoc.getElementsByTag("tbody");
        Elements trElement = tbodyElement.get(1).select("tr");
        
        for (Element element : trElement) {
            Elements hrefElement = element.select("a[href]");
            if (hrefElement.get(0).ownText().contains("Spieltag")) {
                String split[];
                split = hrefElement.get(0).ownText().split(". ");
                matchday = Integer.parseInt(split[0]);
            } else {
                try {
                    String date;
                    String time;
                    DateFormat dateFormatter = new SimpleDateFormat("dd.MM.yyyy");
                    DateFormat timeFormatter = new SimpleDateFormat("HH:mm");
                    switch (hrefElement.size()) {
                        case 5:
                            date = hrefElement.get(0).ownText();
                            sqlDate = new java.sql.Date(dateFormatter.parse(date).getTime());
                            time = element.child(1).ownText();
                            sqlTime = new java.sql.Time(timeFormatter.parse(time).getTime());
                            homeTeam = hrefElement.get(1).ownText();
                            awayTeam = hrefElement.get(2).ownText();
                            break;
                        case 4:
                            if (!hrefElement.get(2).ownText().trim().contains(":")) {
                                date = hrefElement.get(0).ownText();
                                sqlDate = new java.sql.Date(dateFormatter.parse(date).getTime());
                                time = element.child(1).ownText();
                                sqlTime = new java.sql.Time(timeFormatter.parse(time).getTime());
                                homeTeam = hrefElement.get(1).ownText();
                                awayTeam = hrefElement.get(2).ownText();
                            } else {
                                homeTeam = hrefElement.get(0).ownText();
                                awayTeam = hrefElement.get(1).ownText();
                            }
                            break;
                        case 3:
                            if (isValidDate(hrefElement.get(0).ownText().trim())) {
                                date = hrefElement.get(0).ownText();
                                sqlDate = new java.sql.Date(dateFormatter.parse(date).getTime());
                                time = element.child(1).ownText();
                                sqlTime = new java.sql.Time(timeFormatter.parse(time).getTime());
                                homeTeam = hrefElement.get(1).ownText();
                                awayTeam = hrefElement.get(2).ownText();
                            } else {
                                homeTeam = hrefElement.get(0).ownText();
                                awayTeam = hrefElement.get(1).ownText();
                            }
                            break;
                        case 2:
                            homeTeam = hrefElement.get(0).ownText();
                            awayTeam = hrefElement.get(1).ownText();
                            break;
                        default:
                            LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Case is not handled."));
                            break;
                    }
                } catch (ParseException ex) {
                    LOGGER.log(Level.SEVERE, ex.getMessage());
                }
                
                // hier muss for-lopp enden
                SdCompetitions findCompetition = findCompetition(league, correctedSeason).get(0);
                List<Matchdays> listMatchdays = findMatchday(matchday, findCompetition);
                Matchdays findMatchday = null; 
                if (!listMatchdays.isEmpty()) {
                    findMatchday = listMatchdays.get(0);
                }
                List<SdTeams> listHomeTeam = findTeam(homeTeam);
                List<SdTeams> listAwayTeam = findTeam(awayTeam);
                SdTeams findHomeTeam;
                SdTeams findAwayTeam;
                if (listHomeTeam.isEmpty()) {
                    SdTeams createTeam = new SdTeams();
                    createTeam.setName(homeTeam);
                    createTeam.setAwayElo(BigDecimal.valueOf(1500));
                    createTeam.setHomeElo(BigDecimal.valueOf(1500));
                    createTeam.setCreatedBy(getClass().getSimpleName());
                    session.persist(createTeam);
                    findHomeTeam = createTeam;
                    LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                            .concat(createTeam.getName())
                            .concat(" was created."));
                } else {
                    findHomeTeam = listHomeTeam.get(0);
                }
                if (listAwayTeam.isEmpty()) {
                    SdTeams createTeam = new SdTeams();
                    createTeam.setName(awayTeam);
                    createTeam.setAwayElo(BigDecimal.valueOf(1500));
                    createTeam.setHomeElo(BigDecimal.valueOf(1500));
                    createTeam.setCreatedBy(getClass().getSimpleName());
                    session.persist(createTeam);
                    findAwayTeam = createTeam;
                    LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                            .concat(createTeam.getName())
                            .concat(" was created."));
                } else {
                    findAwayTeam = listAwayTeam.get(0);
                }
                List<Matches> listMatches = findMatch(findMatchday, findHomeTeam, findAwayTeam);
                Date startDate = combineDateTime(sqlDate, sqlTime);
                Calendar calandar = Calendar.getInstance();
                calandar.setTime(startDate);
                calandar.add(Calendar.MINUTE, 105);
                Date endDate = calandar.getTime();
                if (!listMatches.isEmpty()) {
                    Matches findMatch = listMatches.get(0);
                    if (findMatch.getStartDate().compareTo(startDate) != 0 
                            && findMatch.getEndDate().compareTo(endDate) != 0) {
                        findMatch.setStartDate(startDate);
                        findMatch.setEndDate(endDate);
                        findMatch.setStatus(checkStatus(findMatch.getStartDate(), findMatch.getEndDate()).get(0));
                        //@TODO Matchday update überdenken
                        if(!findMatch.getMatchday().equals(findMatchday)) {
                            findMatch.setMatchday(findMatchday);
                        }
                        findMatch.setUpdatedBy(getClass().getSimpleName());
                        session.persist(findMatch);
                        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                            .concat(findMatch.toString())
                            .concat(" was updated."));
                    }
                } else {
                    if (findMatchday != null) {
                        findMatchday.setUpdatedBy(getClass().getSimpleName());
                        findMatchday.setEndDate(endDate);
                        findMatchday.setStatus(checkStatus(findMatchday.getStartDate(), findMatchday.getEndDate()).get(0));
                        session.persist(findMatchday);
                        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                            .concat(findMatchday.toString())
                            .concat(" was updated."));
                    } else {
                        Matchdays createMatchday = new Matchdays();
                        createMatchday.setNumber(matchday);
                        createMatchday.setStartDate(startDate);
                        createMatchday.setEndDate(endDate);
                        createMatchday.setStatus(checkStatus(startDate, endDate).get(0));
                        createMatchday.setCompetition(findCompetition);
                        createMatchday.setCreatedBy(getClass().getSimpleName());
                        session.persist(createMatchday);
                        findMatchday = createMatchday;
                        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                            .concat(createMatchday.toString())
                            .concat(" was created."));
                    }
                    Matches createMatch = new Matches();
                    createMatch.setCreatedBy(getClass().getSimpleName());
                    createMatch.setStartDate(startDate);
                    createMatch.setEndDate(endDate);
                    createMatch.setHomeTeam(findHomeTeam);
                    createMatch.setAwayTeam(findAwayTeam);
                    createMatch.setMatchday(findMatchday);
                    createMatch.setStatus(checkStatus(createMatch.getStartDate(), createMatch.getEndDate()).get(0));
                    session.persist(createMatch);
                    LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                        .concat(createMatch.toString())
                        .concat(" was created."));
                }
            }
        }
        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": Processing data was succesfully."));
    }
       
    /**
     * 
     * @param date
     * @return 
     */
    public boolean isValidDate(String date) {
        DateFormat checkDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        checkDateFormat.setLenient(false);
        try {
            checkDateFormat.parse(date.trim());
        } catch (ParseException ex) {
//            LOGGER.log(Level.WARNING, ex.getMessage());
            return false;
        }
        return true;
    }

    /**
     * 
     * @param matchday
     * @param homeTeam
     * @param awayTeam
     * @return 
     */
    public List<Matches> findMatch(Matchdays matchday, SdTeams homeTeam, SdTeams awayTeam) {
        List<Matches> returnMatches = session.createQuery(" FROM Matches m"
                + " WHERE m.matchday = :matchday"
                + " and m.homeTeam = :home"
                + " and m.awayTeam = :away")
                .setParameter("matchday", matchday)
                .setParameter("home", homeTeam)
                .setParameter("away", awayTeam)
                .setMaxResults(1)
                .setFetchSize(1)
                .getResultList();
        if (returnMatches.isEmpty()) {
            LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Cannot find a Match."));  
            return new LinkedList<>();
        }
        return returnMatches;    
    }
    
    /**
     * 
     * @param number
     * @param competition
     * @return 
     */
    public List<Matchdays> findMatchday(long number, SdCompetitions competition) {
        List<Matchdays> returnMatchdays = session.createQuery("SELECT m"
                + " FROM Matchdays m, SdCompetitions c"
                + " WHERE m.competition = c.id"
                + " AND m.number = :number"
                + " AND m.competition = :competition")
                .setParameter("number", number)
                .setParameter("competition", competition)
                .setMaxResults(1)
                .setFetchSize(1)
                .getResultList();  
        if (returnMatchdays.isEmpty()) {
            LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Cannot find a Matchday."));
            return new LinkedList<>();
        }
        return returnMatchdays;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
