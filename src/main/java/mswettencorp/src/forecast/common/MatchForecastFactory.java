package mswettencorp.src.forecast.common;

import java.util.logging.Level;
import mswettencorp.src.forecast.GoalAtkDefPoisson;
import mswettencorp.src.forecast.Result;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.SdModels;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> MatchForecastFactory <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 08.01.2018
 */
public class MatchForecastFactory extends AbstractForecastFactory {
 
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    protected AbstractForecast createCalculator(String ctx, Matches match, SdModels model) {
        AbstractForecast returnCalculator = null;
        if (ctx.equals("Result")) {
            returnCalculator = new Result(match, model);
        } else if (ctx.equals("GoalAtkDefPoisson")) {
            returnCalculator = new GoalAtkDefPoisson(match, model);
        } else {
            LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Cannot create a valid match calculator: ")
                .concat(ctx));
        }
        return returnCalculator;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
