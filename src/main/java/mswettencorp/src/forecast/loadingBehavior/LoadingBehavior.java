package mswettencorp.src.forecast.loadingBehavior;

import java.util.List;
import mswettencorp.src.mapping.Matches;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> LoadingBehavior <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 13.01.2018
 */
public interface LoadingBehavior {
        
    public List<Matches> loading(Session session, Matches match, boolean isHomeTeam);
}
