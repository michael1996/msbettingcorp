package mswettencorp.src.mapping;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import org.hibernate.annotations.Formula;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> Predictions <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 27.12.2017
 */
@Entity
@Table
public class Predictions implements java.io.Serializable {

    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final long serialVersionUID = 4597587983463638363L;
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
    
    @OneToMany(mappedBy="modelForecast", fetch=FetchType.LAZY)
    private List<BetsStaging> listBetsStaging;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "model", nullable = false, updatable = false)
    private SdModels model;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "matchForecast", nullable = false)
    private Matches matchForecast;
    
    @Column(name = "isHistoric", nullable = false)
    private boolean isHistoric;
        
    @Column(name = "winHomeOdd", precision = 12, scale = 4)
    private BigDecimal winHomeOdd;
    
    @Column(name = "drawOdd", precision = 12, scale = 4)
    private BigDecimal drawOdd;
    
    @Column(name = "winAwayOdd", precision = 12, scale = 4)
    private BigDecimal winAwayOdd;
    
    @Formula("(winHomeOdd+drawOdd)/2")
    private BigDecimal homeDrawOdd;
    
    @Formula("(winHomeOdd+winAwayOdd)/2")
    private BigDecimal notDraw;
    
    @Formula("(drawOdd+winAwayOdd)/2")
    private BigDecimal awayDrawOdd;
    
    @Column(name = "under15Odd", precision = 12, scale = 4)
    private BigDecimal under15Odd;
    
    @Column(name = "over15Odd", precision = 12, scale = 4)
    private BigDecimal over15Odd;
    
    @Column(name = "under25Odd", precision = 12, scale = 4)
    private BigDecimal under25Odd;
    
    @Column(name = "over25Odd", precision = 12, scale = 4)
    private BigDecimal over25Odd;
    
    @Column(name = "under35Odd", precision = 12, scale = 4)
    private BigDecimal under35Odd;
    
    @Column(name = "over35Odd", precision = 12, scale = 4)
    private BigDecimal over35Odd;
    
    @Column(name = "analyzedHomeMatches", nullable = false, precision = 18, scale = 0)
    private long analyzedHomeMatches;
    
    @Column(name = "analyzedAwayMatches", nullable = false, precision = 18, scale = 0)
    private long analyzedAwayMatches;
        
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public Predictions() {
        
    }
        
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((model == null) ? 0 : model.hashCode());
        result = prime * result + ((matchForecast == null) ? 0 : matchForecast.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof Predictions)) return false;
        Predictions other = (Predictions) obj;
        if( model == null ) {
            if( other.model != null ) {
                return false;
            }
        } else if( !model.equals( other.model ) ) {
            return false;
        }
        if( matchForecast == null ) {
            if( other.matchForecast != null ) {
                return false;
            }
        } else if( !matchForecast.equals( other.matchForecast ) ) {
            return false;
        }
        return true;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SdModels getModel() {
        return model;
    }

    public void setModel(SdModels model) {
        this.model = model;
    }

    public Matches getMatchForecast() {
        return matchForecast;
    }

    public void setMatchForecast(Matches matchForecast) {
        this.matchForecast = matchForecast;
    }

    public boolean isIsHistoric() {
        return isHistoric;
    }

    public void setIsHistoric(boolean isHistoric) {
        this.isHistoric = isHistoric;
    }

    public BigDecimal getWinHomeOdd() {
        return winHomeOdd;
    }

    public void setWinHomeOdd(BigDecimal winHomeOdd) {
        this.winHomeOdd = winHomeOdd;
    }

    public BigDecimal getDrawOdd() {
        return drawOdd;
    }

    public void setDrawOdd(BigDecimal drawOdd) {
        this.drawOdd = drawOdd;
    }

    public BigDecimal getWinAwayOdd() {
        return winAwayOdd;
    }

    public void setWinAwayOdd(BigDecimal winAwayOdd) {
        this.winAwayOdd = winAwayOdd;
    }

    public BigDecimal getHomeDrawOdd() {
        return homeDrawOdd;
    }

    public BigDecimal getNotDraw() {
        return notDraw;
    }

    public BigDecimal getAwayDrawOdd() {
        return awayDrawOdd;
    }

    public BigDecimal getUnder15Odd() {
        return under15Odd;
    }

    public void setUnder15Odd(BigDecimal under15Odd) {
        this.under15Odd = under15Odd;
    }

    public BigDecimal getOver15Odd() {
        return over15Odd;
    }

    public void setOver15Odd(BigDecimal over15Odd) {
        this.over15Odd = over15Odd;
    }

    public BigDecimal getUnder25Odd() {
        return under25Odd;
    }

    public void setUnder25Odd(BigDecimal under25Odd) {
        this.under25Odd = under25Odd;
    }

    public BigDecimal getOver25Odd() {
        return over25Odd;
    }

    public void setOver25Odd(BigDecimal over25Odd) {
        this.over25Odd = over25Odd;
    }

    public BigDecimal getUnder35Odd() {
        return under35Odd;
    }

    public void setUnder35Odd(BigDecimal under35Odd) {
        this.under35Odd = under35Odd;
    }

    public BigDecimal getOver35Odd() {
        return over35Odd;
    }

    public void setOver35Odd(BigDecimal over35Odd) {
        this.over35Odd = over35Odd;
    }

    public long getAnalyzedHomeMatches() {
        return analyzedHomeMatches;
    }

    public void setAnalyzedHomeMatches(long analyzedHomeMatches) {
        this.analyzedHomeMatches = analyzedHomeMatches;
    }

    public long getAnalyzedAwayMatches() {
        return analyzedAwayMatches;
    }

    public void setAnalyzedAwayMatches(long analyzedAwayMatches) {
        this.analyzedAwayMatches = analyzedAwayMatches;
    }

    public List<BetsStaging> getListBetsStaging() {
        return listBetsStaging;
    }

    public void setListBetsStaging(List<BetsStaging> listBetsStaging) {
        this.listBetsStaging = listBetsStaging;
    }
}
