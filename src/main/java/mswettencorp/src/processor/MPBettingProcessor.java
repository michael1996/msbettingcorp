package mswettencorp.src.processor;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mswettencorp.src.betting.BetStager;
import mswettencorp.src.mapping.Bets;
import mswettencorp.src.mapping.BetsStaging;
import mswettencorp.src.mapping.Capital;
import mswettencorp.src.mapping.SystemBetUsers;
import mswettencorp.src.util.HibernateUtil;
import mswettencorp.src.util.LoggerUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> MPBettingProcessor <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 10.01.2018
 */
public class MPBettingProcessor {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final Logger LOGGER = LoggerUtil.getLogger();
    private static final double PERCENTAGESTAKE = 0.01;
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    private Session session;    
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    /**
     * 
     * @return 
     */
    public boolean initialize() {
        try {
            session = HibernateUtil.startConfiguredSession();  
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        } catch (HibernateException ex) {
            LOGGER.log(Level.SEVERE, null, ex.getMessage());
            return false;
        }
    }
    
    /**
     * 
     */
    public void deInitialize() {
        try {
            session = HibernateUtil.closeSession();
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": deinitialized succesfully."));
        } catch (HibernateException ex) {
            LOGGER.log(Level.SEVERE, null, ex.getMessage());
        } 
    } 
    
    /**
     * 
     * @param ctxType
     * @param ctxValue 
     */
    public void process(String ctxType, String ctxValue) { 
        //Part 1
        BetStager betStager = new BetStager();
        if (betStager.initialize()) betStager.process(false); 
        betStager.deInitialize();

        //Part 2
        //Wird geändert, wenn das Gesamtmodell steht --------------------------->
        List<BetsStaging> listBetsStagingReal = session.createQuery("SELECT bs FROM BetsStaging bs"
            + " INNER JOIN MatchForecasts mf"
            + " ON (bs.modelForecast = mf.id)"
            + " INNER JOIN SdModels m"
            + " ON (mf.model = m.id)"
            + " WHERE mf.isHistoric = false"
            + " AND m.calculator = :calculator"
            + " AND m.loadingBehavior = :behavior")
            .setParameter("calculator", "GoalDiffNormalDistributionEqual")
            .setParameter("behavior", "LongtermSide")
            .getResultList();
        //<---------------------------------------------
        List<BetsStaging> listValueBets = new LinkedList<>();
        for (BetsStaging betsStaged : listBetsStagingReal) {
            if (betsStaged.getValue() != null && !betsStaged.isIsHistoric()) {
                if (betsStaged.getValue().compareTo(BigDecimal.valueOf(0)) > 0) {
                    listValueBets.add(betsStaged);
                }    
            }
        }
        
        //Part 3
        List<Bets> listRealBets = new LinkedList<>();
        for (BetsStaging valueBet : listValueBets) {
            if (valueBet.getBetBookmaker().getName().equals("Unknown")) {
                continue;
            }
            List<SystemBetUsers> listBetUsers = session.createQuery(" FROM SystemBetUsers sbu"
                + " WHERE sbu.bookmaker = :bookmaker")
                .setParameter("bookmaker", valueBet.getBetBookmaker())
                .getResultList();
            SystemBetUsers betUser = null;
            if (!listBetUsers.isEmpty()) {
                if (listBetUsers.size() == 1) betUser = listBetUsers.get(0);
            }
            BigDecimal moneymanagement = BigDecimal.ZERO;
            java.sql.Date sqlDate = new java.sql.Date(Calendar.getInstance().getTime().getTime());
            List<Capital> currentCapitalList = session.createQuery(" FROM Capital c"
                + " WHERE c.created <= :currentDate"
                + " AND c.betAccount = :betAccount")
                .setParameter("currentDate", sqlDate)
                .setParameter("betAccount", betUser)
                .getResultList();
            for (Capital currentCapital : currentCapitalList) {
                moneymanagement = moneymanagement.add(currentCapital.getStake());
            }
            List<Bets> listBets = session.createQuery(" FROM Bets b"
                + " WHERE b.betMatch = :match"
                + " AND b.betType = :betType")
                .setParameter("match", valueBet.getModelForecast().getMatchForecast())
                .setParameter("betType", valueBet.getBetType())
                .getResultList();
            if (listBets != null) {
                if (!listBets.isEmpty()) {
                    LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": Bet already exists."));
                } else {
                    Bets createBet = new Bets();
                    createBet.setBetMatch(valueBet.getModelForecast().getMatchForecast());
                    createBet.setBetType(valueBet.getBetType());
                    createBet.setBetResult(valueBet.getBetResult());
                    createBet.setValue(valueBet.getValue());
                    createBet.setOdd(valueBet.getOdd());
                    createBet.setBetUser(betUser);
                    createBet.setIsHistoric(false);    
                    if (moneymanagement.multiply(BigDecimal.valueOf(PERCENTAGESTAKE)).compareTo(valueBet.getBetBookmaker().getStakeMinimum()) < 0) {
                        createBet.setStake(valueBet.getBetBookmaker().getStakeMinimum());
                    } else {
                        createBet.setStake(moneymanagement.multiply(BigDecimal.valueOf(PERCENTAGESTAKE)));
                    }
                    session.persist(createBet);
                    listRealBets.add(createBet);
                    LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                        .concat(Long.toString(createBet.getId()))
                        .concat(" ")
                        .concat(createBet.getBetMatch().getHomeTeam().getName())
                        .concat(" : ")
                        .concat(createBet.getBetMatch().getAwayTeam().getName())
                        .concat(" ")
                        .concat(createBet.getBetResult())
                        .concat(" Stake: ")
                        .concat(createBet.getStake().toString())
                        .concat(" was created."));
                }
            }
        }
        
        //Part 4
        for (Bets realBet : listRealBets) {
            Capital createCapital = new Capital();
            createCapital.setBetAccount(realBet.getBetUser());
            createCapital.setCurrency("EUR");
            createCapital.setStake(realBet.getStake().negate());
            createCapital.setCreatedBy(getClass().getSimpleName());
            session.persist(createCapital);
        }

        //Part 5
        List<BetsStaging> listBetsStaging = session.createQuery(" FROM BetsStaging bs"
            + " WHERE bs.isHistoric = false")
            .getResultList();
        for (BetsStaging betsStaged : listBetsStaging) {
            if (betsStaged.getModelForecast().getMatchForecast().getStatus().getName().equals("finished")) {
                betsStaged.setIsHistoric(true);
                if (betsStaged.getBetType().getName().equals("3Way")) {
                    if (betsStaged.getBetResult().equals(betsStaged.getModelForecast().getMatchForecast().getFullTimeResult())) {
                        betsStaged.setIsCorrect(true);
                    } else {
                        betsStaged.setIsCorrect(false);
                    }
                }
                if (betsStaged.getBetType().getName().equals("25")) {
                    //Not yet implemented
                }
                session.persist(betsStaged);
                LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                        .concat(Long.toString(betsStaged.getId()))
                        .concat(" isHistoric was set true because match is over."));
            }
        }
        List<Bets> listBets = session.createQuery(" FROM Bets b"
            + " WHERE b.isHistoric = false")
            .getResultList();
        for (Bets bet : listBets) {
            if (bet.getBetMatch().getStatus().getName().equals("finished")) {
                bet.setIsHistoric(true);
                if (bet.getBetType().getName().equals("3Way")) {
                    if (bet.getBetResult().equals(bet.getBetMatch().getFullTimeResult())) {
                        bet.setIsCorrect(true);
                        bet.setPayout(bet.getPotentialProfit());
                    } else {
                        bet.setIsCorrect(false);
                        bet.setPayout(BigDecimal.valueOf(0));
                    }
                }
                if (bet.getBetType().getName().equals("25")) {
                    //Not yet implemented
                }
                session.persist(bet);
                LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                        .concat(Long.toString(bet.getId()))
                        .concat(" isHistoric was set true because match is over."));
                
                Capital createCapital = new Capital();
                createCapital.setBetAccount(bet.getBetUser());
                createCapital.setCurrency("EUR");
                createCapital.setStake(bet.getPayout());
                createCapital.setCreatedBy(getClass().getSimpleName());
                session.persist(createCapital);
                LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                        .concat(Long.toString(createCapital.getId()))
                        .concat(" was created because match is over."));
            }
        }
        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": Betting was succesfully."));
    }
}
