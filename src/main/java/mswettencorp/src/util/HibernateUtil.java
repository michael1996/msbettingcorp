package mswettencorp.src.util;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> HibernateUtil <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 28.12.2017
 */
  
public class HibernateUtil {
  
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    protected static final SessionFactory SESSIONFACTORY = buildSessionFactory();
    protected static final Logger LOGGER = LoggerUtil.getLogger();
    protected static Session session; 
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Data Fields
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    private static SessionFactory buildSessionFactory() {
        try {
            synchronized (SessionFactory.class){
                if (SESSIONFACTORY == null) {
                    return new Configuration().configure().buildSessionFactory();
                }
            }
            return null;
        } catch (HibernateException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
            throw new ExceptionInInitializerError(ex);
        }
    }
  
    public static SessionFactory getSessionFactory() {
        return SESSIONFACTORY;
    }
  
    public static void shutdown() {
        getSessionFactory().close();
        LOGGER.log(Level.INFO, HibernateUtil.class.getSimpleName().concat(": Shutdown."));
    }
      
    /**
     * 
     * @return 
     * @throws HibernateException 
     */
    public static Session startConfiguredSession() throws HibernateException {
        if (!HibernateUtil.getSessionFactory().isOpen()) {
            session = HibernateUtil.getSessionFactory().openSession();  
        } else {
            session = HibernateUtil.getSessionFactory().getCurrentSession();    
        }   
        if (!session.getTransaction().isActive()) {
            session.beginTransaction();
            LOGGER.log(Level.INFO, HibernateUtil.class.getSimpleName().concat(": Begin Transaction."));
        }
        return session;
    }
    
    /**
     * 
     * @return 
     * @throws HibernateException 
     */
    public static Session closeSession()  throws HibernateException{
        if (session.getTransaction().isActive()) {
            session.getTransaction().commit(); 
            LOGGER.log(Level.INFO, HibernateUtil.class.getSimpleName().concat(": Commit Transaction."));
        } else {
            LOGGER.log(Level.WARNING, HibernateUtil.class.getSimpleName().concat(": Cannot commit because transaction is not active."));
        }
        session.close();
        LOGGER.log(Level.INFO, HibernateUtil.class.getSimpleName().concat(": Close Transaction."));
        return session;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
