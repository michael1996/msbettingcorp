package mswettencorp.src.forecast;

import java.util.logging.Level;
import mswettencorp.src.forecast.common.MatchForecast;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.SdModels;
import org.apache.commons.math3.stat.descriptive.summary.Sum;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> Result <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 13.01.2018
 */
public class Result extends MatchForecast {
   
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
        
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    private Sum winHome;
    private Sum drawHome;
    private Sum loseHome;
    private Sum winAway;
    private Sum drawAway;
    private Sum loseAway;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public Result(Matches match, SdModels model) {
        super(match, model);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean initialize(Session session) {
        if (super.initialize(session)) {
            winHome = new Sum();
            drawHome = new Sum();
            loseHome = new Sum();
            winAway = new Sum();
            drawAway = new Sum();
            loseAway = new Sum();
            
            int iterator = (analyzedHomeTeamMatches.size() > analyzedAwayTeamMatches.size() 
                    ? analyzedHomeTeamMatches.size() : analyzedAwayTeamMatches.size());
            for (int i = 0; i < iterator; i++) {
                if (i < analyzedHomeTeamMatches.size()) {
                    if (match.getHomeTeam().getName().equals(analyzedHomeTeamMatches.get(i).getHomeTeam().getName())) {
                        switch(analyzedHomeTeamMatches.get(i).getFullTimeResult()) {
                            case "H":
                                winHome.increment(1 * weightingHomeTeamMatches.get(weightingCounterHome));
                                break;
                            case "D":
                                drawHome.increment(1 * weightingHomeTeamMatches.get(weightingCounterHome));
                                break;
                            case "A":
                                loseHome.increment(1 * weightingHomeTeamMatches.get(weightingCounterHome));
                                break;
                            default:
                                break;
                        }
                    } else {
                        switch(analyzedHomeTeamMatches.get(i).getFullTimeResult()) {
                            case "H":
                                loseHome.increment(1 * weightingHomeTeamMatches.get(weightingCounterHome));
                                break;
                            case "D":
                                drawHome.increment(1 * weightingHomeTeamMatches.get(weightingCounterHome));
                                break;
                            case "A":
                                winHome.increment(1 * weightingHomeTeamMatches.get(weightingCounterHome));
                                break;
                            default:
                                break;
                        }
                    }
                    weightingCounterHome++;
                }
                
                if (i < analyzedAwayTeamMatches.size()) {
                    if (match.getAwayTeam().getName().equals(analyzedAwayTeamMatches.get(i).getAwayTeam().getName())) {
                        switch(analyzedAwayTeamMatches.get(i).getFullTimeResult()) {
                            case "H":
                                loseAway.increment(1 * weightingAwayTeamMatches.get(weightingCounterAway));
                                break;
                            case "D":
                                drawAway.increment(1 * weightingAwayTeamMatches.get(weightingCounterAway));
                                break;
                            case "A":
                                winAway.increment(1 * weightingAwayTeamMatches.get(weightingCounterAway));
                                break;
                            default:
                                break;
                        }
                    } else {
                        switch(analyzedAwayTeamMatches.get(i).getFullTimeResult()) {
                            case "H":
                                winAway.increment(1 * weightingAwayTeamMatches.get(weightingCounterAway));
                                break;
                            case "D":
                                drawAway.increment(1 * weightingAwayTeamMatches.get(weightingCounterAway));
                                break;
                            case "A":
                                loseAway.increment(1 * weightingAwayTeamMatches.get(weightingCounterAway));
                                break;
                            default:
                                break;
                        }
                    }
                    weightingCounterAway++;
                }
            }
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        }
        return false;
    }
    
    @Override
    public void deInitialize() {
        super.deInitialize();
    }
    
    @Override
    public void process() {
        homeWinOdd = ((double)winHome.getResult() + (double)loseAway.getResult()) / 2;
        drawOdd = ((double)drawHome.getResult() + (double)drawAway.getResult()) / 2;
        awayWinOdd = ((double)loseHome.getResult() + (double)winAway.getResult()) / 2;
        
        if (homeWinOdd > 0 && drawOdd > 0 && awayWinOdd > 0) {
            double sum = homeWinOdd + drawOdd + awayWinOdd;
            homeWinOdd = ((homeWinOdd > 0) ? 1 / (homeWinOdd / sum) : 0);
            drawOdd = ((drawOdd > 0) ? 1 / (drawOdd / sum) : 0);
            awayWinOdd = ((awayWinOdd > 0) ? 1 / (awayWinOdd / sum) : 0);
            super.process();
        } else {
            LOGGER.log(Level.SEVERE, getClass().getSimpleName().concat(" Cannot create match forecast because one Odd is 0."));
        }
    }
   
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    //////////////////////////////////////////////////////////////////////////// 
}
