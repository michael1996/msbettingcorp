package mswettencorp.src.forecast.weightingBehavior;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.SdTeams;
import mswettencorp.src.util.LoggerUtil;
import org.apache.commons.math3.analysis.function.Exp;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> ExpTopicality <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 11.02.2018
 */
public class ExpTopicality implements WeightingBehavior {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final int MAX = 1;
    private static final int MIN = 0;
    protected static final Logger LOGGER = LoggerUtil.getLogger();
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    private List<Double> listWeighting;
    private Exp function;
    private double xCoordinate;
    private double xValue;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public ExpTopicality() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public List<Double> weighting(List<Matches> listMatches, SdTeams team) {
        listWeighting = new ArrayList<>(listMatches.size());
        function = new Exp();
        xCoordinate = MAX;
        xValue =(MAX - MIN) / (double)listMatches.size();
        for (Matches match : listMatches) {
            listWeighting.add(function.value(xCoordinate));
            xCoordinate = xCoordinate - xValue;
        }
        if (listWeighting.isEmpty()) {
            return new LinkedList<>();
        } else {
            return listWeighting;
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
