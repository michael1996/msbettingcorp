package mswettencorp.src.processor;

import java.util.logging.Level;
import java.util.logging.Logger;
import mswettencorp.src.betting.BetStager;
import mswettencorp.src.util.HibernateUtil;
import mswettencorp.src.util.LoggerUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> MPLearningProcessor <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 12.02.2018
 */
public class MPLearningProcessor {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final Logger LOGGER = LoggerUtil.getLogger();
    private static final String STARTDATE = "2008-08-09";   
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    private Session session;    
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    /**
     * 
     * @return 
     */
    public boolean initialize() {
        try {
            session = HibernateUtil.startConfiguredSession(); 
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        } catch (HibernateException ex) {
            LOGGER.log(Level.SEVERE, null, ex.getMessage());
            return false;
        }
    }
    
    /**
     * 
     */
    public void deInitialize() {
        try {
            session = HibernateUtil.closeSession(); 
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": deinitialized succesfully."));
        } catch (HibernateException ex) {
            LOGGER.log(Level.SEVERE, null, ex.getMessage());
        } 
    }
    
    /**
     * 
     * @param ctxType
     * @param ctxValue 
     */
    public void process(String ctxType, String ctxValue) {
//        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
//        Date parsedDate;
//        java.sql.Date sqlDate = null;
//        try {
//            parsedDate = format.parse(STARTDATE);
//            sqlDate = new java.sql.Date(parsedDate.getTime());
//        } catch (ParseException ex) {
//            LOGGER.log(Level.SEVERE, null, ex.getMessage());
//        }
//        List<Matches> learningMatches = session.createQuery("SELECT m FROM Matches m"
//                + " JOIN FETCH m.homeTeam"
//                + " JOIN FETCH m.awayTeam"
//                + " WHERE m.isHistoric = true"
//                + " AND m.date >= :startDate"
//                + " ORDER by m.date asc")
//                .setParameter("startDate", sqlDate)
//                .setHibernateFlushMode(FlushMode.COMMIT)
//                .getResultList();     
//        List<SdModels> findModels = session.createQuery(" FROM SdModels m"
//                + " WHERE m.isActive = true"
//                + " AND m.calculator <> 'MPCubeProcessor'")
//                .getResultList();
//        if(!findModels.isEmpty()) {
//            for (SdModels model : findModels) {
//                if (!learningMatches.isEmpty()) {
//                    for (Matches match : learningMatches) {
//                        AbstractForecastFactory matchForecastFactory = new MatchForecastFactory();
//                        AbstractForecast calculator = matchForecastFactory.getCalculator(model.getCalculator(), match, model);
//                        if (calculator.initialize(session)) calculator.process();  
//                        calculator.deInitialize();
//                    }
//                } else {
//                    LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Could not find Matches in the future."));    
//                }
//            } 
//        } else {
//            LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": No Match Calculators are active."));
//        }
//        session = HibernateUtil.closeSession();
//        session = HibernateUtil.startConfiguredSession(); 
        BetStager betStager = new BetStager();
        if (betStager.initialize()) betStager.process(true); 
        betStager.deInitialize();
        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": Learning was succesfully."));    
    }
}
