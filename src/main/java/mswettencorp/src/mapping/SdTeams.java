package mswettencorp.src.mapping;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Formula;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> SdTeams <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 18.07.2017
 */
@Entity
@Table
public class SdTeams implements java.io.Serializable {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final long serialVersionUID = 554870123898717316L;

    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
           
    @Column(name = "name", nullable = false, length = 50)
    private String name;
    
    @Column(name = "homeElo", precision = 12, scale = 4)
    private BigDecimal homeElo;
    
    @Column(name = "awayElo", precision = 12, scale = 4)
    private BigDecimal awayElo;
    
    @Formula("(homeElo+awayElo)/2")
    private BigDecimal overallElo;
    
    @Column (name = "createdBy", length = 50)
    private String createdBy;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public SdTeams() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof SdTeams)) return false;
        SdTeams other = (SdTeams) obj;
        if( name == null ) {
            if( other.name != null ) {
                return false;
            }
        } else if( !name.equals( other.name ) ) {
            return false;
        }
        return true;
    }
    
    ///////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getHomeElo() {
        return homeElo;
    }

    public void setHomeElo(BigDecimal homeElo) {
        this.homeElo = homeElo;
    }

    public BigDecimal getAwayElo() {
        return awayElo;
    }

    public void setAwayElo(BigDecimal awayElo) {
        this.awayElo = awayElo;
    }

    public BigDecimal getOverallElo() {
        return overallElo;
    }

    public void setOverallElo(BigDecimal overallElo) {
        this.overallElo = overallElo;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
