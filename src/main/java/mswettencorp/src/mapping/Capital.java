package mswettencorp.src.mapping;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> Capital <br>
 *
 * @author Michael Werner
 * @version v1.1
 * @since 18.07.2017
 */
@Entity
@Table
public class Capital implements java.io.Serializable {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final long serialVersionUID = -5280171458285793279L;
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "betAccount", nullable = false)
    private SystemBetUsers betAccount;
    
    @Column(name = "stake", precision = 12, scale = 2)
    private BigDecimal stake;
    
    @Column (name = "currency", nullable = false, length = 3)
    private String currency;
        
    @Column (name = "createdBy", length = 50)
    private String createdBy;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", length = 11)
    private Date created;  
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public Capital() {

    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof Capital)) return false;
        return true;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SystemBetUsers getBetAccount() {
        return betAccount;
    }

    public void setBetAccount(SystemBetUsers betAccount) {
        this.betAccount = betAccount;
    }

    public BigDecimal getStake() {
        return stake;
    }

    public void setStake(BigDecimal stake) {
        this.stake = stake;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
