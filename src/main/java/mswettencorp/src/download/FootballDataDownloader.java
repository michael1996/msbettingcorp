package mswettencorp.src.download;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import mswettencorp.src.mapping.SdLeagues;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> AbstractDownloader <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 09.01.2018
 */
public class FootballDataDownloader extends AbstractDownloader {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final String URL = "http://www.football-data.co.uk/";

    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public FootballDataDownloader(String input) {
        super(input);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean initialize(Session session) {
        if (super.initialize(session)) {
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        }
        return false;
    }
    
    @Override
    public void deInitialize() {
        super.deInitialize();
    }
    
    @Override
    public void process() {
        String split[];
        split = input.split("/");
        List<SdLeagues> listLeagues;
        switch (split[2].replace(".csv", "").trim().length()) {
            case 2:
                listLeagues = findLeague(split[2].substring(0, 2).trim());
                break;
            case 3:
                listLeagues = findLeague(split[2].substring(0, 3).trim());
                break;
            default:
                listLeagues = new LinkedList<>();
                LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Case for specified length is not handled."));
                break;
        }     
        if (!listLeagues.isEmpty()) {
            String file = FILEPATH.concat(listLeagues.get(0).getCountry().getName().toLowerCase(Locale.getDefault())
                .concat("/")
                .concat(listLeagues.get(0).getName()).toLowerCase())
                .concat("/")
                .concat(listLeagues.get(0).getCode().toLowerCase())
                .concat("_")
                .concat(split[1].substring(0, 2).trim())
                .concat("_")
                .concat(split[1].substring(2, 4).trim())
                .concat(".csv");
            setUrl(URL.concat(input));
            setFile(file);          
            super.process();
        }
    } 
    
    /**
     * 
     * @param code
     * @return 
     */
    public List<SdLeagues> findLeague(String code) {
        List<SdLeagues> returnLeagues = session.createQuery(" FROM SdLeagues l"
                + " WHERE l.code = :code")
                .setParameter("code", code)
                .setMaxResults(1)
                .setFetchSize(1)
                .getResultList();
        if (returnLeagues.isEmpty()) {
            LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Cannot find a League."));  
            return new LinkedList<>();
        }
        return returnLeagues;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
