package mswettencorp.src.reader.oddsReader;

import java.net.URISyntaxException;
import java.util.logging.Level;
import mswettencorp.src.reader.common.OddsReader;
import org.apache.http.client.utils.URIBuilder;
import org.hibernate.Session;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> Gamebookers <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 21.01.2017
 */
public class Gamebookers extends OddsReader {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final String URL = "sports.gamebookers.com";
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////          
    
    ////////////////////////////////////////////////////////////////////////////
    // Data Fields
    //////////////////////////////////////////////////////////////////////////// 
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public Gamebookers(String input) {
        super(input);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean initialize(Session session) {
        builder = new URIBuilder();
        builder.setScheme("https").setHost(URL).setPath("de/sports/4/wetten/fußball")
            .setFragment(input);
        try {
            uri = builder.build();
            Thread.sleep(5000);
            System.out.println(uri.toString());
        } catch (URISyntaxException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        } catch (InterruptedException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        }
        if (super.initialize(session)) {
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        }
        return false;
    }
    
    @Override
    public void deInitialize() {
        super.deInitialize();
    }
    
    @Override
    public void process() {   
//        System.out.println(parseDoc.toString());
        Elements bets = parseDoc.getElementsByClass("marketboard-options-row marketboard-options-row--3-way");
        MatchOdds createMatchOdd;
        for (Element bet : bets) {
            createMatchOdd = new MatchOdds();
            createMatchOdd.homeTeam = bet.child(0).child(1).child(0).ownText(); 
            createMatchOdd.awayTeam = bet.child(2).child(1).child(0).ownText(); 
            createMatchOdd.winHomeOdd = Double.parseDouble(bet.child(0).child(1).child(1).ownText()); 
            createMatchOdd.drawOdd = Double.parseDouble(bet.child(1).child(0).child(1).ownText()); 
            createMatchOdd.winAwayOdd = Double.parseDouble(bet.child(2).child(1).child(1).ownText()); 
            System.out.println(createMatchOdd.homeTeam + " : " + createMatchOdd.awayTeam);
            System.out.println(createMatchOdd.winHomeOdd + "    " + createMatchOdd.drawOdd + "  " + createMatchOdd.winAwayOdd);
            System.out.println("-----------------------");
            listMatchOdds.add(createMatchOdd);
        }   
        System.out.println(listMatchOdds.size());
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
