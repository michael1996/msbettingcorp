package mswettencorp.src.mapping;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;
/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> SdModels <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 18.07.2017
 */
@Entity
@Immutable
@Table
public class SdModels implements java.io.Serializable {

    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final long serialVersionUID = 5598113874073777833L;

    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
    
    @Column(name = "calculator", nullable = false, length = 100)
    private String calculator;
    
    @Column(name = "loadingBehavior", nullable = false, length = 100)
    private String loadingBehavior;
    
    @Column(name = "weightingBehavior", nullable = false, length = 100)
    private String weightingBehavior;
    
    @Column(name = "isActive", nullable = false)
    private boolean isActive;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public SdModels() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((calculator == null) ? 0 : calculator.hashCode());
        result = prime * result + ((loadingBehavior == null) ? 0 : loadingBehavior.hashCode());
        result = prime * result + ((weightingBehavior == null) ? 0 : weightingBehavior.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof SdModels)) return false;
        SdModels other = (SdModels) obj;
        if( calculator == null ) {
            if( other.calculator != null ) {
                return false;
            }
        } else if( !calculator.equals( other.calculator ) ) {
            return false;
        }
        if( loadingBehavior == null ) {
            if( other.loadingBehavior != null ) {
                return false;
            }
        } else if( !loadingBehavior.equals( other.loadingBehavior ) ) {
            return false;
        }
        if( weightingBehavior == null ) {
            if( other.weightingBehavior != null ) {
                return false;
            }
        } else if( !weightingBehavior.equals( other.weightingBehavior ) ) {
            return false;
        }
        return true;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCalculator() {
        return calculator;
    }

    public void setCalculator(String calculator) {
        this.calculator = calculator;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getLoadingBehavior() {
        return loadingBehavior;
    }

    public void setLoadingBehavior(String loadingBehavior) {
        this.loadingBehavior = loadingBehavior;
    }

    public String getWeightingBehavior() {
        return weightingBehavior;
    }

    public void setWeightingBehavior(String weightingBehavior) {
        this.weightingBehavior = weightingBehavior;
    }
}
