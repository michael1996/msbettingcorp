package mswettencorp.src.reader.common;

import java.util.logging.Level;
import mswettencorp.src.reader.dataReader.Betexplorer;
import mswettencorp.src.reader.dataReader.Weltfussball;
import mswettencorp.src.reader.oddsReader.Bet365;
import mswettencorp.src.reader.oddsReader.Betsafe;
import mswettencorp.src.reader.oddsReader.Betvictor;
import mswettencorp.src.reader.oddsReader.Bwin;
import mswettencorp.src.reader.oddsReader.Gamebookers;
import mswettencorp.src.reader.oddsReader.Interwetten;
import mswettencorp.src.reader.oddsReader.NetBet;
import mswettencorp.src.reader.oddsReader.Pinnacle;
import mswettencorp.src.reader.oddsReader.Sportingbet;
import mswettencorp.src.reader.oddsReader.Tipico;
import mswettencorp.src.reader.oddsReader.Williamhill;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> HtmlReaderFactory <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 08.01.2018
 */
public class HtmlReaderFactory extends AbstractReaderFactory {
 
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////    
    @Override
    protected AbstractReader createReader(String ctx, String input) {
        AbstractReader returnReader = null;
        if (ctx.equals("Weltfussball")) {
            returnReader = new Weltfussball(input);
        } else if (ctx.equals("Betexplorer")) {
            returnReader = new Betexplorer(input);
        } else if (ctx.equals("Tipico")) {
            returnReader = new Tipico(input);
        } else if (ctx.equals("Interwetten")) {
            returnReader = new Interwetten(input);
        } else if (ctx.equals("Gamebookers")) {
            returnReader = new Gamebookers(input);
        } else if (ctx.equals("Bwin")) {
            returnReader = new Bwin(input);
        } else if (ctx.equals("Pinnacle")) {
            returnReader = new Pinnacle(input);
        } else if (ctx.equals("Sportingbet")) {
            returnReader = new Sportingbet(input);
        } else if (ctx.equals("Betvictor")) {
            returnReader = new Betvictor(input);
        } else if (ctx.equals("Williamhill")) {
            returnReader = new Williamhill(input);
        } else if (ctx.equals("Bet365")) {
            returnReader = new Bet365(input);
        } else if (ctx.equals("Betsafe")) {
            returnReader = new Betsafe(input);
        } else if (ctx.equals("NetBet")) {
            returnReader = new NetBet(input);
        } else {
            LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Cannot create a valid Html Reader."));
        }
        return returnReader;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
