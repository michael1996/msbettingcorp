package mswettencorp.src.mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> SystemWebsites <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 27.12.2017
 */
@Entity
@Immutable
@Table
public class SystemWebsites implements java.io.Serializable {
   
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    /////////////////////////////////////////////////%///////////////////////////
    private static final long serialVersionUID = -5280171458285798273L;
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
    
    @Column(name = "websiteLink", nullable = false, length = 255)
    private String websiteLink;
    
    @Column(name = "HtmlParser", nullable = false, length = 50)
    private String htmlParser;
    
    @Column(name = "parserType", nullable = false, length = 50)
    private String parserType;
    
    @Column(name = "isActive", nullable = false)
    private boolean isActive;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public SystemWebsites() {

    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((websiteLink == null) ? 0 : websiteLink.hashCode());
        result = prime * result + ((htmlParser == null) ? 0 : htmlParser.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof SystemWebsites)) return false;
        SystemWebsites other = (SystemWebsites) obj;
        if( websiteLink == null ) {
            if( other.websiteLink != null ) {
                return false;
            }
        } else if( !websiteLink.equals( other.websiteLink ) ) {
            return false;
        }
        if( htmlParser == null ) {
            if( other.htmlParser != null ) {
                return false;
            }
        } else if( !htmlParser.equals( other.htmlParser ) ) {
            return false;
        }
        return true;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getWebsiteLink() {
        return websiteLink;
    }

    public void setWebsiteLink(String websiteLink) {
        this.websiteLink = websiteLink;
    }

    public String getHtmlParser() {
        return htmlParser;
    }

    public void setHtmlParser(String htmlParser) {
        this.htmlParser = htmlParser;
    }

    public String getParserType() {
        return parserType;
    }

    public void setParserType(String parserType) {
        this.parserType = parserType;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
}
