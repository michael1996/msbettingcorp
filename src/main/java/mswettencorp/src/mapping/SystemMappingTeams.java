package mswettencorp.src.mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> SystemFiles <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 27.12.2017
 */
@Entity
@Table
public class SystemMappingTeams implements java.io.Serializable {
   
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final long serialVersionUID = -5280171458285798273L;
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
        
    @Column (name = "originalName", length = 100)
    private String originalName;
    
    @Column (name = "mappingName", nullable = false, length = 100)
    private String mappingName;
        
    @Column (name = "createdBy", length = 50)
    private String createdBy;
    
    @Column (name = "updatedBy", length = 50)
    private String updatedBy;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public SystemMappingTeams() {

    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
  
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof SystemMappingTeams)) return false;
        SystemMappingTeams other = (SystemMappingTeams) obj;
        if( originalName == null ) {
            if( other.originalName != null ) {
                return false;
            }
        } else if( !originalName.equals( other.originalName ) ) {
            return false;
        }
        if( mappingName == null ) {
            if( other.mappingName != null ) {
                return false;
            }
        } else if( !mappingName.equals( other.mappingName ) ) {
            return false;
        }
        return true;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getMappingName() {
        return mappingName;
    }

    public void setMappingName(String mappingName) {
        this.mappingName = mappingName;
    }
 
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
}
