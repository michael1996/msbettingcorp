package mswettencorp.src.forecast.weightingBehavior;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.SdTeams;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> LinearTopicalityElo <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 16.02.2018
 */
public class LinearTopicalityElo implements WeightingBehavior {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    private List<Double> listWeighting;
    private double increment;
    private double homeTeamFactor;
    private double awayTeamFactor;
    private double sum;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public LinearTopicalityElo() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public List<Double> weighting(List<Matches> listMatches, SdTeams team) {
        listWeighting = new ArrayList<>(listMatches.size());
        increment = listMatches.size();
        for (Matches match : listMatches) {
            awayTeamFactor = match.getHomeTeamElo().doubleValue() / match.getAwayTeamElo().doubleValue();
            homeTeamFactor = match.getAwayTeamElo().doubleValue() / match.getHomeTeamElo().doubleValue();
            sum = awayTeamFactor + homeTeamFactor;
            if (team.getName().equals(match.getHomeTeam().getName())) {
                homeTeamFactor = homeTeamFactor / sum;
                listWeighting.add(increment * homeTeamFactor);
            } else {
                awayTeamFactor = awayTeamFactor / sum;
                listWeighting.add(increment * awayTeamFactor);
            }
            increment--;
        }
        if (listWeighting.isEmpty()) {
            return new LinkedList<>();
        } else {
            return listWeighting;
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
