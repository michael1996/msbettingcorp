package mswettencorp.src.processor;

import java.util.logging.Level;
import java.util.logging.Logger;
import mswettencorp.src.util.LoggerUtil;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> MPDataProcessor <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 10.01.2018
 */
public class MPDataProcessor {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final Logger LOGGER = LoggerUtil.getLogger();
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    /**
     * 
     * @return 
     */
    public boolean initialize() {
        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
        return true;
    }
    
    /**
     * 
     */
    public void deInitialize() {
        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": deinitialized succesfully."));
    }
    
    /**
     * 
     * @param ctxType
     * @param ctxValue 
     */
    public void process(String ctxType, String ctxValue) {
        switch(ctxType) {
            case "Odd":
                OddProcessor oddProcessor = new OddProcessor();
                if (oddProcessor.initialize()) oddProcessor.process(ctxValue); 
                oddProcessor.deInitialize();
                break;
            case "Match":
                MatchProcessor matchProcessor = new MatchProcessor();
                if (matchProcessor.initialize()) matchProcessor.process(ctxValue); 
                matchProcessor.deInitialize();
                break;
            case "Player":
                PlayerProcessor playerProcessor = new PlayerProcessor();
                if (playerProcessor.initialize()) playerProcessor.process(ctxValue);
                playerProcessor.deInitialize();
                break;
            default: 
                LOGGER.log(Level.SEVERE," Cannot find the data processor.");
                break;
        }
    }
}
