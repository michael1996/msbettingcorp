package mswettencorp.src.reader.oddsReader;

import java.net.URISyntaxException;
import java.util.logging.Level;
import mswettencorp.src.reader.common.OddsReader;
import org.apache.http.client.utils.URIBuilder;
import org.hibernate.Session;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> Bet365 <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 21.01.2017
 */
public class Bet365 extends OddsReader {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final String URL = "www.bet365.com";
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////          
    
    ////////////////////////////////////////////////////////////////////////////
    // Data Fields
    //////////////////////////////////////////////////////////////////////////// 
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public Bet365(String input) {
        super(input);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean initialize(Session session) {
        String splitInput[] = input.split("#");
        String splitParamter[] = splitInput[0].split("&");
        String splitFirst[] = splitParamter[0].split("=");
        String splitSecond[] = splitParamter[1].split("=");
        builder = new URIBuilder();
        builder.setScheme("https").setHost(URL)
            .setParameter(splitFirst[0], splitFirst[1])
            .setParameter(splitSecond[0], splitSecond[1])
            .setFragment(splitInput[1]);
        try {
            uri = builder.build();
            System.out.println(uri.toString());
        } catch (URISyntaxException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        }
        if (super.initialize(session)) {
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        }
        return false;
    }
    
    @Override
    public void deInitialize() {
        super.deInitialize();
    }
    
    @Override
    public void process() {   
        System.out.println(parseDoc.toString());
        Elements bets = parseDoc.getElementsByClass("sl-CouponParticipantWithBookCloses_Name ");
        MatchOdds createMatchOdd;
        for (Element bet : bets) {
            String splitTeam[] = bet.ownText().split("v");
            System.out.println(splitTeam[0].trim() + " : " + splitTeam[1].trim());
//            createMatchOdd = new matchOdds();
//            createMatchOdd.homeTeam = bet.select("[itemprop=homeTeam]").get(0).child(0).ownText(); 
//            createMatchOdd.awayTeam = bet.select("[itemprop=awayTeam]").get(0).child(0).ownText(); 
//            createMatchOdd.winHomeOdd = Double.parseDouble(bet.select("[itemprop=homeTeam]").get(0).child(1).ownText().replace(",", ".")); 
//            createMatchOdd.drawOdd = Double.parseDouble(bet.child(1).child(0).child(1).ownText().replace(",", "."));
//            createMatchOdd.winAwayOdd = Double.parseDouble(bet.select("[itemprop=awayTeam]").get(0).child(1).ownText().replace(",", ".")); 
//            listMatchOdds.add(createMatchOdd);
        }  
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
