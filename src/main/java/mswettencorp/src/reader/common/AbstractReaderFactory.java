package mswettencorp.src.reader.common;

import java.util.logging.Logger;
import mswettencorp.src.util.LoggerUtil;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> AbstractReaderFactory <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 08.01.2018
 */
public abstract class AbstractReaderFactory {
 
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    protected static final Logger LOGGER = LoggerUtil.getLogger();
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    public AbstractReader getReader(String ctx, String input) {
        AbstractReader abstractReader = createReader(ctx, input);
        
        return abstractReader;
    }
    
    protected abstract AbstractReader createReader(String ctx, String input);
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
