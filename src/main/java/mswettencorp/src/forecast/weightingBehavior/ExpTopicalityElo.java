package mswettencorp.src.forecast.weightingBehavior;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.SdTeams;
import org.apache.commons.math3.analysis.function.Exp;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> ExpTopicalityElo <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 16.02.2018
 */
public class ExpTopicalityElo implements WeightingBehavior {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final int MAX = 1;
    private static final int MIN = 0;
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    private List<Double> listWeighting;
    private Exp function;
    private double xCoordinate;
    private double xValue;
    private double homeTeamFactor;
    private double awayTeamFactor;
    private double sum;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public ExpTopicalityElo() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public List<Double> weighting(List<Matches> listMatches, SdTeams team) {
        listWeighting = new ArrayList<>(listMatches.size());
        function = new Exp();
        xCoordinate = MAX;
        xValue =(MAX - MIN) / (double)listMatches.size();
        for (Matches match : listMatches) {
            awayTeamFactor = match.getHomeTeamElo().doubleValue() / match.getAwayTeamElo().doubleValue();
            homeTeamFactor = match.getAwayTeamElo().doubleValue() / match.getHomeTeamElo().doubleValue();
            sum = awayTeamFactor + homeTeamFactor;
            if (team.getName().equals(match.getHomeTeam().getName())) {
                homeTeamFactor = homeTeamFactor / sum * 2;
                listWeighting.add(function.value(xCoordinate) * homeTeamFactor);
            } else {
                awayTeamFactor = awayTeamFactor / sum * 2;
                listWeighting.add(function.value(xCoordinate) * awayTeamFactor);
            }
            xCoordinate = xCoordinate - xValue;
        }
        if (listWeighting.isEmpty()) {
            return new LinkedList<>();
        } else {
            return listWeighting;
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
