package mswettencorp.src.processor;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mswettencorp.src.forecast.common.AbstractForecast;
import mswettencorp.src.forecast.common.AbstractForecastFactory;
import mswettencorp.src.forecast.common.MatchForecastFactory;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.Predictions;
import mswettencorp.src.mapping.SdModels;
import mswettencorp.src.util.HibernateUtil;
import mswettencorp.src.util.LoggerUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> MPForecastProcessor <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 12.01.2018
 */
public class MPForecastProcessor {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final Logger LOGGER = LoggerUtil.getLogger();
    private static final int DAYSINFUTURE = 7;
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    private Session session;    
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    /**
     * 
     * @return 
     */
    public boolean initialize() {
        try {
            session = HibernateUtil.startConfiguredSession(); 
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        } catch (HibernateException ex) {
            LOGGER.log(Level.SEVERE, null, ex.getMessage());
            return false;
        }
    }
    
    /**
     * 
     */
    public void deInitialize() {
        try {
            session = HibernateUtil.closeSession(); 
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": deinitialized succesfully."));
        } catch (HibernateException ex) {
            LOGGER.log(Level.SEVERE, null, ex.getMessage());
        } 
    }
    
    /**
     * 
     * @param ctxType
     * @param ctxValue 
     */
    public void process(String ctxType, String ctxValue) {
        Date currentDate = new Date();
        java.sql.Date currentSqlDate = new java.sql.Date(currentDate.getTime());
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(currentDate);
        calendar.add(Calendar.DAY_OF_MONTH, DAYSINFUTURE);
        Date future = calendar.getTime();
        java.sql.Date endSqlDate = new java.sql.Date(future.getTime());
        List<Matches> futureMatches = session.createQuery("SELECT m FROM Matches m"
                + " JOIN FETCH m.homeTeam"
                + " JOIN FETCH m.awayTeam"
                + " WHERE m.isHistoric = false"
                + " AND m.date >= :startDate"
                + " AND m.date < :endDate"
                + " ORDER by m.date asc")
                .setParameter("startDate", currentSqlDate)
                .setParameter("endDate", endSqlDate)
                .getResultList();
        List<SdModels> findModels = session.createQuery(" FROM SdModels m"
                + " WHERE m.isActive = true"
                + " AND m.calculator <> 'MPCubeProcessor'")
                .getResultList();
        
        if(!findModels.isEmpty()) {
            for (SdModels model : findModels) {
                if (!futureMatches.isEmpty()) {
                    for (Matches match : futureMatches) {
                        AbstractForecastFactory matchForecastFactory = new MatchForecastFactory();
                        AbstractForecast calculator = matchForecastFactory.getCalculator(model.getCalculator(), match, model);
                        if (calculator.initialize(session)) calculator.process();  
                        calculator.deInitialize();
                    } 
                } else {
                    LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Could not find Matches in the future."));    
                }
            }
        } else {
            LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": No Match Calculators are active."));
        }
        
        List<Predictions> listForecasts = session.createQuery(" FROM MatchForecasts mf"
            + " WHERE mf.isHistoric = false")
            .getResultList();
        for (Predictions forecast : listForecasts) {
//            if (forecast.getMatchForecast().isIsHistoric()) {
//                forecast.setIsHistoric(true);
//                session.persist(forecast);
//                LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
//                        .concat(forecast.toString())
//                        .concat(" match is over."));
//            }
        }
        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": Forecasting was succesfully."));    
    }
}
