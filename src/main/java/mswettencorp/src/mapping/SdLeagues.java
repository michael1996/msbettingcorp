package mswettencorp.src.mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Immutable;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> SdLeagues <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 18.07.2017
 */
@Entity
@Immutable
@Table
public class SdLeagues implements java.io.Serializable {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final long serialVersionUID = -4962413634791542584L;

    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country", nullable = false)
    private SdCountries country;
       
    @Column(name = "name", length = 50)
    private String name;
    
    @Column(name = "code", nullable = false, length = 3)
    private String code;
        
    @Column(name = "numberTeams", nullable = false, precision = 18, scale = 0)    
    private long numberTeams;
    
    @Formula("(numberTeams-1)*2")
    private long numberMatchdays;
    
    @Formula("numberTeams/2")
    private long numberMatchesPerMatchday;
    
    @Formula("((numberTeams-1)*2)*(numberTeams/2)")
    private long numberMatches;
    
    ////////////////////////////////////////////////////////////////////////////
    // Contructor
    ////////////////////////////////////////////////////////////////////////////
    public SdLeagues() {
        
    }
  
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        result = prime * result + ((country == null) ? 0 : country.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof SdLeagues)) return false;
        SdLeagues other = (SdLeagues) obj;
        if( name == null ) {
            if( other.name != null ) {
                return false;
            }
        } else if( !name.equals( other.name ) ) {
            return false;
        }
        if( country == null ) {
            if( other.country != null ) {
                return false;
            }
        } else if( !country.equals( other.country ) ) {
            return false;
        }
        return true;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SdCountries getCountry() {
        return country;
    }

    public void setCountry(SdCountries country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getNumberTeams() {
        return numberTeams;
    }

    public void setNumberTeams(long numberTeams) {
        this.numberTeams = numberTeams;
    }

    public long getNumberMatchdays() {
        return numberMatchdays;
    }

    public long getNumberMatchesPerMatchday() {
        return numberMatchesPerMatchday;
    }

    public long getNumberMatches() {
        return numberMatches;
    }
}
