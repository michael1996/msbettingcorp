package mswettencorp.src.mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> SystemSettings <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 26.12.2017
 */
@Entity
@Immutable
@Table
public class SystemSettings implements java.io.Serializable {
   
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    /////////////////////////////////////////////////%///////////////////////////
    private static final long serialVersionUID = -5280171458285798273L;
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;

    @Column(name = "property", nullable = false, length = 100) 
    private String property;
    
    @Column(name = "parameterValue", nullable = false, length = 256)
    private String parameterValue;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public SystemSettings() {

    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((property == null) ? 0 : property.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof SystemSettings)) return false;
        SystemSettings other = (SystemSettings) obj;
        if( property == null ) {
            if( other.property != null ) {
                return false;
            }
        } else if( !property.equals( other.property ) ) {
            return false;
        }
        
        return true;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getParameterValue() {
        return parameterValue;
    }

    public void setParameterValue(String parameterValue) {
        this.parameterValue = parameterValue;
    }    
}
