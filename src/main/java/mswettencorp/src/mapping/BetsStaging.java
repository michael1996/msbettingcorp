package mswettencorp.src.mapping;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> BetsStaging <br>
 *
 * @author Michael Werner
 * @version v1.1
 * @since 23.01.2018
 */
@Entity
@Table
public class BetsStaging implements java.io.Serializable {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final long serialVersionUID = -5280171458285798279L;
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "modelForecast", nullable = false)
    private Predictions modelForecast;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "betType", nullable = false)
    private SdBetTyps betType;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "betBookmaker", nullable = false)
    private SdBookmakers betBookmaker;
    
    @Column(name = "betResult", nullable = false, length = 5)
    private String betResult;
    
    @Column(name = "isCorrect")
    private boolean isCorrect;
    
    @Column(name = "isHistoric")
    private boolean isHistoric;
    
    @Column(name = "value", precision = 12, scale = 4)
    private BigDecimal value;
    
    @Column(name = "odd", precision = 12, scale = 4)
    private BigDecimal odd;
        
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public BetsStaging() {

    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((betType == null) ? 0 : betType.hashCode());
        result = prime * result + ((modelForecast == null) ? 0 : modelForecast.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof BetsStaging)) return false;
        BetsStaging other = (BetsStaging) obj;
        if( betType == null ) {
            if( other.betType != null ) {
                return false;
            }
        } else if( !betType.equals( other.betType ) ) {
            return false;
        }
        if( modelForecast == null ) {
            if( other.modelForecast != null ) {
                return false;
            }
        } else if( !modelForecast.equals( other.modelForecast ) ) {
            return false;
        }        
        return true;
    }

    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Predictions getModelForecast() {
        return modelForecast;
    }

    public void setModelForecast(Predictions modelForecast) {
        this.modelForecast = modelForecast;
    }

    public SdBetTyps getBetType() {
        return betType;
    }

    public void setBetType(SdBetTyps betType) {
        this.betType = betType;
    }

    public SdBookmakers getBetBookmaker() {
        return betBookmaker;
    }

    public void setBetBookmaker(SdBookmakers betBookmaker) {
        this.betBookmaker = betBookmaker;
    }

    public String getBetResult() {
        return betResult;
    }

    public void setBetResult(String betResult) {
        this.betResult = betResult;
    }

    public boolean isIsCorrect() {
        return isCorrect;
    }
    
    public void setIsCorrect(boolean isCorrect) {    
        this.isCorrect = isCorrect;
    }

    public boolean isIsHistoric() {
        return isHistoric;
    }

    public void setIsHistoric(boolean isHistoric) {
        this.isHistoric = isHistoric;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getOdd() {
        return odd;
    }

    public void setOdd(BigDecimal odd) {
        this.odd = odd;
    }
}
