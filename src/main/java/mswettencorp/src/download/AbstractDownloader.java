package mswettencorp.src.download;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import mswettencorp.src.util.LoggerUtil;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> AbstractDownloader <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 08.01.2018
 */
public abstract class AbstractDownloader {
 
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    protected static final Logger LOGGER = LoggerUtil.getLogger();
    protected static final String FILEPATH = System.getProperty("user.dir") 
        .concat("/src/main/java/mswettencorp/res/data/");
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    protected Session session;  
    protected final String input;
    private String file;
    private String url;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public AbstractDownloader(String input) {
        this.input = input.toLowerCase(Locale.getDefault());
    }
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    public boolean initialize(Session session) {
        this.session = session;
        return true;
    }
    
    public void deInitialize() {
        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": deinitialized succesfully."));
    }
    
    public void process() {
        try {    
            URL netUrl = new URL(url);    
            BufferedInputStream bufferedInputStream = new BufferedInputStream(netUrl.openStream());
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            byte[] buffer = new byte[1024];
            int count = 0;
            while((count = bufferedInputStream.read(buffer, 0, 1024)) != -1)
            {
                fileOutputStream.write(buffer, 0, count);
            }
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ").concat(new File(file).getName())
                    .concat(" was downloaded succesfully."));
        } catch (MalformedURLException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        } catch (FileNotFoundException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}

