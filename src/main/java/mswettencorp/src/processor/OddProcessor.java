package mswettencorp.src.processor;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mswettencorp.src.mapping.Odds;
import mswettencorp.src.mapping.SystemWebsites;
import mswettencorp.src.reader.common.AbstractReader;
import mswettencorp.src.reader.common.AbstractReaderFactory;
import mswettencorp.src.reader.common.HtmlReaderFactory;
import mswettencorp.src.util.HibernateUtil;
import mswettencorp.src.util.LoggerUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> OddProcessor <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 09.02.2018
 */
public class OddProcessor {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final Logger LOGGER = LoggerUtil.getLogger();
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    private Session session;  
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    /**
     * 
     * @return 
     */
    public boolean initialize() {
        try {
            session = HibernateUtil.startConfiguredSession(); 
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        } catch (HibernateException ex) {
            LOGGER.log(Level.SEVERE, null, ex.getMessage());
            return false;
        }
    }
    
    /**
     * 
     */
    public void deInitialize() {
        try {
            session = HibernateUtil.closeSession(); 
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": deinitialized succesfully."));
        } catch (HibernateException ex) {
            LOGGER.log(Level.SEVERE, null, ex.getMessage());
        } 
    }
    
    /**
     * 
     * @param ctxValue 
     */
    public void process(String ctxValue) {
        if (ctxValue.equals("website")) {
            List<SystemWebsites> findWebsites = session.createQuery(" FROM SystemWebsites sw"
                + " WHERE sw.isActive = true"
                + " AND sw.parserType = 'odd'")
                .getResultList();
            if(!findWebsites.isEmpty()) {
                for (SystemWebsites website : findWebsites) {
                    AbstractReaderFactory websiteFactory = new HtmlReaderFactory();
                    AbstractReader parser = websiteFactory.getReader(website.getHtmlParser(), website.getWebsiteLink());
                    if (parser.initialize(session)) parser.process();   
                    parser.deInitialize();
                }
            } else {
                LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": No Websites are active."));
            }
        }
        
        List<Odds> listOdds = session.createQuery(" FROM Odds o"
            + " WHERE o.isHistoric = false")
            .getResultList();
        for (Odds odd : listOdds) {
            if (odd.getMatchOdds().getStatus().getName().equalsIgnoreCase("finished")) {
                odd.setIsHistoric(true);
                session.persist(odd);
                LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                        .concat(odd.toString())
                        .concat(" match is over."));
            }
        }
        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": Processing odds was succesfully."));
    }
}
