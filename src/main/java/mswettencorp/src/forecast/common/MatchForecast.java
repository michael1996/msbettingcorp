package mswettencorp.src.forecast.common;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import static mswettencorp.src.forecast.common.AbstractForecast.LOGGER;
import mswettencorp.src.forecast.loadingBehavior.LoadingBehavior;
import mswettencorp.src.forecast.loadingBehavior.LongtermHeadToHeadOverall;
import mswettencorp.src.forecast.loadingBehavior.LongtermHeadToHeadSide;
import mswettencorp.src.forecast.loadingBehavior.LongtermOverall;
import mswettencorp.src.forecast.loadingBehavior.LongtermSide;
import mswettencorp.src.forecast.loadingBehavior.MediumtermOverall;
import mswettencorp.src.forecast.loadingBehavior.MediumtermSide;
import mswettencorp.src.forecast.loadingBehavior.ShorttermSide;
import mswettencorp.src.forecast.weightingBehavior.EqualTopicality;
import mswettencorp.src.forecast.weightingBehavior.EqualTopicalityElo;
import mswettencorp.src.forecast.weightingBehavior.EqualTopicalityEloWithGoalDiff;
import mswettencorp.src.forecast.weightingBehavior.ExpTopicality;
import mswettencorp.src.forecast.weightingBehavior.ExpTopicalityElo;
import mswettencorp.src.forecast.weightingBehavior.ExpTopicalityEloWithGoalDiff;
import mswettencorp.src.forecast.weightingBehavior.LinearTopicality;
import mswettencorp.src.forecast.weightingBehavior.LinearTopicalityElo;
import mswettencorp.src.forecast.weightingBehavior.LinearTopicalityEloWithGoalDiff;
import mswettencorp.src.forecast.weightingBehavior.Log10Topicality;
import mswettencorp.src.forecast.weightingBehavior.Log10TopicalityElo;
import mswettencorp.src.forecast.weightingBehavior.Log10TopicalityEloWithGoalDiff;
import mswettencorp.src.forecast.weightingBehavior.WeightingBehavior;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.Predictions;
import mswettencorp.src.mapping.SdModels;
import mswettencorp.src.mapping.SdTeams;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> MatchForecast <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 13.01.2018
 */
public class MatchForecast extends AbstractForecast {
   
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
        
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    protected SdModels model;
    protected Matches match;
    protected List<Matches> analyzedHomeTeamMatches;
    protected List<Matches> analyzedAwayTeamMatches;
    protected List<Double> weightingHomeTeamMatches;
    protected List<Double> weightingAwayTeamMatches;
    protected LoadingBehavior loadingBehavior;
    protected WeightingBehavior weightingBehavior;

    ////////////////////////////////////////////////////////////////////////////
    // Data Fields
    ////////////////////////////////////////////////////////////////////////////
    protected double homeWinOdd = 0;
    protected double drawOdd = 0;
    protected double awayWinOdd = 0;
    protected double over15Odd = 0;
    protected double under15Odd = 0;
    protected double over25Odd = 0;
    protected double under25Odd = 0;
    protected double over35Odd = 0;
    protected double under35Odd = 0;
    protected int weightingCounterHome = 0;
    protected int weightingCounterAway = 0;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public MatchForecast(Matches match, SdModels model) {
        super();
        this.match = match;
        this.model = model;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean initialize(Session session) {
        if (super.initialize(session)) {
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": Starting initialization ..."));
            analyzedHomeTeamMatches = loading(session, model.getLoadingBehavior(), true);
            analyzedAwayTeamMatches = loading(session, model.getLoadingBehavior(), false);
            if (analyzedHomeTeamMatches.isEmpty() || analyzedAwayTeamMatches.isEmpty()) {
                LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Found not enough matches to analyze."));
                return false;
            } else {
                weightingHomeTeamMatches = weighting(model.getWeightingBehavior(), analyzedHomeTeamMatches, match.getHomeTeam());
                weightingAwayTeamMatches = weighting(model.getWeightingBehavior(), analyzedAwayTeamMatches, match.getAwayTeam());
                if (weightingHomeTeamMatches.isEmpty() || weightingAwayTeamMatches.isEmpty()) {
                    LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Cannot calculate the weighting."));
                    return false;
                }
                return true;
            }
        } else {
            return false;
        }
    }
    
    @Override
    public void deInitialize() {
        super.deInitialize();
    }
    
    @Override
    public void process() {
        List<Predictions> listForecast = findMatchForecast(match, model);
        Predictions createMatchForecast;
        if (!listForecast.isEmpty()) {
            createMatchForecast = listForecast.get(0);
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                    .concat(createMatchForecast.toString())
                    .concat(" was updated."));
        } else {
            createMatchForecast = new Predictions();
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                    .concat(createMatchForecast.toString())
                    .concat(" was created."));
        }
        createMatchForecast.setMatchForecast(match);
        createMatchForecast.setModel(model);
        createMatchForecast.setWinHomeOdd(BigDecimal.valueOf(homeWinOdd));
        createMatchForecast.setDrawOdd(BigDecimal.valueOf(drawOdd));
        createMatchForecast.setWinAwayOdd(BigDecimal.valueOf(awayWinOdd));
        createMatchForecast.setUnder15Odd(BigDecimal.valueOf(under15Odd));
        createMatchForecast.setOver15Odd(BigDecimal.valueOf(over15Odd));
        createMatchForecast.setUnder25Odd(BigDecimal.valueOf(under25Odd));
        createMatchForecast.setOver25Odd(BigDecimal.valueOf(over25Odd));
        createMatchForecast.setUnder35Odd(BigDecimal.valueOf(under35Odd));
        createMatchForecast.setOver35Odd(BigDecimal.valueOf(over35Odd));
//        if (match.isIsHistoric()) createMatchForecast.setIsHistoric(true);
//        if (!match.isIsHistoric()) createMatchForecast.setIsHistoric(false);
        createMatchForecast.setAnalyzedHomeMatches(analyzedHomeTeamMatches.size());
        createMatchForecast.setAnalyzedAwayMatches(analyzedAwayTeamMatches.size());
        session.persist(createMatchForecast);
        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": Processing match forecast was succesfully."));
    }
    
    /**
     * 
     * @param session
     * @param parmLoadingBehavior
     * @param isHomeTeam
     * @return 
     */
    @Override
    public List<Matches> loading(Session session, String parmLoadingBehavior, boolean isHomeTeam) {
        switch (parmLoadingBehavior) {
            case "LongtermSide":
                loadingBehavior = new LongtermSide();
                break;
            case "MediumtermSide":
                loadingBehavior = new MediumtermSide();
                break;
            case "ShorttermSide":
                loadingBehavior = new ShorttermSide();
                break;
            case "LongtermOverall":
                loadingBehavior = new LongtermOverall();
                break;
            case "MediumtermOverall":
                loadingBehavior = new MediumtermOverall();
                break;
            case "LongtermHeadToHeadOverall":
                loadingBehavior = new LongtermHeadToHeadOverall();
                break;
            case "LongtermHeadToHeadSide":
                loadingBehavior = new LongtermHeadToHeadSide();
                break;
            default:
                loadingBehavior = null;
                break;
        }
        if (loadingBehavior != null) {
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": Choose loading strategy: ")
                .concat(loadingBehavior.getClass().getSimpleName()));
            return loadingBehavior.loading(session, match, isHomeTeam); 
        } else {
            LOGGER.log(Level.SEVERE, getClass().getSimpleName().concat(": Cannot find a loading strategy."));
            return null;
        }   
    }
    
    /**
     * 
     * @param parmWeightingBehavior
     * @param listMatches
     * @param team
     * @return 
     */
    @Override
    public List<Double> weighting(String parmWeightingBehavior, List<Matches> listMatches, SdTeams team) {
        switch (parmWeightingBehavior) {
            case "EqualTopicality":
                weightingBehavior = new EqualTopicality();
                break;
            case "LinearTopicality":
                weightingBehavior = new LinearTopicality();
                break;   
            case "Log10Topicality":
                weightingBehavior = new Log10Topicality();
                break;  
            case "ExpTopicality":
                weightingBehavior = new ExpTopicality();
                break;  
            case "EqualTopicalityEloWithGoalDiff":
                weightingBehavior = new EqualTopicalityEloWithGoalDiff();
                break;
            case "LinearTopicalityEloWithGoalDiff":
                weightingBehavior = new LinearTopicalityEloWithGoalDiff();
                break;   
            case "Log10TopicalityEloWithGoalDiff":
                weightingBehavior = new Log10TopicalityEloWithGoalDiff();
                break;  
            case "ExpTopicalityEloWithGoalDiff":
                weightingBehavior = new ExpTopicalityEloWithGoalDiff();
                break;    
            case "EqualTopicalityElo":
                weightingBehavior = new EqualTopicalityElo();
                break;
            case "LinearTopicalityElo":
                weightingBehavior = new LinearTopicalityElo();
                break;   
            case "Log10TopicalityElo":
                weightingBehavior = new Log10TopicalityElo();
                break;  
            case "ExpTopicalityElo":
                weightingBehavior = new ExpTopicalityElo();
                break; 
            default:
                weightingBehavior = null;
                break;
        }
        if (weightingBehavior != null) {
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": Choose weighting strategy: ")
                .concat(weightingBehavior.getClass().getSimpleName()));
            return weightingBehavior.weighting(listMatches, team); 
        } else {
            LOGGER.log(Level.SEVERE, getClass().getSimpleName().concat(": Cannot find a weighting strategy."));
            return null;
        }   
    }
    
    /**
     * 
     * @param match
     * @param model
     * @return 
     */
    public List<Predictions> findMatchForecast(Matches match, SdModels model) {
        List<Predictions> returnMatchForecast = session.createQuery("SELECT mf FROM MatchForecasts mf"
                + " JOIN FETCH mf.model"
                + " WHERE mf.matchForecast = :match"
                + " AND mf.model = :model")
                .setParameter("match", match)
                .setParameter("model", model)
                .setMaxResults(1)
                .setFetchSize(1)
                .getResultList();
        if (returnMatchForecast != null) {
            if (returnMatchForecast.isEmpty()) {
                LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Cannot find a match forecast: ")
                    .concat(Integer.toString(returnMatchForecast.size())));  
                return new LinkedList<>();
            }
            return returnMatchForecast;
        } else {
            return new LinkedList<>();
        }
    }
    
    /**
     * 
     * @param name
     * @param parmloadingBehavior
     * @param weightingBehavior
     * @return 
     */
    public List<SdModels> findModel(String name, String parmloadingBehavior, String weightingBehavior) {
        List<SdModels> returnModels = session.createQuery(" FROM SdModels m"
                .concat(" WHERE m.calculator = :name")
                .concat(" AND m.loadingBehavior = :loadingBehavior")
                .concat(" AND m.weightingBehavior = :weightingBehavior"))
                .setParameter("name", name)
                .setParameter("loadingBehavior", parmloadingBehavior)
                .setParameter("weightingBehavior", weightingBehavior)
                .setMaxResults(1)
                .setFetchSize(1)
                .getResultList();
        if (returnModels != null) {
            if (returnModels.isEmpty()) {
                LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Cannot find a model: ")
                    .concat(Integer.toString(returnModels.size())));  
                return new LinkedList<>();
            }
            return returnModels;
        } else {
            return new LinkedList<>();
        }   
    }

    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////   
}
