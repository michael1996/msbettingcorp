package mswettencorp.src.mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> SystemFiles <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 27.12.2017
 */
@Entity
@Immutable
@Table
public class SystemFiles implements java.io.Serializable {
   
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final long serialVersionUID = -5280171458285798273L;
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
    
    @Column(name = "fileName", nullable = false, length = 100)
    private String fileName;
    
    @Column(name = "fileReader", nullable = false, length = 50)
    private String fileReader;
    
    @Column(name = "isActive", nullable = false)
    private boolean isActive;

    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public SystemFiles() {

    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((fileName == null) ? 0 : fileName.hashCode());
        result = prime * result + ((fileReader == null) ? 0 : fileReader.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof SystemFiles)) return false;
        SystemFiles other = (SystemFiles) obj;
        if( fileName == null ) {
            if( other.fileName != null ) {
                return false;
            }
        } else if( !fileName.equals( other.fileName ) ) {
            return false;
        }
        if( fileReader == null ) {
            if( other.fileReader != null ) {
                return false;
            }
        } else if( !fileReader.equals( other.fileReader ) ) {
            return false;
        }
        return true;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileReader() {
        return fileReader;
    }

    public void setFileReader(String fileReader) {
        this.fileReader = fileReader;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
}
