package mswettencorp.src.mapping;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> Matchdays <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 18.07.2017
 */
@Entity
@Table
public class Matchdays implements java.io.Serializable {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final long serialVersionUID = -7046047603741480596L;

    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "competition", nullable = false)
    private SdCompetitions competition;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "status")
    private SdStatus status;
    
    @Column(name = "number", nullable = false, updatable = false, precision = 18, scale = 0)    
    private long number;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "startDate", nullable = false, length = 11)
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "endDate", nullable = false, length = 11)
    private Date endDate;
        
    @Column (name = "createdBy", length = 50)
    private String createdBy;
    
    @Column (name = "updatedBy", length = 50)
    private String updatedBy;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public Matchdays() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((competition == null) ? 0 : competition.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof Matchdays)) return false;
        Matchdays other = (Matchdays) obj;
        if( competition == null ) {
            if( other.competition != null ) {
                return false;
            }
        } else if( !competition.equals( other.competition ) ) {
            return false;
        }
        if (number != other.number) {
            return false;
        }
        return true;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SdCompetitions getCompetition() {
        return competition;
    }

    public void setCompetition(SdCompetitions competition) {
        this.competition = competition;
    }

    public SdStatus getStatus() {
        return status;
    }

    public void setStatus(SdStatus status) {
        this.status = status;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }
}
