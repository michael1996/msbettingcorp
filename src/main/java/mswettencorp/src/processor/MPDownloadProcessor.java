package mswettencorp.src.processor;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mswettencorp.src.download.AbstractDownloader;
import mswettencorp.src.download.AbstractDownloaderFactory;
import mswettencorp.src.download.DownloaderFactory;
import mswettencorp.src.mapping.SystemDownloads;
import mswettencorp.src.util.HibernateUtil;
import mswettencorp.src.util.LoggerUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> MPDownloadProcessor <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 10.01.2018
 */
public class MPDownloadProcessor {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final Logger LOGGER = LoggerUtil.getLogger();
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    private Session session;    
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    /**
     * 
     * @return 
     */
    public boolean initialize() {
        try {
            session = HibernateUtil.startConfiguredSession();  
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        } catch (HibernateException ex) {
            LOGGER.log(Level.SEVERE, null, ex.getMessage());
            return false;
        }
    }
    
    /**
     * 
     */
    public void deInitialize() {
        try {
            session = HibernateUtil.closeSession();
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": deinitialized succesfully."));
        } catch (HibernateException ex) {
            LOGGER.log(Level.SEVERE, null, ex.getMessage());
        } 
    }
    
    /**
     * 
     * @param ctxType
     * @param ctxValue 
     */
    public void process(String ctxType, String ctxValue) {
        List<SystemDownloads> findDownloads = session.createQuery(" FROM SystemDownloads sd"
                + " WHERE sd.isActive = true")
                .getResultList();
        if(!findDownloads.isEmpty()) {
            for (SystemDownloads download : findDownloads) {
                AbstractDownloaderFactory downloaderFactory = new DownloaderFactory();
                AbstractDownloader downloader = downloaderFactory.getDownloader(download.getDownloader(), download.getDownloadLink());
                if (downloader.initialize(session)) downloader.process();  
                downloader.deInitialize();
            }
        } else {
            LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": No Website Parser are active."));
        }
        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": Downloading files was succesfully."));
    }
}
