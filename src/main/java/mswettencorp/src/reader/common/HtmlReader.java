package mswettencorp.src.reader.common;

import java.io.IOException;
import java.net.URI;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.logging.Level;
import static mswettencorp.src.reader.common.AbstractReader.LOGGER;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.util.EntityUtils;
import org.hibernate.Session;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> HtmlReader <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 08.01.2018
 */
public class HtmlReader extends AbstractReader {
   
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    protected URIBuilder builder;
    protected URI uri;
    protected Document parseDoc;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public HtmlReader(String input) {
        super(input);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean initialize(Session session) {
        if (super.initialize(session)) {
            CloseableHttpAsyncClient httpclient = HttpAsyncClients.custom()
                .setDefaultRequestConfig(RequestConfig.custom()
                .setCookieSpec(CookieSpecs.STANDARD).build())
                .setRedirectStrategy(new LaxRedirectStrategy())
                .build();
            try {
                httpclient.start();
                HttpGet request = new HttpGet(uri);
                request.setHeader("Content-Type", "text/html; charset=UTF-8");
                request.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:25.0) Gecko/20100101 Firefox/25.0");
                request.setHeader("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8");
                request.setHeader("Accept-Language", "en-US,en;q=0.5");
                request.setHeader("Referer", "https://www.google.de/");
                Future<HttpResponse> futureRequest = httpclient.execute(request, null);
                HttpResponse response = futureRequest.get();
                int responseCode = response.getStatusLine().getStatusCode();
                //@TODO Handle different response Codes
                if (responseCode == 200) {
                    HttpEntity entity = response.getEntity(); 
                    String body = EntityUtils.toString(entity, "UTF-8"); 
                    parseDoc = Jsoup.parse(body);
                    return true;
                } else {
                    LOGGER.log(Level.SEVERE, getClass().getSimpleName().concat(": Response Code is ").concat(Integer.toString(responseCode)));
                    return false;
                }
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, ex.getMessage());
                return false;
            } catch (InterruptedException ex) {
                LOGGER.log(Level.SEVERE, ex.getMessage());
                return false;
            } catch (ExecutionException ex) {
                LOGGER.log(Level.SEVERE, ex.getMessage());
                return false;
            } finally {
                try {
                    httpclient.close();
                } catch (IOException ex) {
                    LOGGER.log(Level.SEVERE, ex.getMessage());
                }
            }
        }
        return false;
    }
    
    @Override
    public void deInitialize() {
        super.deInitialize();
    }
    
    @Override
    public void process() {

    }
   
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
