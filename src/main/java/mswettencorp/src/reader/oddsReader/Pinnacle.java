package mswettencorp.src.reader.oddsReader;

import java.net.URISyntaxException;
import java.util.logging.Level;
import mswettencorp.src.reader.common.OddsReader;
import org.apache.http.client.utils.URIBuilder;
import org.hibernate.Session;
import org.jsoup.select.Elements;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> Pinnacle <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 21.01.2017
 */
public class Pinnacle extends OddsReader {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final String URL = "www.pinnacle.com/";
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////          
    
    ////////////////////////////////////////////////////////////////////////////
    // Data Fields
    //////////////////////////////////////////////////////////////////////////// 
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public Pinnacle(String input) {
        super(input);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean initialize(Session session) {
        builder = new URIBuilder();
        builder.setScheme("https").setHost(URL).setPath(input);
        try {
            uri = builder.build();
        } catch (URISyntaxException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        }
        if (super.initialize(session)) {
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        }
        return false;
    }
    
    @Override
    public void deInitialize() {
        super.deInitialize();
    }
    
    @Override
    public void process() {   
        System.out.println(parseDoc.toString());  
        
        Elements gameName = parseDoc.getElementsByClass("game-name name");
        Elements gameMoneyline = parseDoc.getElementsByClass("oddTip game-moneyline");
        //System.out.println(gameName.toString());
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
