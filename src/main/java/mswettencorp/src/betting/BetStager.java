package mswettencorp.src.betting;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mswettencorp.src.mapping.BetsStaging;
import mswettencorp.src.mapping.Odds;
import mswettencorp.src.mapping.Predictions;
import mswettencorp.src.mapping.SdBetTyps;
import mswettencorp.src.util.HibernateUtil;
import mswettencorp.src.util.LoggerUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> BetStager <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 13.02.2018
 */
public class BetStager {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final Logger LOGGER = LoggerUtil.getLogger();  
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    private Session session;    
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    /**
     * 
     * @return 
     */
    public boolean initialize() {
        try {
            session = HibernateUtil.startConfiguredSession(); 
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        } catch (HibernateException ex) {
            LOGGER.log(Level.SEVERE, null, ex.getMessage());
            return false;
        }
    }
    
    /**
     * 
     */
    public void deInitialize() {
        try {
            session = HibernateUtil.closeSession(); 
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": deinitialized succesfully."));
        } catch (HibernateException ex) {
            LOGGER.log(Level.SEVERE, null, ex.getMessage());
        } 
    }
    
    /**
     * 
     * @param isHistoric 
     */
    public void process(boolean isHistoric) {        
        List<SdBetTyps> listBetTyps = session.createQuery(" FROM SdBetTyps sbt")
            .getResultList();
        List<Predictions> listMatchForecasts = session.createQuery(" FROM MatchForecasts mf"
            + " WHERE mf.isHistoric = :historic")
            .setParameter("historic", isHistoric)
            .getResultList();
        if (!listBetTyps.isEmpty()) {
            for (SdBetTyps betType : listBetTyps) {
                for (Predictions forecast : listMatchForecasts) {
                    // Hier ist ein Fehler, das vergangene Quoten genommen werden können und nicht nur aktuelle
                    // Unterschied Learning und Betting Processor
                    List<Odds> listOdds = forecast.getMatchForecast().getListOdds();
                    if (!listOdds.isEmpty()) {
                        List<Odds> listBestOdds = findBestOdd(listOdds);
                        String betResult = "X";
                        if (betType.getName().equals("3Way")) betResult = decisionTree3Way(forecast, listBestOdds);
                        if (betType.getName().equals("25")) betResult = decisionTreeOverUnder25(forecast, listBestOdds);
                        
                        
                        List<BetsStaging> listBetsStaging = forecast.getListBetsStaging();
                        BetsStaging createBetStaging = null;
                        if (!listBetsStaging.isEmpty()) {
                            for (BetsStaging stagedBet : listBetsStaging) {
                                if (stagedBet.getBetType().equals(betType)) createBetStaging = stagedBet;
                            }
                            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                                .concat(createBetStaging.toString())
                                .concat(" was updated."));
                        } else {
                            createBetStaging = new BetsStaging();
                            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                                .concat(createBetStaging.toString())
                                .concat(" was created."));
                        }
                        createBetStaging.setModelForecast(forecast);
                        createBetStaging.setBetType(betType);
                        createBetStaging.setBetResult(betResult);
                        if (forecast.isIsHistoric()) createBetStaging.setIsHistoric(true);
                        if (!forecast.isIsHistoric()) createBetStaging.setIsHistoric(false);
                        BigDecimal bestOdd;
                        switch(betResult) {
                            case "H":  
                                bestOdd = listBestOdds.get(0).getWinHomeOdd();
                                createBetStaging.setValue(bestOdd.subtract(forecast.getWinHomeOdd()));
                                createBetStaging.setBetBookmaker(listBestOdds.get(0).getBookmaker());
                                break;
                            case "D":
                                bestOdd = listBestOdds.get(1).getDrawOdd();
                                createBetStaging.setValue(bestOdd.subtract(forecast.getDrawOdd()));
                                createBetStaging.setBetBookmaker(listBestOdds.get(1).getBookmaker());
                                break;
                            case "A":
                                bestOdd = listBestOdds.get(2).getWinAwayOdd();
                                createBetStaging.setValue(bestOdd.subtract(forecast.getWinAwayOdd()));
                                createBetStaging.setBetBookmaker(listBestOdds.get(2).getBookmaker());
                                break;
                            case "O":
                                bestOdd = listBestOdds.get(3).getOver25Odd();
                                createBetStaging.setValue(bestOdd.subtract(forecast.getOver25Odd()));
                                createBetStaging.setBetBookmaker(listBestOdds.get(3).getBookmaker());
                                break;
                            case "U":
                                bestOdd = listBestOdds.get(4).getUnder25Odd();
                                createBetStaging.setValue(bestOdd.subtract(forecast.getUnder25Odd()));
                                createBetStaging.setBetBookmaker(listBestOdds.get(4).getBookmaker());
                                break;
                            default:
                                bestOdd = null;
                                break;
                        }
                        createBetStaging.setOdd(bestOdd);
                        session.persist(createBetStaging);
                    } else {
                        LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Cannot create a staged bet because no odds are available."));
                    }
                }
            }
        } else {
            LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Cannot find valid bet types."));
        }
        
        List<BetsStaging> listBetsStaging = session.createQuery(" FROM BetsStaging bs"
            + " WHERE bs.isHistoric = :historic")
            .setParameter("historic", isHistoric)
            .getResultList();
        for (BetsStaging betsStaged : listBetsStaging) {
//            if (betsStaged.getModelForecast().getMatchForecast().isIsHistoric()) {
                betsStaged.setIsHistoric(true);
                if (betsStaged.getBetType().getName().equals("3Way")) {
                    if (betsStaged.getBetResult().equals(betsStaged.getModelForecast().getMatchForecast().getFullTimeResult())) {
                        betsStaged.setIsCorrect(true);
                    } else {
                        betsStaged.setIsCorrect(false);
                    }
                }
                if (betsStaged.getBetType().getName().equals("25")) {
                    long fullTimeOverallGoals = betsStaged.getModelForecast().getMatchForecast().getFullTimeHomeGoals() + 
                            betsStaged.getModelForecast().getMatchForecast().getFullTimeAwayGoals();
                    if (betsStaged.getBetResult().equals("U")) {
                        if (fullTimeOverallGoals < 2.5) {
                            betsStaged.setIsCorrect(true);
                        } else {
                            betsStaged.setIsCorrect(false);
                        }
                    }
                    if (betsStaged.getBetResult().equals("O")) {
                        if (fullTimeOverallGoals > 2.5) {
                            betsStaged.setIsCorrect(true);
                        } else {
                            betsStaged.setIsCorrect(false);
                        }
                    }
                }
                session.persist(betsStaged);
                LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": Checked bet result of ")
                        .concat(betsStaged.toString()));
//            }     
        }
        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": BetStager was succesfully."));    
    }
    
    /**
     * 
     * @param forecast
     * @param listOdds
     * @return 
     */
    public String decisionTree3Way(Predictions forecast, List<Odds> listOdds) {
        String betResult = "X";
        double homeWinOddForecast = forecast.getWinHomeOdd().doubleValue();
        double drawOddForecast = forecast.getDrawOdd().doubleValue();
        double awayWinOddForecast = forecast.getWinAwayOdd().doubleValue();
        double homeWinOddReal = listOdds.get(0).getWinHomeOdd().doubleValue();
        double drawOddReal = listOdds.get(1).getDrawOdd().doubleValue();
        double awayWinOddReal = listOdds.get(2).getWinAwayOdd().doubleValue();
        
        double valueHomeWin = homeWinOddReal - homeWinOddForecast;
        double valueDraw = drawOddReal - drawOddForecast;
        double valueAwayWin = awayWinOddReal - awayWinOddForecast;
        
        if (valueHomeWin > valueDraw && valueHomeWin > valueAwayWin && valueHomeWin >= 0) {
            betResult = "H";
        }
        if (valueDraw >= valueHomeWin && valueDraw >= valueAwayWin && valueDraw >= 0) {
            betResult = "D";
        }
        if (valueAwayWin > valueHomeWin && valueAwayWin > valueDraw && valueAwayWin >= 0) {
            betResult = "A";
        }
        if (valueHomeWin < 0 && valueDraw < 0 && valueAwayWin < 0) {
            System.out.println("Error");
        }
        
        // Nach berechneten Quoten filtern
        return betResult;
    }
    
    /**
     * 
     * @param forecast
     * @param listOdds
     * @return 
     */
    public String decisionTreeOverUnder25(Predictions forecast, List<Odds> listOdds) {
        String betResult = "X";
        double over25Odd = forecast.getOver25Odd().doubleValue();
        double under25Odd = forecast.getUnder25Odd().doubleValue();
        if (over25Odd == under25Odd) {
            betResult = "X";
        } else {
            if (over25Odd < under25Odd) {
                betResult = "O";
            }
            if (over25Odd > under25Odd) {
                betResult = "U";
            } 
        }
        return betResult;
    }
    
    /**
     * 
     * @param listOdds
     * @return 
     */
    public List<Odds> findBestOdd(List<Odds> listOdds) {
        List<Odds> listReturnOdds = new LinkedList<>();
        Collections.sort(listOdds, new Comparator<Odds>() {
            @Override
            public int compare(Odds o1, Odds o2) {
                return o1.getWinHomeOdd().compareTo(o2.getWinHomeOdd());
            }
        }.reversed());
        listReturnOdds.add(listOdds.get(0));
        Collections.sort(listOdds, new Comparator<Odds>() {
            @Override
            public int compare(Odds o1, Odds o2) {
                return o1.getDrawOdd().compareTo(o2.getDrawOdd());
            }
        }.reversed());
        listReturnOdds.add(listOdds.get(0));
        Collections.sort(listOdds, new Comparator<Odds>() {
            @Override
            public int compare(Odds o1, Odds o2) {
                return o1.getWinAwayOdd().compareTo(o2.getWinAwayOdd());
            }
        }.reversed());
        listReturnOdds.add(listOdds.get(0));
        return listReturnOdds;
    }
}
