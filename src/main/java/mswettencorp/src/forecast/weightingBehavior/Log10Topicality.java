package mswettencorp.src.forecast.weightingBehavior;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.SdTeams;
import org.apache.commons.math3.analysis.function.Log10;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> Log10Topicality <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 11.02.2018
 */
public class Log10Topicality implements WeightingBehavior {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final int MAX = 10;
    private static final int MIN = 1;
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    private List<Double> listWeighting;
    private Log10 function;
    private double xCoordinate;
    private double xValue;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public Log10Topicality() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public List<Double> weighting(List<Matches> listMatches, SdTeams team) {
        listWeighting = new ArrayList<>(listMatches.size());
        function = new Log10();
        xCoordinate = MAX;
        xValue =(MAX - MIN) / (double)listMatches.size();
        for (Matches match : listMatches) {
            listWeighting.add(function.value(xCoordinate));
            xCoordinate = xCoordinate - xValue;
        }
        if (listWeighting.isEmpty()) {
            return new LinkedList<>();
        } else {
            return listWeighting;
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
