package mswettencorp.src.reader.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Level;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> FilesReader <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 08.01.2018
 */
public class FilesReader extends AbstractReader {
   
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final String FILEPATH = System.getProperty("user.dir")
            .concat("/src/main/java/mswettencorp/res/data/");
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    protected File file;
    protected BufferedReader bufferedReader;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public FilesReader(String input) {
        super(input);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean initialize(Session session) {
        if (super.initialize(session)) {
            try {
                file = new File(FILEPATH + input);
                bufferedReader = new BufferedReader(new java.io.FileReader(file));
                return true;
            } catch (FileNotFoundException ex) {
                LOGGER.log(Level.SEVERE, ex.getMessage());
                return false;
            }
        }
        return false;
    }
    
    @Override
    public void deInitialize() {
        super.deInitialize();
        try {
            if (bufferedReader != null) {
                bufferedReader.close();
            }
        } catch (IOException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException ex) {
                LOGGER.log(Level.SEVERE, ex.getMessage());
            }
        }
    }
    
    @Override
    public void process() {
        
    }
   
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    
}
