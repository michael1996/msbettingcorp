package mswettencorp.src.mapping;


import java.math.BigDecimal;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> SdCompetitions <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 18.07.2017
 */
@Entity
@Table
public class SdCompetitions implements java.io.Serializable {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final long serialVersionUID = -4409951888197453136L;

    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "league", nullable = false, updatable = false)
    private SdLeagues league;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "season", nullable = false, updatable = false)
    private SdSeasons season;
           
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "status", nullable = false)
    private SdStatus status;

    @OneToMany(mappedBy="competition")
    private Set<Matchdays> matchdayItems;
    
    @Column(name = "matchesPlayed", precision = 18, scale = 0)    
    private long matchesPlayed;
    
    @Column(name = "matchesRemaining", precision = 18, scale = 0)    
    private long matchesRemaining;
    
    @Column(name = "winsHomeTeam", precision = 12, scale = 4)    
    private BigDecimal winsHomeTeam;
    
    @Column(name = "draws", precision = 12, scale = 4)    
    private BigDecimal draws;
    
    @Column(name = "winsAwayTeam", precision = 12, scale = 4)    
    private BigDecimal winsAwayTeam;
    
    @Column(name = "homeGoals", precision = 12, scale = 4)    
    private BigDecimal homeGoals;
    
    @Column(name = "awayGoals", precision = 12, scale = 4)    
    private BigDecimal awayGoals;
    
    @Column(name = "scoredGoals", precision = 12, scale = 4)    
    private BigDecimal scoredGoals;
    
    @Column(name = "over25", precision = 12, scale = 4)    
    private BigDecimal over25;
    
    @Column(name = "under25", precision = 12, scale = 4)    
    private BigDecimal under25;

    ////////////////////////////////////////////////////////////////////////////
    // Contructor
    ////////////////////////////////////////////////////////////////////////////
    public SdCompetitions() {
        
    }
  
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((league == null) ? 0 : league.hashCode());
        result = prime * result + ((season == null) ? 0 : season.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof SdCompetitions)) return false;
        SdCompetitions other = (SdCompetitions) obj;
        if( league == null ) {
            if( other.league != null ) {
                return false;
            }
        } else if( !league.equals( other.league ) ) {
            return false;
        }
        if( season == null ) {
            if( other.season != null ) {
                return false;
            }
        } else if( !season.equals( other.season ) ) {
            return false;
        }
        return true;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SdLeagues getLeague() {
        return league;
    }

    public void setLeague(SdLeagues league) {
        this.league = league;
    }

    public SdSeasons getSeason() {
        return season;
    }

    public void setSeason(SdSeasons season) {
        this.season = season;
    }

    public SdStatus getStatus() {
        return status;
    }

    public void setStatus(SdStatus status) {
        this.status = status;
    }

    public long getMatchesPlayed() {
        return matchesPlayed;
    }

    public void setMatchesPlayed(long matchesPlayed) {
        this.matchesPlayed = matchesPlayed;
    }

    public long getMatchesRemaining() {
        return matchesRemaining;
    }

    public void setMatchesRemaining(long matchesRemaining) {
        this.matchesRemaining = matchesRemaining;
    }

    public BigDecimal getWinsHomeTeam() {
        return winsHomeTeam;
    }

    public void setWinsHomeTeam(BigDecimal winsHomeTeam) {
        this.winsHomeTeam = winsHomeTeam;
    }

    public BigDecimal getDraws() {
        return draws;
    }

    public void setDraws(BigDecimal draws) {
        this.draws = draws;
    }

    public BigDecimal getWinsAwayTeam() {
        return winsAwayTeam;
    }

    public void setWinsAwayTeam(BigDecimal winsAwayTeam) {
        this.winsAwayTeam = winsAwayTeam;
    }

    public BigDecimal getHomeGoals() {
        return homeGoals;
    }

    public void setHomeGoals(BigDecimal homeGoals) {
        this.homeGoals = homeGoals;
    }

    public BigDecimal getAwayGoals() {
        return awayGoals;
    }

    public void setAwayGoals(BigDecimal awayGoals) {
        this.awayGoals = awayGoals;
    }

    public BigDecimal getScoredGoals() {
        return scoredGoals;
    }

    public void setScoredGoals(BigDecimal scoredGoals) {
        this.scoredGoals = scoredGoals;
    }

    public BigDecimal getOver25() {
        return over25;
    }

    public void setOver25(BigDecimal over25) {
        this.over25 = over25;
    }

    public BigDecimal getUnder25() {
        return under25;
    }

    public void setUnder25(BigDecimal under25) {
        this.under25 = under25;
    }
}
