package mswettencorp.src.forecast.loadingBehavior;

import java.util.LinkedList;
import java.util.List;
import mswettencorp.src.mapping.Matches;
import org.hibernate.FlushMode;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> xxxxtermSide <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 14.01.2018
 */
public class xxxxtermSide implements LoadingBehavior {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    protected int numberMatchesMaximum;
    protected int numberMatchesMinimum;
    private List<Matches> homeTeamMatches;
    private List<Matches> awayTeamMatches;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public xxxxtermSide() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    /**
     * 
     * @param session
     * @param match
     * @param isHomeTeam
     * @return 
     */
    @Override
    public List<Matches> loading(Session session, Matches match, boolean isHomeTeam) {
        Session currentSession = session;
        if (isHomeTeam) {
            homeTeamMatches = currentSession.createQuery("SELECT m FROM Matches m"
                + " JOIN FETCH m.homeTeam"
                + " JOIN FETCH m.awayTeam"
                + " WHERE m.homeTeam = :homeTeam"
                + " AND m.date < :matchDate"
                + " AND m.isHistoric = true"
                + " ORDER BY m.date desc")
                .setParameter("homeTeam", match.getHomeTeam())
                .setParameter("matchDate", match.getStartDate())
                .setMaxResults(numberMatchesMaximum)
                .setFetchSize(numberMatchesMaximum)
                .setHibernateFlushMode(FlushMode.COMMIT)
                .getResultList();  
            if (homeTeamMatches != null) {
                if (!homeTeamMatches.isEmpty()) {
                    if (homeTeamMatches.size() <= numberMatchesMinimum) {
                        return new LinkedList<>();
                    }
                    return homeTeamMatches;
                } else {
                    return new LinkedList<>();
                }
            } else {
                return new LinkedList<>();
            }
        } else {
            awayTeamMatches = currentSession.createQuery("SELECT m FROM Matches m"
                + " JOIN FETCH m.homeTeam"
                + " JOIN FETCH m.awayTeam"
                + " WHERE m.awayTeam = :awayTeam"
                + " AND m.date < :matchDate"
                + " AND m.isHistoric = true"
                + " ORDER BY m.date desc")
                .setParameter("awayTeam", match.getAwayTeam())
                .setParameter("matchDate", match.getStartDate())
                .setMaxResults(numberMatchesMaximum)
                .setFetchSize(numberMatchesMaximum)
                .setHibernateFlushMode(FlushMode.COMMIT)
                .getResultList();  
            if (awayTeamMatches != null) {
                if (!awayTeamMatches.isEmpty()) {
                    if (awayTeamMatches.size() <= numberMatchesMinimum) {
                        return new LinkedList<>();
                    }
                    return awayTeamMatches;
                } else {
                    return new LinkedList<>();
                }
            } else {
                return new LinkedList<>();
            }          
        }   
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
