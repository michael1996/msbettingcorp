package mswettencorp.src.reader.oddsReader;

import java.net.URISyntaxException;
import java.util.logging.Level;
import mswettencorp.src.reader.common.OddsReader;
import org.apache.http.client.utils.URIBuilder;
import org.hibernate.Session;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> Interwetten <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 21.01.2017
 */
public class Interwetten extends OddsReader {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final String URL = "www.interwetten.com/";
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////          
    
    ////////////////////////////////////////////////////////////////////////////
    // Data Fields
    //////////////////////////////////////////////////////////////////////////// 
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public Interwetten(String input) {
        super(input);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean initialize(Session session) {
        builder = new URIBuilder();
        builder.setScheme("https").setHost(URL).setPath(input);
        try {
            uri = builder.build();
        } catch (URISyntaxException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        }
        if (super.initialize(session)) {
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        }
        return false;
    }
    
    @Override
    public void deInitialize() {
        super.deInitialize();
    }
    
    @Override
    public void process() {   
        Elements bets = parseDoc.getElementsByClass("b n3x");
        MatchOdds createMatchOdd;
        for (Element bet : bets) {
            createMatchOdd = new MatchOdds();
            createMatchOdd.homeTeam = bet.select("[itemprop=homeTeam]").get(0).child(0).ownText(); 
            createMatchOdd.awayTeam = bet.select("[itemprop=awayTeam]").get(0).child(0).ownText(); 
            createMatchOdd.winHomeOdd = Double.parseDouble(bet.select("[itemprop=homeTeam]").get(0).child(1).ownText().replace(",", ".")); 
            createMatchOdd.drawOdd = Double.parseDouble(bet.child(1).child(0).child(1).ownText().replace(",", "."));
            createMatchOdd.winAwayOdd = Double.parseDouble(bet.select("[itemprop=awayTeam]").get(0).child(1).ownText().replace(",", ".")); 
            listMatchOdds.add(createMatchOdd);
        }      
        super.process();            
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
