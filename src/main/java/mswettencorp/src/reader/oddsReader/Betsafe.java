package mswettencorp.src.reader.oddsReader;

import java.net.URISyntaxException;
import java.util.logging.Level;
import mswettencorp.src.reader.common.OddsReader;
import org.apache.http.client.utils.URIBuilder;
import org.hibernate.Session;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> Betsafe <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 10.02.2017
 */
public class Betsafe extends OddsReader {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final String URL = "www.betsafe.com";
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////          
    
    ////////////////////////////////////////////////////////////////////////////
    // Data Fields
    //////////////////////////////////////////////////////////////////////////// 
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public Betsafe(String input) {
        super(input);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean initialize(Session session) {
        builder = new URIBuilder();
        try {
            System.out.println(input);
            builder.setScheme("https").setHost(URL).setPath("de/sportwetten")
                .setFragment("/")
                .setFragment("/Mainmarket/16");
            uri = builder.build();
            System.out.println(uri);
        } catch (URISyntaxException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
            return false;
        }
        if (super.initialize(session)) {
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        }
        return false;
    }
    
    @Override
    public void deInitialize() {
        super.deInitialize();
    }
    
    @Override
    public void process() {   
        System.out.println(parseDoc.toString());
        Elements bets = parseDoc.getElementsByClass("dvEventNameProview");
        MatchOdds createMatchOdd;
        for (Element bet : bets) {
            String splitTeam[] = bet.child(0).ownText().split("-");
            System.out.println(splitTeam[0].trim() + " : " + splitTeam[1].trim());
//            createMatchOdd = new matchOdds();
//            createMatchOdd.homeTeam = bet.select("[itemprop=homeTeam]").get(0).child(0).ownText(); 
//            createMatchOdd.awayTeam = bet.select("[itemprop=awayTeam]").get(0).child(0).ownText(); 
//            createMatchOdd.winHomeOdd = Double.parseDouble(bet.select("[itemprop=homeTeam]").get(0).child(1).ownText().replace(",", ".")); 
//            createMatchOdd.drawOdd = Double.parseDouble(bet.child(1).child(0).child(1).ownText().replace(",", "."));
//            createMatchOdd.winAwayOdd = Double.parseDouble(bet.select("[itemprop=awayTeam]").get(0).child(1).ownText().replace(",", ".")); 
//            listMatchOdds.add(createMatchOdd);
        }  
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
