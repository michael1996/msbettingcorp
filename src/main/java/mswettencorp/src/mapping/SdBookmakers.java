package mswettencorp.src.mapping;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> SdBookmakers <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 18.07.2017
 */
@Entity
@Immutable
@Table
public class SdBookmakers implements java.io.Serializable {

    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final long serialVersionUID = -5602251652886135471L;
      
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
    
    @Column (name = "name", nullable = false, length = 50)
    private String name;
    
    @Column (name = "website", nullable = false, length = 100)
    private String website;
    
    @Column (name = "taxes", nullable = false, precision = 12, scale = 4)
    private BigDecimal taxes;
    
    @Column(name = "stakeMinimum", nullable = false, precision = 12, scale = 2)
    private BigDecimal stakeMinimum;
    
    @Column(name = "stakeMaximum", nullable = false, precision = 12, scale = 2)
    private BigDecimal stakeMaximum;
    
    @Column(name = "profitMaximum", nullable = false, precision = 12, scale = 2)
    private BigDecimal profitMaximum;
    
    @Column(name = "amountBetsMaximum", nullable = false, precision = 18, scale = 0)
    private long amountBetsMaximum;
    
    @Column(name = "stakePerBillMinimum", nullable = false, precision = 18, scale = 0)
    private long stakePerBillMinimum;
      
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public SdBookmakers() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof SdBookmakers)) return false;
        SdBookmakers other = (SdBookmakers) obj;
        if( name == null ) {
            if( other.name != null ) {
                return false;
            }
        } else if( !name.equals( other.name ) ) {
            return false;
        }
        return true;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public BigDecimal getTaxes() {
        return taxes;
    }

    public void setTaxes(BigDecimal taxes) {
        this.taxes = taxes;
    }

    public BigDecimal getStakeMinimum() {
        return stakeMinimum;
    }

    public void setStakeMinimum(BigDecimal stakeMinimum) {
        this.stakeMinimum = stakeMinimum;
    }

    public BigDecimal getProfitMaximum() {
        return profitMaximum;
    }

    public void setProfitMaximum(BigDecimal profitMaximum) {
        this.profitMaximum = profitMaximum;
    }

    public long getAmountBetsMaximum() {
        return amountBetsMaximum;
    }

    public void setAmountBetsMaximum(long amountBetsMaximum) {
        this.amountBetsMaximum = amountBetsMaximum;
    }

    public BigDecimal getStakeMaximum() {
        return stakeMaximum;
    }

    public void setStakeMaximum(BigDecimal stakeMaximum) {
        this.stakeMaximum = stakeMaximum;
    }

    public long getStakePerBillMinimum() {
        return stakePerBillMinimum;
    }

    public void setStakePerBillMinimum(long stakePerBillMinimum) {
        this.stakePerBillMinimum = stakePerBillMinimum;
    }
}