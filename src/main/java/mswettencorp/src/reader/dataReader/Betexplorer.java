package mswettencorp.src.reader.dataReader;

import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import mswettencorp.src.mapping.SdCompetitions;
import mswettencorp.src.mapping.SdLeagues;
import mswettencorp.src.reader.common.HtmlReader;
import org.apache.http.client.utils.URIBuilder;
import org.hibernate.Session;
import org.jsoup.select.Elements;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> Betexplorer <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 21.02.2017
 */
public class Betexplorer extends HtmlReader {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final String URL = "www.betexplorer.com/";
    private static final String PATH = "soccer/";
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////          
    
    ////////////////////////////////////////////////////////////////////////////
    // Data Fields
    //////////////////////////////////////////////////////////////////////////// 
    private long matchesPlayed;  
    private long matchesRemaining; 
    private double winsHomeTeam; 
    private double draws;
    private double winsAwayTeam;
    private double homeGoals;
    private double awayGoals;
    private double scoredGoals;
    private double over25;
    private double under25;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public Betexplorer(String input) {
        super(input);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean initialize(Session session) {
        builder = new URIBuilder();
        if (input.contains("?")) {
            String split[] = input.split("\\?");
            String parameterSplit[] = split[1].split("=");
            builder.setScheme("http").setHost(URL).setPath(PATH.concat(split[0])).setParameter(parameterSplit[0], parameterSplit[1]); 
        } else {
            builder.setScheme("http").setHost(URL).setPath(PATH.concat(input));
        }
        try {
            uri = builder.build();
        } catch (URISyntaxException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        }
        if (super.initialize(session)) {
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        }
        return false;
    }
    
    @Override
    public void deInitialize() {
        super.deInitialize();
    }
    
    @Override
    public void process() {
        Elements competitionElements = parseDoc.getElementsByClass("wrap-section__header__title");
        Elements tableElements = parseDoc.getElementsByClass("table-main leaguestats");
        Elements trElement = tableElements.get(0).select("tr");
        String split[] = competitionElements.get(0).ownText().split(" "); 
        String splitSeason[] = new String[2];
        String league = "";
        switch (split.length) {
            case 2:
                splitSeason = split[1].split("/");
                league = split[0].toLowerCase();
                break;
            case 3:
                splitSeason= split[2].split("/");
                league = split[0].toLowerCase().concat(split[1].toLowerCase());
                break;
            default:
                break;
        }
        String correctedSeason = correctSeason(splitSeason[0], splitSeason[1]);
        if (correctedSeason.isEmpty()) {
            return;
        }
        matchesPlayed = Long.valueOf(trElement.get(1).child(1).ownText());  
        matchesRemaining = Long.valueOf(trElement.get(2).child(1).ownText()); 
        winsHomeTeam = Double.valueOf(trElement.get(4).child(1).ownText()) / matchesPlayed;
        draws = Double.valueOf(trElement.get(5).child(1).ownText())  / matchesPlayed;
        winsAwayTeam = Double.valueOf(trElement.get(6).child(1).ownText()) / matchesPlayed;
        scoredGoals = Double.valueOf(trElement.get(8).child(1).ownText()) / matchesPlayed;
        homeGoals = Double.valueOf(trElement.get(9).child(1).ownText()) / matchesPlayed;
        awayGoals = Double.valueOf(trElement.get(10).child(1).ownText()) / matchesPlayed;
        over25 = Double.valueOf(trElement.get(12).child(1).ownText()) / matchesPlayed;
        under25  = Double.valueOf(trElement.get(13).child(1).ownText()) / matchesPlayed;
        
        List<SdCompetitions> listCompetitions = findCompetition(league, correctedSeason);
        if (!listCompetitions.isEmpty()) {
            SdLeagues findLeague = listCompetitions.get(0).getLeague();
            SdCompetitions updateCompetition = listCompetitions.get(0);
            if ((matchesPlayed + matchesRemaining) > findLeague.getNumberMatches()) {
//                    || (winsHomeTeam + draws + winsAwayTeam) != 1
//                    || (homeGoals + awayGoals) != scoredGoals
//                    || (under25 + over25) != 1) {
                LOGGER.log(Level.SEVERE, getClass().getSimpleName().concat(": Error in competition statistics:")
                    .concat(updateCompetition.toString()));
                return;
            }
            updateCompetition.setMatchesPlayed(matchesPlayed);
            updateCompetition.setMatchesRemaining(matchesRemaining);
            updateCompetition.setWinsHomeTeam(BigDecimal.valueOf(winsHomeTeam));
            updateCompetition.setDraws(BigDecimal.valueOf(draws));
            updateCompetition.setWinsAwayTeam(BigDecimal.valueOf(winsAwayTeam));
            updateCompetition.setScoredGoals(BigDecimal.valueOf(scoredGoals));
            updateCompetition.setHomeGoals(BigDecimal.valueOf(homeGoals));
            updateCompetition.setAwayGoals(BigDecimal.valueOf(awayGoals));
            updateCompetition.setOver25(BigDecimal.valueOf(over25));
            updateCompetition.setUnder25(BigDecimal.valueOf(under25));
            session.persist(updateCompetition);
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                .concat(updateCompetition.toString())
                .concat(" was updated."));
        }
        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": Processing league data was succesfully."));
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
