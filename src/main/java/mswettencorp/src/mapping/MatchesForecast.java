package mswettencorp.src.mapping;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Formula;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> MatchesForecast <br>
 *
 * @author Michael Werner
 * @version v1.1
 * @since 16.03.2018
 */
@Entity
@Table
public class MatchesForecast implements java.io.Serializable {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final long serialVersionUID = -5280171458285798273L;
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
    
    @OneToMany(mappedBy="matchOdds", fetch=FetchType.LAZY)
    private List<Odds> listOdds;
    
    @OneToMany(mappedBy="matchForecast", fetch=FetchType.LAZY)
    private List<Predictions> listMatchForecasts;    
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "matchday", nullable = false)
    private Matchdays matchday;
         
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "homeTeam", nullable = false, updatable = false)
    private SdTeams homeTeam;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "awayTeam", nullable = false, updatable = false)
    private SdTeams awayTeam;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "status", nullable = false)
    private SdStatus status;
    
    @Column(name = "fullTimeResult", length = 1)
    private String fullTimeResult;
        
    @Column(name = "halfTimeResult", length = 1)
    private String halfTimeResult;
       
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "startDate", nullable = false, length = 11)
    private Date startDate;  
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "endDate", nullable = false, length = 11)
    private Date endDate;  
    
    @Formula("fullTimeHomeGoals+fullTimeAwayGoals")
    private long fullTimeSumGoals;
    
    @Column(name = "fullTimeHomeGoals", precision = 18, scale = 0)    
    private long fullTimeHomeGoals;
    
    @Column(name = "fullTimeAwayGoals", precision = 18, scale = 0)    
    private long fullTimeAwayGoals;
    
    @Column(name = "halfTimeHomeGoals", precision = 18, scale = 0)    
    private long halfTimeHomeGoals;
     
    @Column(name = "halfTimeAwayGoals", precision = 18, scale = 0)    
    private long halfTimeAwayGoals;
    
    @Column(name = "crowdAttendence", precision = 18, scale = 0)
    private long crowdAttendence;
     
    @Column(name = "homeTeamShots", precision = 18, scale = 0)
    private long homeTeamShots;
    
    @Column(name = "homeTeamShotsOt", precision = 18, scale = 0)
    private long homeTeamShotsOt;
    
    //@TODO HHW = Home Team Hit Woodwork
    
    @Column(name = "homeTeamCorners", precision = 18, scale = 0)
    private long homeTeamCorners;
    
    @Column(name = "homeTeamFouls", precision = 18, scale = 0)
    private long homeTeamFouls;
    
    @Column(name = "homeTeamOffsides", precision = 18, scale = 0)
    private long homeTeamOffsides;
    
    @Column(name = "homeTeamYellowCards", precision = 18, scale = 0)
    private long homeTeamYellowCards;
    
    @Column(name = "homeTeamRedCards", precision = 18, scale = 0)
    private long homeTeamRedCards; 
    
    @Column(name = "homeTeamPossession", precision = 12, scale = 4)
    private BigDecimal homeTeamPossession;
    
    @Column(name = "homeTeamElo", precision = 12, scale = 4)
    private BigDecimal homeTeamElo;
    
    @Column(name = "awayTeamShots", precision = 18, scale = 0)
    private long awayTeamShots;
    
    @Column(name = "awayTeamShotsOt", precision = 18, scale = 0)
    private long awayTeamShotsOt;
    
    //@TODO AHW = Away Team Hit Woodwork
    
    @Column(name = "awayTeamCorners", precision = 18, scale = 0)
    private long awayTeamCorners;  
    
    @Column(name = "awayTeamFouls", precision = 18, scale = 0)
    private long awayTeamFouls;
    
    @Column(name = "awayTeamOffsides", precision = 18, scale = 0)
    private long awayTeamOffsides;
    
    @Column(name = "awayTeamYellowCards", precision = 18, scale = 0)
    private long awayTeamYellowCards;
    
    @Column(name = "awayTeamRedCards", precision = 18, scale = 0)
    private long awayTeamRedCards;
    
    @Column(name = "awayTeamPossession", precision = 12, scale = 4)
    private BigDecimal awayTeamPossession;
    
    @Column(name = "awayTeamElo", precision = 12, scale = 4)
    private BigDecimal awayTeamElo;

    @Column (name = "createdBy", length = 50)
    private String createdBy;
    
    @Column (name = "updatedBy", length = 50)
    private String updatedBy;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public MatchesForecast() {

    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
        result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
        result = prime * result + ((matchday == null) ? 0 : matchday.hashCode());
        result = prime * result + ((homeTeam == null) ? 0 : homeTeam.hashCode());
        result = prime * result + ((awayTeam == null) ? 0 : awayTeam.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof MatchesForecast)) return false;
        MatchesForecast other = (MatchesForecast) obj;
        if( startDate == null ) {
            if( other.startDate != null ) {
                return false;
            }
        } else if( !startDate.equals( other.startDate ) ) {
            return false;
        }
        if( endDate == null ) {
            if( other.endDate != null ) {
                return false;
            }
        } else if( !endDate.equals( other.endDate ) ) {
            return false;
        }
        if( matchday == null ) {
            if( other.matchday != null ) {
                return false;
            }
        } else if( !matchday.equals( other.matchday ) ) {
            return false;
        }
        if( homeTeam == null ) {
            if( other.homeTeam != null ) {
                return false;
            }
        } else if( !homeTeam.equals( other.homeTeam ) ) {
            return false;
        }
        if( awayTeam == null ) {
            if( other.awayTeam != null ) {
                return false;
            }
        } else if( !awayTeam.equals( other.awayTeam ) ) {
            return false;
        }
        return true;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Matchdays getMatchday() {
        return matchday;
    }

    public void setMatchday(Matchdays matchday) {
        this.matchday = matchday;
    }

    public SdTeams getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(SdTeams homeTeam) {
        this.homeTeam = homeTeam;
    }

    public SdTeams getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(SdTeams awayTeam) {
        this.awayTeam = awayTeam;
    }

    public SdStatus getStatus() {
        return status;
    }

    public void setStatus(SdStatus status) {
        this.status = status;
    }

    public String getFullTimeResult() {
        return fullTimeResult;
    }

    public void setFullTimeResult(String fullTimeResult) {
        this.fullTimeResult = fullTimeResult;
    }

    public String getHalfTimeResult() {
        return halfTimeResult;
    }

    public void setHalfTimeResult(String halfTimeResult) {
        this.halfTimeResult = halfTimeResult;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public long getFullTimeHomeGoals() {
        return fullTimeHomeGoals;
    }

    public void setFullTimeHomeGoals(long fullTimeHomeGoals) {
        this.fullTimeHomeGoals = fullTimeHomeGoals;
    }

    public long getFullTimeAwayGoals() {
        return fullTimeAwayGoals;
    }

    public void setFullTimeAwayGoals(long fullTimeAwayGoals) {
        this.fullTimeAwayGoals = fullTimeAwayGoals;
    }

    public long getHalfTimeHomeGoals() {
        return halfTimeHomeGoals;
    }

    public void setHalfTimeHomeGoals(long halfTimeHomeGoals) {
        this.halfTimeHomeGoals = halfTimeHomeGoals;
    }

    public long getHalfTimeAwayGoals() {
        return halfTimeAwayGoals;
    }

    public void setHalfTimeAwayGoals(long halfTimeAwayGoals) {
        this.halfTimeAwayGoals = halfTimeAwayGoals;
    }

    public long getCrowdAttendence() {
        return crowdAttendence;
    }

    public void setCrowdAttendence(long crowdAttendence) {
        this.crowdAttendence = crowdAttendence;
    }

    public long getHomeTeamShots() {
        return homeTeamShots;
    }

    public void setHomeTeamShots(long homeTeamShots) {
        this.homeTeamShots = homeTeamShots;
    }

    public long getHomeTeamShotsOt() {
        return homeTeamShotsOt;
    }

    public void setHomeTeamShotsOt(long homeTeamShotsOt) {
        this.homeTeamShotsOt = homeTeamShotsOt;
    }

    public long getHomeTeamCorners() {
        return homeTeamCorners;
    }

    public void setHomeTeamCorners(long homeTeamCorners) {
        this.homeTeamCorners = homeTeamCorners;
    }

    public long getHomeTeamFouls() {
        return homeTeamFouls;
    }

    public void setHomeTeamFouls(long homeTeamFouls) {
        this.homeTeamFouls = homeTeamFouls;
    }

    public long getHomeTeamOffsides() {
        return homeTeamOffsides;
    }

    public void setHomeTeamOffsides(long homeTeamOffsides) {
        this.homeTeamOffsides = homeTeamOffsides;
    }

    public long getHomeTeamYellowCards() {
        return homeTeamYellowCards;
    }

    public void setHomeTeamYellowCards(long homeTeamYellowCards) {
        this.homeTeamYellowCards = homeTeamYellowCards;
    }

    public long getHomeTeamRedCards() {
        return homeTeamRedCards;
    }

    public void setHomeTeamRedCards(long homeTeamRedCards) {
        this.homeTeamRedCards = homeTeamRedCards;
    }

    public BigDecimal getHomeTeamPossession() {
        return homeTeamPossession;
    }

    public void setHomeTeamPossession(BigDecimal homeTeamPossession) {
        this.homeTeamPossession = homeTeamPossession;
    }

    public BigDecimal getHomeTeamElo() {
        return homeTeamElo;
    }

    public void setHomeTeamElo(BigDecimal homeTeamElo) {
        this.homeTeamElo = homeTeamElo;
    }

    public long getAwayTeamShots() {
        return awayTeamShots;
    }

    public void setAwayTeamShots(long awayTeamShots) {
        this.awayTeamShots = awayTeamShots;
    }

    public long getAwayTeamShotsOt() {
        return awayTeamShotsOt;
    }

    public void setAwayTeamShotsOt(long awayTeamShotsOt) {
        this.awayTeamShotsOt = awayTeamShotsOt;
    }

    public long getAwayTeamCorners() {
        return awayTeamCorners;
    }

    public void setAwayTeamCorners(long awayTeamCorners) {
        this.awayTeamCorners = awayTeamCorners;
    }

    public long getAwayTeamFouls() {
        return awayTeamFouls;
    }

    public void setAwayTeamFouls(long awayTeamFouls) {
        this.awayTeamFouls = awayTeamFouls;
    }

    public long getAwayTeamOffsides() {
        return awayTeamOffsides;
    }

    public void setAwayTeamOffsides(long awayTeamOffsides) {
        this.awayTeamOffsides = awayTeamOffsides;
    }

    public long getAwayTeamYellowCards() {
        return awayTeamYellowCards;
    }

    public void setAwayTeamYellowCards(long awayTeamYellowCards) {
        this.awayTeamYellowCards = awayTeamYellowCards;
    }

    public long getAwayTeamRedCards() {
        return awayTeamRedCards;
    }

    public void setAwayTeamRedCards(long awayTeamRedCards) {
        this.awayTeamRedCards = awayTeamRedCards;
    }

    public BigDecimal getAwayTeamPossession() {
        return awayTeamPossession;
    }

    public void setAwayTeamPossession(BigDecimal awayTeamPossession) {
        this.awayTeamPossession = awayTeamPossession;
    }

    public BigDecimal getAwayTeamElo() {
        return awayTeamElo;
    }

    public void setAwayTeamElo(BigDecimal awayTeamElo) {
        this.awayTeamElo = awayTeamElo;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public List<Odds> getListOdds() {
        return listOdds;
    }

    public void setListOdds(List<Odds> listOdds) {
        this.listOdds = listOdds;
    }

    public List<Predictions> getListMatchForecasts() {
        return listMatchForecasts;
    }

    public void setListMatchForecasts(List<Predictions> listMatchForecasts) {
        this.listMatchForecasts = listMatchForecasts;
    }

    public long getFullTimeSumGoals() {
        return fullTimeSumGoals;
    }
}
