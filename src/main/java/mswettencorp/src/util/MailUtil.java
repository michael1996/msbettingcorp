package mswettencorp.src.util;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> MailUtil <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 16.01.2017
 */
public class MailUtil {
    //@TODO nicht fest im Programm definieren, sondern in einer eigenen DB Relation
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    protected static final Logger LOGGER = LoggerUtil.getLogger();
    protected static final String TOFIRST = "michael@stbwerner.com";
    protected static final String TOSECOND = "Stefan.werner@rwth-Aachen.de";
    protected static final String FROM = "mswettencorp@gmail.com";
    protected static final String PASSWORD = "MS25!Wetten1_Corp1996";
    protected static final String HOST = "smtp.gmail.com";  
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Data Fields
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public MailUtil() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    /**
     * 
     * @param subject
     * @param messageText 
     */
    public void sendMailWithoutAttachment(String subject, String messageText) {   
        Properties properties = System.getProperties();
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", HOST);
        properties.put("mail.smtp.user", FROM);
        properties.put("mail.smtp.password", PASSWORD);
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.auth", "true");
       
        Session session = Session.getDefaultInstance(properties);
        MimeMessage message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(FROM));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(TOFIRST));
            message.addRecipient(Message.RecipientType.CC, new InternetAddress(TOSECOND));
            message.setSubject(subject);
            message.setText(messageText);
            
            try (Transport transport = session.getTransport("smtp")) {
                transport.connect(HOST, FROM, PASSWORD);
                transport.sendMessage(message, message.getAllRecipients());
            }
            LOGGER.log(Level.INFO, HibernateUtil.class.getSimpleName().concat(": Sent message successfully to")
                .concat(TOFIRST)
                .concat(", ")
                .concat(TOSECOND));
        } catch (MessagingException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        }
    }
    
    /**
     * 
     * @param subject
     * @param messageText
     * @param file 
     */
    public void sendMailWithAttachment(String subject, String messageText, String file) {   
        Properties properties = System.getProperties();
        properties.put("mail.smtp.starttls.enable", "true");
        properties.put("mail.smtp.host", HOST);
        properties.put("mail.smtp.user", FROM);
        properties.put("mail.smtp.password", PASSWORD);
        properties.put("mail.smtp.port", "587");
        properties.put("mail.smtp.auth", "true");
       
        Session session = Session.getDefaultInstance(properties);
        MimeMessage message = new MimeMessage(session);
        try {
            message.setFrom(new InternetAddress(FROM));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(TOFIRST));
            message.addRecipient(Message.RecipientType.CC, new InternetAddress(TOSECOND));
            message.setSubject(subject);
            message.setText(messageText);
            
            try (Transport transport = session.getTransport("smtp")) {
                transport.connect(HOST, FROM, PASSWORD);
                transport.sendMessage(message, message.getAllRecipients());
            }
            LOGGER.log(Level.INFO, HibernateUtil.class.getSimpleName().concat(": Sent message successfully to")
                .concat(TOFIRST)
                .concat(", ")
                .concat(TOSECOND));
        } catch (MessagingException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
        }
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
