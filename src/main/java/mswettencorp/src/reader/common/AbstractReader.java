
package mswettencorp.src.reader.common;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mswettencorp.src.mapping.SdCompetitions;
import mswettencorp.src.mapping.SdStatus;
import mswettencorp.src.mapping.SdTeams;
import mswettencorp.src.mapping.SystemMappingTeams;
import mswettencorp.src.util.LoggerUtil;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> AbstractReader <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 08.01.2018
 */
public abstract class AbstractReader {
 
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    protected static final Logger LOGGER = LoggerUtil.getLogger();
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    protected Session session; 
    protected String input;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public AbstractReader(String input) {
        this.input = input;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    /**
     * 
     * @param session 
     * @return  
     */
    public boolean initialize(Session session) {
        this.session = session;
        return true;
    }
    
    public void deInitialize() {
        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": deinitialized succesfully."));
    }
    
    public abstract void process(); 
    
    /**
     * 
     * @param seasonFirstPart
     * @param seasonSecondPart
     * @return 
     */
    public String correctSeason(String seasonFirstPart, String seasonSecondPart) {
        String correctedSeason = seasonFirstPart.substring(2, 4)
            .concat("/")
            .concat(seasonSecondPart.substring(2, 4));
        return correctedSeason;
    }
    
    /**
     * 
     * @param date
     * @param time
     * @return 
     */
    public Date combineDateTime(Date date, Date time) {
        Calendar calendarA = Calendar.getInstance();
	calendarA.setTime(date);
	Calendar calendarB = Calendar.getInstance();
	calendarB.setTime(time);
 
	calendarA.set(Calendar.HOUR_OF_DAY, calendarB.get(Calendar.HOUR_OF_DAY));
	calendarA.set(Calendar.MINUTE, calendarB.get(Calendar.MINUTE));
	calendarA.set(Calendar.SECOND, calendarB.get(Calendar.SECOND));
	calendarA.set(Calendar.MILLISECOND, calendarB.get(Calendar.MILLISECOND));
 	return calendarA.getTime();
    }
    
    /**
     * 
     * @param startDate
     * @param endDate
     * @return 
     */
    public List<SdStatus> checkStatus(Date startDate, Date endDate) {
        Date currentDate = new Date();
        String status = "";
        if (currentDate.before(endDate) && currentDate.after(startDate)) {
            status = "running";
        } else if (currentDate.before(startDate)) {
            status = "starting";
        } else if (currentDate.after(endDate)) {
            status = "finished";
        } 
        List<SdStatus> returnStatus = findStatus(status);
        if (returnStatus.isEmpty()) {
            return new LinkedList<>();
        }
        return returnStatus;    
    }
    
    /**
     * 
     * @param mappingName
     * @return 
     */
    public List<SystemMappingTeams> mappingTeamsTeam(String mappingName) {
        String correctedMappingName = mappingName;
        correctedMappingName = (correctedMappingName.contains(" FC") ? correctedMappingName.replace(" FC", "").trim() : correctedMappingName);
        correctedMappingName = ((correctedMappingName.contains("FC ") && correctedMappingName.charAt(0) == 'F' ) 
                ? correctedMappingName.replace("FC ", "").trim() : correctedMappingName);
        correctedMappingName = (correctedMappingName.contains("Man ") ? correctedMappingName.replace("Man ", "Manchester ").trim() : correctedMappingName); 
        correctedMappingName = (correctedMappingName.contains(" Utd") ? correctedMappingName.replace(" Utd", " United").trim() : correctedMappingName); 
        correctedMappingName = ((correctedMappingName.contains("Ein ") && correctedMappingName.charAt(0) == 'E' ) 
                ? correctedMappingName.replace("Ein ", "Eintracht ").trim() : correctedMappingName);
        
        List<SystemMappingTeams> returnMappingTeams = session.createQuery("SELECT smt"
                + " FROM SystemMappingTeams smt"
                + " WHERE smt.mappingName = :name")
                .setParameter("name", mappingName)
                .setMaxResults(1)
                .setFetchSize(1)
                .getResultList();  
        if (returnMappingTeams.isEmpty()) {
            LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Cannot find a TeamMapping."));
            List<SdTeams> listTeams = findTeam(correctedMappingName);
            SystemMappingTeams createMappingTeam = new SystemMappingTeams();
            if (!listTeams.isEmpty()) {
                SdTeams returnTeam = listTeams.get(0);
                createMappingTeam.setOriginalName(returnTeam.getName());
                createMappingTeam.setMappingName(mappingName);
                createMappingTeam.setCreatedBy(getClass().getSimpleName());
                LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                    .concat(createMappingTeam.toString())
                    .concat(" was created."));
                returnMappingTeams.add(createMappingTeam);
            } else {
                createMappingTeam.setOriginalName(null);
                createMappingTeam.setMappingName(mappingName);
                createMappingTeam.setCreatedBy(getClass().getSimpleName());
                createMappingTeam.setUpdatedBy("Michael Werner");
                LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": ")
                    .concat(createMappingTeam.getMappingName())
                    .concat(" cannot be created. Please check."));
            }
            session.persist(createMappingTeam);
        }
        return returnMappingTeams;
    } 
    
    /**
     * 
     * @param teamName
     * @return 
     */
    public List<SdTeams> findTeam(String teamName) {
        List<SdTeams> returnTeams = session.createQuery("SELECT t FROM SdTeams t"
                + " WHERE t.name LIKE :name")
                .setParameter("name", "%".concat(teamName).concat("%"))
                .setMaxResults(1)
                .setFetchSize(1)
                .getResultList();
        if (returnTeams.isEmpty()) {
            LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Cannot find a Team."));  
            return new LinkedList<>();
        }
        return returnTeams;
    }
    
    /**
     * 
     * @param league
     * @param season
     * @return 
     */
    public List<SdCompetitions> findCompetition(String league, String season) {
        List<SdCompetitions> returnCompetitions = session.createQuery("SELECT c"
                + " FROM SdCompetitions c, SdLeagues l, SdSeasons s"
                + " WHERE c.league = l.id"
                + " AND c.season = s.id"
                + " AND l.name = :league"
                + " AND s.name = :season")
                .setParameter("league", league)
                .setParameter("season", season)
                .setMaxResults(1)
                .setFetchSize(1)
                .getResultList();
        if (returnCompetitions.isEmpty()) {
            LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Cannot find a Competition."));  
            return new LinkedList<>();
        }
        return returnCompetitions;
    }
    
    /**
     * 
     * @param status
     * @return 
     */
    public List<SdStatus> findStatus(String status) {
        List<SdStatus> returnStatus = session.createQuery(" FROM SdStatus s"
                + " WHERE s.name = :name")
                .setParameter("name", status)
                .setMaxResults(1)
                .setFetchSize(1)
                .getResultList();
        if (returnStatus.isEmpty()) {
            LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Cannot find a Status.")); 
            return new LinkedList<>();
        }
        return returnStatus;
    }
   
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
