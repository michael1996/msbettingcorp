package mswettencorp.src.processor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.SdTeams;
import mswettencorp.src.mapping.SystemFiles;
import mswettencorp.src.mapping.SystemWebsites;
import mswettencorp.src.reader.common.AbstractReader;
import mswettencorp.src.reader.common.AbstractReaderFactory;
import mswettencorp.src.reader.common.FilesReaderFactory;
import mswettencorp.src.reader.common.HtmlReaderFactory;
import mswettencorp.src.util.HibernateUtil;
import mswettencorp.src.util.LoggerUtil;
import org.hibernate.HibernateException;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> MatchProcessor <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 09.02.2018
 */
public class MatchProcessor {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final Logger LOGGER = LoggerUtil.getLogger();
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    private Session session;  
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    /**
     * 
     * @return 
     */
    public boolean initialize() {
        try {
            session = HibernateUtil.startConfiguredSession(); 
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        } catch (HibernateException ex) {
            LOGGER.log(Level.SEVERE, null, ex.getMessage());
            return false;
        }
    }
    
    /**
     * 
     */
    public void deInitialize() {
        try {
            session = HibernateUtil.closeSession(); 
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": deinitialized succesfully."));
        } catch (HibernateException ex) {
            LOGGER.log(Level.SEVERE, null, ex.getMessage());
        } 
    }
    
    /**
     * 
     * @param ctxValue 
     */
    public void process(String ctxValue) {
        if (ctxValue.equals("website")) {
            List<SystemWebsites> findWebsites = session.createQuery(" FROM SystemWebsites sw"
                + " WHERE sw.isActive = true"
                + " AND sw.parserType = 'data'")
                .getResultList();
            if(!findWebsites.isEmpty()) {
                for (SystemWebsites website : findWebsites) {
                    AbstractReaderFactory websiteFactory = new HtmlReaderFactory();
                    AbstractReader parser = websiteFactory.getReader(website.getHtmlParser(), website.getWebsiteLink());
                    if (parser.initialize(session)) parser.process();   
                    parser.deInitialize();
                }
            } else {
                LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": No Websites are active."));
            }
        }
        
        if (ctxValue.equals("file")) {
            List<SystemFiles> findFiles = session.createQuery(" FROM SystemFiles sf"
                    + " WHERE sf.isActive = true")
                    .getResultList(); 
            if(!findFiles.isEmpty()) {
                for (SystemFiles file : findFiles) {
                    AbstractReaderFactory filesFactory = new FilesReaderFactory();
                    AbstractReader reader = filesFactory.getReader(file.getFileReader(), file.getFileName());
                    if (reader.initialize(session)) reader.process();
                    reader.deInitialize();
                }
            } else {
                LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": No File Reader are active."));
            } 
            
            List<SdTeams> listTeams = session.createQuery(" FROM SdTeams t").getResultList();
            for (SdTeams team : listTeams) {
                team.setHomeElo(BigDecimal.valueOf(1500));
                team.setAwayElo(BigDecimal.valueOf(1500));
                session.persist(team);
            }
            List<Matches> listMatches = session.createQuery(" FROM Matches m"
                + " WHERE m.startDate < :date"
                + " AND m.fullTimeResult is not null"
                + " AND m.halfTimeResult is not null"
                + " ORDER BY m.startDate asc")
                .setParameter("date", new Date())
                .getResultList();
            if (!listMatches.isEmpty()) {
                for (Matches eloMatch : listMatches) {
                    SdTeams homeTeam = eloMatch.getHomeTeam();
                    SdTeams awayTeam = eloMatch.getAwayTeam();
                    double weighting = 25;
                    double eloHomeTeam = homeTeam.getHomeElo().doubleValue();
                    double eloAwayTeam = awayTeam.getAwayElo().doubleValue();
                    double calculatedPointsHomeTeam = 1 / (1 + Math.pow(10, (eloAwayTeam - eloHomeTeam) / 400));
                    double calculatedPointsAwayTeam = 1 / (1 + Math.pow(10, (eloHomeTeam - eloAwayTeam) / 400));
                    double newEloHomeTeam = eloHomeTeam;
                    double newEloAwayTeam = eloAwayTeam;

                    switch(eloMatch.getFullTimeResult()) {
                        case "H":
                            newEloHomeTeam = eloHomeTeam + weighting * (1 - calculatedPointsHomeTeam);
                            newEloAwayTeam = eloAwayTeam + weighting * (0 - calculatedPointsAwayTeam);
                            break;
                        case "D":
                            newEloHomeTeam = eloHomeTeam + weighting * (0.5 - calculatedPointsHomeTeam);
                            newEloAwayTeam = eloAwayTeam + weighting * (0.5 - calculatedPointsAwayTeam);
                            break;
                        case "A":
                            newEloHomeTeam = eloHomeTeam + weighting * (0 - calculatedPointsHomeTeam);
                            newEloAwayTeam = eloAwayTeam + weighting * (1 - calculatedPointsAwayTeam);
                            break;
                        default:
                            LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Invalid symbol for result: ")
                                .concat(eloMatch.getFullTimeResult()));
                            break;
                    }
                    homeTeam.setHomeElo(BigDecimal.valueOf(newEloHomeTeam));
                    session.persist(homeTeam);
                    awayTeam.setAwayElo(BigDecimal.valueOf(newEloAwayTeam));
                    session.persist(awayTeam);
                    eloMatch.setHomeTeamElo(BigDecimal.valueOf(eloHomeTeam));
                    eloMatch.setAwayTeamElo(BigDecimal.valueOf(eloAwayTeam));
                    session.persist(eloMatch);
                    LOGGER.log(Level.INFO, getClass().getSimpleName()
                        .concat(eloMatch.toString())
                        .concat(": elo was calculated"));
                }
            } else {
                LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": No new Elo calculated for the teams."));
            }
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": Elo was calculated succesfully."));
        }  
        LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": Processing matches was succesfully."));
    }
}
