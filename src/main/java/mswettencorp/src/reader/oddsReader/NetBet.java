package mswettencorp.src.reader.oddsReader;

import java.net.URISyntaxException;
import java.util.List;
import java.util.logging.Level;
import mswettencorp.src.reader.common.OddsReader;
import org.apache.http.client.utils.URIBuilder;
import org.hibernate.Session;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> NetBet <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 10.02.2017
 */
public class NetBet extends OddsReader {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final String URL = "sport.netbet.de";
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////          
    
    ////////////////////////////////////////////////////////////////////////////
    // Data Fields
    //////////////////////////////////////////////////////////////////////////// 
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public NetBet(String input) {
        super(input);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean initialize(Session session) {
        builder = new URIBuilder();
        builder.setScheme("https").setHost(URL).setPath(input);
        try {
            uri = builder.build();
        } catch (URISyntaxException ex) {
            LOGGER.log(Level.SEVERE, ex.getMessage());
            return false;
        }
        if (super.initialize(session)) {
            LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": initialized succesfully."));
            return true;
        }
        return false;
    }
    
    @Override
    public void deInitialize() {
        super.deInitialize();
    }
    
    @Override
    public void process() {   
        System.setProperty("webdriver.chrome.driver", "C:/Program Files (x86)/Google/Chrome/Application/chrome.exe");
        WebDriver driver = new ChromeDriver();
        driver.navigate().to("https://sport.netbet.de/fu%C3%9Fball/england-premier-league/");
        //event-details-team-name event-details-team-a
        //event-details-row event-details-row-team-a clearfix
        List<WebElement> homeTeam = driver.findElements(By.className("event-details-team-name event-details-team-a"));
//        List<WebElement> awayTeam = driver.findElements(By.className("event-details-team-name event-details-team-b"));
        
        for (WebElement team : homeTeam) {
            String teamName = team.getText();
        }
        driver.close();
        driver.quit();
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
}
