package mswettencorp.src.mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.Immutable;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> SystemDownloads <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 27.12.2017
 */
@Entity
@Immutable
@Table
public class SystemDownloads implements java.io.Serializable {
   
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    /////////////////////////////////////////////////%///////////////////////////
    private static final long serialVersionUID = -5280171458285798273L;
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
    
    @Column(name = "downloadLink", nullable = false, length = 100)
    private String downloadLink;
    
    @Column(name = "downloader", nullable = false, length = 50)
    private String downloader;
    
    @Column(name = "isActive", nullable = false)
    private boolean isActive;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public SystemDownloads() {

    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((downloadLink == null) ? 0 : downloadLink.hashCode());
        result = prime * result + ((downloader == null) ? 0 : downloader.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof SystemDownloads)) return false;
        SystemDownloads other = (SystemDownloads) obj;
        if( downloadLink == null ) {
            if( other.downloadLink != null ) {
                return false;
            }
        } else if( !downloadLink.equals( other.downloadLink ) ) {
            return false;
        }
        if( downloader == null ) {
            if( other.downloader != null ) {
                return false;
            }
        } else if( !downloader.equals( other.downloader ) ) {
            return false;
        }
        return true;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

    public String getDownloader() {
        return downloader;
    }

    public void setDownloader(String downloader) {
        this.downloader = downloader;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }
}
