package mswettencorp.src.mapping;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> SystemLogging <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 26.12.2017
 */
@Entity
@Table
public class SystemLogging implements java.io.Serializable {
   
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    /////////////////////////////////////////////////%///////////////////////////
    private static final long serialVersionUID = -5280171458285798273L;
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
    
    @Column(name = "tableName", length = 50)
    private String tableName;
    
    @Column(name = "insertedRows", precision = 18, scale = 0)
    private long insertedRows;
        
    @Column (name = "createdBy", length = 50)
    private String createdBy;
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public SystemLogging() {

    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((tableName == null) ? 0 : tableName.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof SystemLogging)) return false;
        SystemLogging other = (SystemLogging) obj;
        if( tableName == null ) {
            if( other.tableName != null ) {
                return false;
            }
        } else if( !tableName.equals( other.tableName ) ) {
            return false;
        }
        return true;
    }
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public long getInsertedRows() {
        return insertedRows;
    }

    public void setInsertedRows(long insertedRows) {
        this.insertedRows = insertedRows;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }
}
