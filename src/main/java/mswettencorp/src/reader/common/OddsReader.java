package mswettencorp.src.reader.common;

import com.mysql.jdbc.StringUtils;
import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import mswettencorp.src.mapping.Matches;
import mswettencorp.src.mapping.Odds;
import mswettencorp.src.mapping.SdBookmakers;
import mswettencorp.src.mapping.SdTeams;
import mswettencorp.src.mapping.SystemMappingTeams;
import org.hibernate.Session;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> OddsReader <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 21.01.2017
 */
public class OddsReader extends HtmlReader {
    
    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////             
    protected List<MatchOdds> listMatchOdds;
    
    ////////////////////////////////////////////////////////////////////////////
    // Data Fields
    //////////////////////////////////////////////////////////////////////////// 
    public class MatchOdds {
        public String homeTeam;
        public String awayTeam;
        public double winHomeOdd;
        public double drawOdd;
        public double winAwayOdd;
        public double under15Odd;
        public double over15Odd;
        public double under25Odd;
        public double over25Odd;
        public double under35Odd;
        public double over35Odd;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public OddsReader(String input) {
        super(input);
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public boolean initialize(Session session) {
        if (super.initialize(session)) {
            listMatchOdds = new LinkedList<>();
            return true;
        }
        return false;
    }
    
    @Override
    public void deInitialize() {
        super.deInitialize();
    }
    
    @Override
    public void process() {   
        super.process();
        SdBookmakers findBookmaker;
        findBookmaker = findBookmaker(getClass().getSimpleName()).get(0);
        for (int i = 0; i < listMatchOdds.size(); i++) {
            listMatchOdds.get(i).winHomeOdd =  listMatchOdds.get(i).winHomeOdd * (1 - findBookmaker.getTaxes().doubleValue());
            listMatchOdds.get(i).drawOdd =  listMatchOdds.get(i).drawOdd * (1 - findBookmaker.getTaxes().doubleValue());
            listMatchOdds.get(i).winAwayOdd =  listMatchOdds.get(i).winAwayOdd * (1 - findBookmaker.getTaxes().doubleValue());
            listMatchOdds.get(i).under15Odd =  listMatchOdds.get(i).under15Odd * (1 - findBookmaker.getTaxes().doubleValue());
            listMatchOdds.get(i).over15Odd =  listMatchOdds.get(i).over15Odd * (1 - findBookmaker.getTaxes().doubleValue());
            listMatchOdds.get(i).under25Odd =  listMatchOdds.get(i).under25Odd * (1 - findBookmaker.getTaxes().doubleValue());
            listMatchOdds.get(i).over25Odd =  listMatchOdds.get(i).over25Odd * (1 - findBookmaker.getTaxes().doubleValue());
            listMatchOdds.get(i).under35Odd =  listMatchOdds.get(i).under35Odd * (1 - findBookmaker.getTaxes().doubleValue());
            listMatchOdds.get(i).over35Odd =  listMatchOdds.get(i).over35Odd * (1 - findBookmaker.getTaxes().doubleValue());
            
            List<SystemMappingTeams> listMappedHomeTeam = mappingTeamsTeam(listMatchOdds.get(i).homeTeam);
            List<SystemMappingTeams> listMappedAwayTeam = mappingTeamsTeam(listMatchOdds.get(i).awayTeam);
            if (listMappedHomeTeam.isEmpty() || listMappedAwayTeam.isEmpty()) {
                continue;
            }
            SystemMappingTeams mappedHomeTeam = listMappedHomeTeam.get(0);
            SystemMappingTeams mappedAwayTeam = listMappedAwayTeam.get(0);
            if (StringUtils.isNullOrEmpty(mappedHomeTeam.getOriginalName())) {
                LOGGER.log(Level.WARNING, getClass().getSimpleName()
                    .concat(": Skipped row. Cannot identify original team name: ")
                    .concat(mappedHomeTeam.getMappingName()));
                continue;
            }
            if (StringUtils.isNullOrEmpty(mappedAwayTeam.getOriginalName())) {
                LOGGER.log(Level.WARNING, getClass().getSimpleName()
                    .concat(": Skipped row. Cannot identify original team name: ")
                    .concat(mappedAwayTeam.getMappingName()));
                continue;
            }
            List<SdTeams> listHomeTeam = findTeam(mappedHomeTeam.getOriginalName());
            List<SdTeams> listAwayTeam = findTeam(mappedAwayTeam.getOriginalName());
            if (listHomeTeam.isEmpty() || listAwayTeam.isEmpty()) {
                continue;
            }
            SdTeams findHomeTeam = listHomeTeam.get(0);
            SdTeams findAwayTeam = listAwayTeam.get(0);
            Matches findMatch;
            findMatch = findMatch(findHomeTeam, findAwayTeam).get(0);
            if (findMatch.getStatus().getName().equalsIgnoreCase("starting") 
                    || findMatch.getStatus().getName().equalsIgnoreCase("running")) {
                List<Odds> listOdds = session.createQuery(" FROM Odds o"
                    + " WHERE o.bookmaker = :bookmaker"
                    + " AND o.matchOdds = :match"
                    + " AND o.isHistoric = false")
                    .setParameter("bookmaker", findBookmaker)
                    .setParameter("match", findMatch)
                    .setMaxResults(1)
                    .setFetchSize(1)
                    .getResultList();
                if (!listOdds.isEmpty()) {
                    Odds updateOldOdd = listOdds.get(0);
                    updateOldOdd.setIsHistoric(true);
                    session.persist(updateOldOdd);  
                    LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                        .concat(updateOldOdd.toString())
                        .concat(" was updated."));
                }
                Odds createOdd = new Odds();
                createOdd.setMatchOdds(findMatch);
                createOdd.setBookmaker(findBookmaker);
                createOdd.setIsHistoric(false);
                createOdd.setWinHomeOdd(BigDecimal.valueOf(listMatchOdds.get(i).winHomeOdd));
                createOdd.setDrawOdd(BigDecimal.valueOf(listMatchOdds.get(i).drawOdd));
                createOdd.setWinAwayOdd(BigDecimal.valueOf(listMatchOdds.get(i).winAwayOdd));
                session.persist(createOdd);
                LOGGER.log(Level.INFO, getClass().getSimpleName().concat(": ")
                    .concat(createOdd.toString())
                    .concat(" was created."));
            }
        }  
    }
    
    /**
     * 
     * @param homeTeam
     * @param awayTeam
     * @return 
     */
    public List<Matches> findMatch(SdTeams homeTeam, SdTeams awayTeam) {
        List<Matches> returnMatches = session.createQuery("SELECT m FROM Matches m"
                + " WHERE m.homeTeam = :home"
                + " and m.awayTeam = :away"
                + " ORDER by m.startDate desc")
                .setParameter("home", homeTeam)
                .setParameter("away", awayTeam)
                .setMaxResults(1)
                .setFetchSize(1)
                .getResultList();
        if (returnMatches.isEmpty()) {
            LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Cannot find a Match."));  
            return new LinkedList<>();
        }          
        return returnMatches;    
    }
    
    /**
     * 
     * @param bookmakerName
     * @return 
     */
    public List<SdBookmakers> findBookmaker(String bookmakerName) {
        List<SdBookmakers> returnBookmakers = session.createQuery("SELECT b FROM SdBookmakers b"
                + " WHERE b.name = :name")
                .setParameter("name", bookmakerName)
                .setMaxResults(1)
                .setFetchSize(1)
                .getResultList();
        if (returnBookmakers.isEmpty()) {
            LOGGER.log(Level.WARNING, getClass().getSimpleName().concat(": Cannot find a Bookmaker."));  
            return new LinkedList<>();
        }
        return returnBookmakers;    
    }
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////   
}
