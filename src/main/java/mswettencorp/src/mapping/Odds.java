package mswettencorp.src.mapping;

import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Formula;

/**
 * <strong>Project:</strong> BettingAnalytics <br>
 * <strong>File:</strong> Odds <br>
 *
 * @author Michael Werner
 * @version v1
 * @since 18.07.2017
 */
@Entity
@Table
public class Odds implements java.io.Serializable {

    ////////////////////////////////////////////////////////////////////////////
    // Constants
    ////////////////////////////////////////////////////////////////////////////
    private static final long serialVersionUID = 9025032408988537712L;
    
    ////////////////////////////////////////////////////////////////////////////
    // Fields
    ////////////////////////////////////////////////////////////////////////////
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, precision = 18, scale = 0)
    private long id;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "bookmaker")
    private SdBookmakers bookmaker;
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "matchOdds")
    private Matches matchOdds;
    
    @Column(name = "isHistoric", nullable = false)
    private boolean isHistoric;
    
    @Column(name = "winHomeOdd", precision = 12, scale = 4)
    private BigDecimal winHomeOdd;
    
    @Column(name = "drawOdd", precision = 12, scale = 4)
    private BigDecimal drawOdd;
    
    @Column(name = "winAwayOdd", precision = 12, scale = 4)
    private BigDecimal winAwayOdd;
    
    @Formula("1 - (1 / winHomeOdd + 1 / drawOdd + 1 / winAwayOdd)")
    private BigDecimal margeThreeWayOdds;
    
    @Formula("(winHomeOdd+drawOdd)/2")
    private BigDecimal homeDrawOdd;
    
    @Formula("(winHomeOdd+winAwayOdd)/2")
    private BigDecimal notDraw;
    
    @Formula("(drawOdd+winAwayOdd)/2")
    private BigDecimal awayDrawOdd;
       
    @Column(name = "under15Odd", precision = 12, scale = 4)
    private BigDecimal under15Odd;
    
    @Column(name = "over15Odd", precision = 12, scale = 4)
    private BigDecimal over15Odd;
    
    @Formula("1 - (1 / under15Odd + 1 / over15Odd)")
    private BigDecimal marge15Odds;
    
    @Column(name = "under25Odd", precision = 12, scale = 4)
    private BigDecimal under25Odd;
    
    @Column(name = "over25Odd", precision = 12, scale = 4)
    private BigDecimal over25Odd;
    
    @Formula("1 - (1 / under25Odd + 1 / over25Odd)")
    private BigDecimal marge25Odds;
    
    @Column(name = "under35Odd", precision = 12, scale = 4)
    private BigDecimal under35Odd;
    
    @Column(name = "over35Odd", precision = 12, scale = 4)
    private BigDecimal over35Odd;
    
    @Formula("1 - (1 / under35Odd + 1 / over35Odd)")
    private BigDecimal marge35Odds;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "created", length = 11)
    private Date created;   

    ////////////////////////////////////////////////////////////////////////////
    // Constructor
    ////////////////////////////////////////////////////////////////////////////
    public Odds() {
        
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Methods
    ////////////////////////////////////////////////////////////////////////////
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (int)(id ^ (id >>> 32));
        result = prime * result + ((bookmaker == null) ? 0 : bookmaker.hashCode());
        result = prime * result + ((matchOdds == null) ? 0 : matchOdds.hashCode());
        return result;
    }

    @Override
    public boolean equals( Object obj ) {
        if (obj == null) return false;
        if (this == obj) return true;
        if (!(obj instanceof Odds)) return false;
        Odds other = (Odds) obj;
        if( bookmaker == null ) {
            if( other.bookmaker != null ) {
                return false;
            }
        } else if( !bookmaker.equals( other.bookmaker ) ) {
            return false;
        }
        if( matchOdds == null ) {
            if( other.matchOdds != null ) {
                return false;
            }
        } else if( !matchOdds.equals( other.matchOdds ) ) {
            return false;
        }
        return true;
    }
    
    ////////////////////////////////////////////////////////////////////////////
    // Getter & Setter
    ////////////////////////////////////////////////////////////////////////////
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public SdBookmakers getBookmaker() {
        return bookmaker;
    }

    public void setBookmaker(SdBookmakers bookmaker) {
        this.bookmaker = bookmaker;
    }

    public Matches getMatchOdds() {
        return matchOdds;
    }

    public void setMatchOdds(Matches matchOdds) {
        this.matchOdds = matchOdds;
    }

    public boolean isIsHistoric() {
        return isHistoric;
    }

    public void setIsHistoric(boolean isHistoric) {
        this.isHistoric = isHistoric;
    }

    public BigDecimal getDrawOdd() {
        return drawOdd;
    }

    public void setDrawOdd(BigDecimal drawOdd) {
        this.drawOdd = drawOdd;
    }

    public BigDecimal getWinAwayOdd() {
        return winAwayOdd;
    }

    public void setWinAwayOdd(BigDecimal winAwayOdd) {
        this.winAwayOdd = winAwayOdd;
    }

    public BigDecimal getWinHomeOdd() {
        return winHomeOdd;
    }

    public void setWinHomeOdd(BigDecimal winHomeOdd) {
        this.winHomeOdd = winHomeOdd;
    }

    public BigDecimal getHomeDrawOdd() {
        return homeDrawOdd;
    }

    public void setHomeDrawOdd(BigDecimal homeDrawOdd) {
        this.homeDrawOdd = homeDrawOdd;
    }

    public BigDecimal getNotDraw() {
        return notDraw;
    }

    public void setNotDraw(BigDecimal notDraw) {
        this.notDraw = notDraw;
    }

    public BigDecimal getAwayDrawOdd() {
        return awayDrawOdd;
    }

    public void setAwayDrawOdd(BigDecimal awayDrawOdd) {
        this.awayDrawOdd = awayDrawOdd;
    }

    public BigDecimal getUnder15Odd() {
        return under15Odd;
    }

    public void setUnder15Odd(BigDecimal under15Odd) {
        this.under15Odd = under15Odd;
    }

    public BigDecimal getOver15Odd() {
        return over15Odd;
    }

    public void setOver15Odd(BigDecimal over15Odd) {
        this.over15Odd = over15Odd;
    }

    public BigDecimal getUnder25Odd() {
        return under25Odd;
    }

    public void setUnder25Odd(BigDecimal under25Odd) {
        this.under25Odd = under25Odd;
    }

    public BigDecimal getOver25Odd() {
        return over25Odd;
    }

    public void setOver25Odd(BigDecimal over25Odd) {
        this.over25Odd = over25Odd;
    }

    public BigDecimal getUnder35Odd() {
        return under35Odd;
    }

    public void setUnder35Odd(BigDecimal under35Odd) {
        this.under35Odd = under35Odd;
    }

    public BigDecimal getOver35Odd() {
        return over35Odd;
    }

    public void setOver35Odd(BigDecimal over35Odd) {
        this.over35Odd = over35Odd;
    }

    public BigDecimal getMargeThreeWayOdds() {
        return margeThreeWayOdds;
    }

    public void setMargeThreeWayOdds(BigDecimal margeThreeWayOdds) {
        this.margeThreeWayOdds = margeThreeWayOdds;
    }

    public BigDecimal getMarge15Odds() {
        return marge15Odds;
    }

    public void setMarge15Odds(BigDecimal marge15Odds) {
        this.marge15Odds = marge15Odds;
    }

    public BigDecimal getMarge25Odds() {
        return marge25Odds;
    }

    public void setMarge25Odds(BigDecimal marge25Odds) {
        this.marge25Odds = marge25Odds;
    }

    public BigDecimal getMarge35Odds() {
        return marge35Odds;
    }

    public void setMarge35Odds(BigDecimal marge35Odds) {
        this.marge35Odds = marge35Odds;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
